package com.ebiz.soap.integration.dao.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ebiz.soap.integration.dao.IntegrationRepository;
import com.ebiz.soap.integration.dao.Sender;
import com.ebiz.soap.integration.wsdl.DocumentWsdlService;
import com.ebiz.soap.integration.wsdl.exception.DocumentoRequestException;
import com.ebizlatin.xsd.DocumentoRequest;

@Repository
public class IntegrationRepositoryImpl implements IntegrationRepository {

	private static final Logger LOGGER = LoggerFactory.getLogger(IntegrationRepositoryImpl.class);

	@Autowired
	private Sender sender;
	
	@Autowired
	private DocumentWsdlService documentWsdlService;

	
	@Override
	public void sendOrdenCompra(String topicName,DocumentoRequest request) {

		String payload=null;
			
		try {
			payload = documentWsdlService.marshalDocumentoRequest(request);
		} catch (DocumentoRequestException e) {
			LOGGER.error("Error generando payload", e);
		}
		
		if (payload != null) {
			try {
				sender.send(topicName, payload);
			} catch (Exception ex) {
				LOGGER.error("kafka sender error", ex);
			}

		}

	}

}
