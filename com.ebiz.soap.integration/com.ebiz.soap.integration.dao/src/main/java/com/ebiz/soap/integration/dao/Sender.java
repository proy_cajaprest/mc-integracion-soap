package com.ebiz.soap.integration.dao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Service;
import org.springframework.util.concurrent.ListenableFuture;

@Service
public class Sender {

  private static final Logger LOGGER = LoggerFactory.getLogger(Sender.class);

  @Autowired
  private KafkaTemplate<String, String> kafkaTemplate;

  public void send(String topic, String payload) {

    LOGGER.info("sending payload='{}' to topic='{}'", payload, topic);
    try {
      ListenableFuture<SendResult<String, String>> lf = kafkaTemplate.send(topic, payload);
      Thread.sleep(1000);
      kafkaTemplate.flush();
      lf.get();
      LOGGER.info("sent payload='{}' to topic='{}'", payload, topic);
    } catch (Exception e) {
      LOGGER.error("Error en el envio a kafka: " + e.getMessage());
    }

  }
}