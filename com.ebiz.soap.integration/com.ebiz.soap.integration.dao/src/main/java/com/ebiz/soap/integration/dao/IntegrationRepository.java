package com.ebiz.soap.integration.dao;

import com.ebizlatin.xsd.DocumentoRequest;

public interface IntegrationRepository {
	
	
	void sendOrdenCompra(String topicName,DocumentoRequest request);

}
