package com.ebiz.soap.integration.service;

import static org.junit.Assert.assertNotNull;

import org.dozer.Mapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.ebiz.soap.integration.api.oauth.IOAuthProxy;
import com.ebiz.soap.integration.api.ordenes.proxy.OrdenesApiProxy;
import com.ebiz.soap.integration.dao.IntegrationRepository;
import com.ebiz.soap.integration.schemas.DocumentXsdService;
import com.ebiz.soap.integration.schemas.ValidateService;
import com.ebiz.soap.integration.schemas.impl.DocumentXsdServiceImpl;
import com.ebiz.soap.integration.schemas.impl.ValidateServiceImpl;
import com.ebiz.soap.integration.service.impl.MessageServiceImpl;
import com.ebiz.soap.integration.service.impl.DocumentServiceImpl;

@RunWith(SpringRunner.class)
@ActiveProfiles("Default")
public class DocumentServiceTest {

	
	@Autowired
	private DocumentService ordenService;
	
	@MockBean
	private OrdenesApiProxy ordenesApiProxy;
	
	@MockBean
	private IOAuthProxy oauthProxy;
	
	@MockBean
	private IntegrationRepository integrationRepository;
	
	@MockBean
	private Mapper mapper;

	

	@Test
	public void sanityTest() {
		assertNotNull(ordenService);
		assertNotNull(oauthProxy);
		assertNotNull(mapper);
		assertNotNull(integrationRepository);
	}
	
	
	@Profile("Default")
	@Configuration
	static public class ConfigTest001 {

		@Bean
		@Primary
		public DocumentService accountService() {
			return new DocumentServiceImpl();
		}
		
		
		@Bean
		@Primary
		public MessageService messageService() {
			return new MessageServiceImpl();
		}
		
		@Bean
		@Primary
		public ValidateService validateService() {
			return new ValidateServiceImpl();
		}
		
		@Bean
		public DocumentXsdService documentXsdService() {
			return new DocumentXsdServiceImpl();
		}
		
		
		
		
	}
	
}
