package com.ebiz.soap.integration.exception;

public class AcctionNotFountException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public AcctionNotFountException(String message, String codigoAccion) {
		super(message);
		this.codigoAccion = codigoAccion;
	}

	public AcctionNotFountException(String message, Throwable cause) {
		super(message, cause);
		this.codigoAccion = null;
	}

	private final String codigoAccion;

	public String getCodigoAccion() {
		return codigoAccion;
	}
	
	

	
}