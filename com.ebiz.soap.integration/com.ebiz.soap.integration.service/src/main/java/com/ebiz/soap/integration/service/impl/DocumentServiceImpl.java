package com.ebiz.soap.integration.service.impl;

import org.apache.commons.lang3.StringUtils;
import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.ebiz.soap.integration.api.oauth.IOAuthProxy;
import com.ebiz.soap.integration.api.oauth.model.TokenResponse;
import com.ebiz.soap.integration.api.ordenes.model.Orden;
import com.ebiz.soap.integration.api.ordenes.proxy.OrdenesApiProxy;
import com.ebiz.soap.integration.dao.IntegrationRepository;
import com.ebiz.soap.integration.exception.AcctionNotFountException;
import com.ebiz.soap.integration.schemas.ValidateService;
import com.ebiz.soap.integration.schemas.exception.XmlRequestException;
import com.ebiz.soap.integration.schemas.exception.XsdException;
import com.ebiz.soap.integration.service.CodigoAccion;
import com.ebiz.soap.integration.service.MessageService;
import com.ebiz.soap.integration.service.DocumentService;
import com.ebizlatin.xsd.DocumentoRequest;
import com.ebizlatin.xsd.DocumentoRequestType;
import com.ebizlatin.xsd.DocumentoResponse;
import com.ebizlatin.xsd.DocumentoResponseType;
import com.ebizlatin.xsd.DocumentosRequest;
import com.ebizlatin.xsd.DocumentosResponse;

import org.apache.commons.lang3.StringUtils;

@Service
public class DocumentServiceImpl implements DocumentService {

	@Autowired
	private MessageService messageService;

	@Autowired
	private ValidateService validateService;

	@Autowired
	private OrdenesApiProxy ordenesApiProxy;

	@Autowired
	private IOAuthProxy oauthProxy;

	@Autowired
	private Mapper mapper;

	@Autowired
	private IntegrationRepository integrationRepository;

	@Value("${kafka.topic.name-o1}")
	private String topicO1;

	@Value("${kafka.topic.name-m1}")
	private String topicM1;

	@Value("${kafka.topic.name-f3}")
	private String topicF3;

	@Value("${kafka.topic.name-oc}")
	private String topicOC;

	@Value("${kafka.topic.name-fa}")
	private String topicFA;

	@Value("${kafka.topic.name-fc}")
	private String topicFC;

	@Value("${kafka.topic.name-fg}")
	private String topicFG;

	@Override
	public DocumentoResponse validatedDocument(DocumentoRequest request) {
		DocumentoResponse documentoResponse = null;

		String[] message = null;

		if (StringUtils.isBlank(request.getOrgId()) || StringUtils.isBlank(request.getCodigoAccion())
				|| StringUtils.isBlank(request.getId()) || StringUtils.isBlank(request.getUserErp())
				|| StringUtils.isBlank(request.getUserOauth()) || StringUtils.isBlank(request.getPwdOauth())

		) {
			documentoResponse = new DocumentoResponse();
			message = messageService.getErrorInfo("error_ESB101");

			documentoResponse.setCodigoError(message[0]);
			documentoResponse.setDataError(message[1]);

		}

		return documentoResponse;

	}

	@Override
	public DocumentoResponse send(DocumentoRequest request) {

		DocumentoResponse documentoResponse = validatedDocument(request);

		if (documentoResponse != null) {
			return documentoResponse;
		}

		documentoResponse = new DocumentoResponse();
		String[] message = null;

		try {

			if (CodigoAccion.CODIGO_ACCION_ORDEN_COMPRA_CREAR.toString().compareTo(request.getCodigoAccion()) == 0) {
				sendOrdenCompra(request);
			} else if (CodigoAccion.CODIGO_ACCION_MOVIMIENTO_PUBLICACION.toString()
					.compareTo(request.getCodigoAccion()) == 0) {
				sendMovimiento(request);
			} else if (CodigoAccion.CODIGO_ACCION_COMPROMANTE_PAGO_PUBLICACION.toString()
					.compareTo(request.getCodigoAccion()) == 0) {
				sendComprobatePago(request);
			} else if (CodigoAccion.CODIGO_ACCION_ORDEN_COMPRA_ANULAR.toString()
					.compareTo(request.getCodigoAccion()) == 0) {
				sendOrdenCompraAnular(request);
			} else if (CodigoAccion.CODIGO_ACCION_PAGO_PUBLICACION.toString()
					.compareTo(request.getCodigoAccion()) == 0) {
				sendPagoPublicacion(request);
			} else if (CodigoAccion.CODIGO_ACCION_COMPROBANTE_PAGO_ANULACION.toString()
					.compareTo(request.getCodigoAccion()) == 0) {
				sendComprobantePagoAnulacion(request);
			} else if (CodigoAccion.CODIGO_ACCION_COMPROBANTE_PAGO_ACTUALIZACION.toString()
					.compareTo(request.getCodigoAccion()) == 0) {
				sendComprobantePagoActualizacion(request);
			} else {
				throw new AcctionNotFountException("send", request.getCodigoAccion());
			}

			message = messageService.getErrorInfo("message_0000");
			documentoResponse.setCodigoError(message[0]);
			documentoResponse.setDataError(message[1]);

		} catch (AcctionNotFountException e) {
			message = messageService.getErrorInfo("error_ESB101");
			documentoResponse.setCodigoError(message[0]);
			documentoResponse.setDataError(e.getMessage());
		} catch (Exception e) {

			message = messageService.getErrorInfo("error_ESB101");
			documentoResponse.setCodigoError(message[0]);
			documentoResponse.setDataError(e.getMessage());
		}

		documentoResponse.setCodigoAccion(request.getCodigoAccion());
		documentoResponse.setOrgId(request.getOrgId());
		documentoResponse.setDocumentID(request.getId());

		return documentoResponse;

	}

	@Override
	public DocumentosResponse query(DocumentosRequest request) {
		

		DocumentosResponse documentosResponse = validatedQuery(request);

		if (!documentosResponse.getDocumento().isEmpty()) {
			return documentosResponse;
		}

		documentosResponse = new DocumentosResponse();
		String[] message = null;
		
		TokenResponse tokenResponse = oauthProxy.login(request.getUserOauth(), request.getPwdOauth());

		
		for (DocumentoRequestType documentoRequest : request.getDocumento()) {

			Orden orden = ordenesApiProxy.queryStatus(documentoRequest.getOrgId(), documentoRequest.getId(),
					tokenResponse.getAccessToken());

			DocumentoResponseType documentoResponseType = mapper.map(documentoRequest, DocumentoResponseType.class);

			if (orden != null) {

				mapper.map(orden, documentoResponseType);

				message = messageService.getErrorInfo("message_0000");
				documentoResponseType.setCodigoError(message[0]);
				documentoResponseType.setDataError(message[1]);

			} else {
				message = messageService.getErrorInfo("error_ESB101");
				documentoResponseType.setCodigoError(message[0]);
				documentoResponseType.setDataError(message[1]);
			}

			documentosResponse.getDocumento().add(documentoResponseType);

		}

		return documentosResponse;
	}

	@Override
	public void sendOrdenCompra(DocumentoRequest request) throws XmlRequestException, XsdException {

		validateService.validateOrdenCompra(request.getDocXml());
		
		String payload = StringUtils.replaceEach(request.getDocXml(), new String[]{"<![CDATA[", "]]>","&lt;![CDATA[","]]&gt;"}, new String[]{"", "","",""});
		
		request.setDocXml(payload);
		

		integrationRepository.sendOrdenCompra(topicO1, request);
	}

	@Override
	public void sendMovimiento(DocumentoRequest request) throws Exception {

		validateService.validateMovimiento(request.getDocXml());

		integrationRepository.sendOrdenCompra(topicM1, request);
	}

	@Override
	public void sendComprobatePago(DocumentoRequest request) throws Exception {

		validateService.validateCompraPago(request.getDocXml());

		integrationRepository.sendOrdenCompra(topicF3, request);
	}

	@Override
	public void sendOrdenCompraAnular(DocumentoRequest request) throws Exception {

		validateService.validateAnularOrdenCompra(request.getDocXml());

		integrationRepository.sendOrdenCompra(topicOC, request);
	}

	@Override
	public void sendPagoPublicacion(DocumentoRequest request) throws Exception {
		integrationRepository.sendOrdenCompra(topicFA, request);
	}

	@Override
	public void sendComprobantePagoAnulacion(DocumentoRequest request) throws Exception {

		validateService.validateAnularComprobatePago(request.getDocXml());

		integrationRepository.sendOrdenCompra(topicFC, request);
	}

	@Override
	public void sendComprobantePagoActualizacion(DocumentoRequest request) throws Exception {
		integrationRepository.sendOrdenCompra(topicFG, request);
	}

	@Override
	public DocumentosResponse validatedQuery(DocumentosRequest request) {
		DocumentosResponse documentoResponse = new DocumentosResponse();

		String[] message = messageService.getErrorInfo("error_ESB101");

		
		if (StringUtils.isBlank(request.getUserOauth()) || StringUtils.isBlank(request.getPwdOauth())) {
			documentoResponse = new DocumentosResponse();
			

			
			for (int i=0; i<=request.getDocumento().size()-1;i++) {
				
				DocumentoResponseType documentoResponseType = new DocumentoResponseType();
				documentoResponseType.setCodigoError(message[0]);
				documentoResponseType.setDataError(message[1]);
				documentoResponse.getDocumento().add(documentoResponseType);
			}
			
		}else {
			
			for (DocumentoRequestType documentoRequestType : request.getDocumento()) {
				
				if (StringUtils.isBlank(documentoRequestType.getOrgId()) 
						|| StringUtils.isBlank(documentoRequestType.getRucProveedor())
						|| StringUtils.isBlank(documentoRequestType.getCodigoAccion()) 
						|| StringUtils.isBlank(documentoRequestType.getId())
						|| StringUtils.isBlank(documentoRequestType.getUserErp())
						) {
					
					DocumentoResponseType documentoResponseType = new DocumentoResponseType();
					documentoResponseType.setCodigoError(message[0]);
					documentoResponseType.setDataError(message[1]);
					documentoResponse.getDocumento().add(documentoResponseType);
					
				}
				
			} 
			
		}
		

		return documentoResponse;
	}

}
