package com.ebiz.soap.integration.config;

import org.dozer.DozerBeanMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.ebiz.soap.integration.schemas.ValidateService;
import com.ebiz.soap.integration.schemas.impl.ValidateServiceImpl;

import java.util.Arrays;
import java.util.List;


@Configuration
public class DozerConfig {

    @Bean(name = "org.dozer.Mapper")
    public DozerBeanMapper dozerBean() {
        List<String> mappingFiles = Arrays.asList(
                "classpath:context/mapping/dozer-global-configuration.xml",
                "classpath:context/mapping/orden-DocumentoResponse.xml"
        );

        DozerBeanMapper dozerBean = new DozerBeanMapper();
        dozerBean.setMappingFiles(mappingFiles);
        return dozerBean;
    }
    
    @Bean
    public ValidateService validateService() {
    		return new ValidateServiceImpl();
    } 

}