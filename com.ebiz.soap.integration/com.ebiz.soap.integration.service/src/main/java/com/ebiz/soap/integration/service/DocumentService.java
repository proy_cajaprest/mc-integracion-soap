package com.ebiz.soap.integration.service;


import com.ebiz.soap.integration.schemas.exception.XmlRequestException;
import com.ebiz.soap.integration.schemas.exception.XsdException;
import com.ebizlatin.xsd.DocumentoRequest;
import com.ebizlatin.xsd.DocumentoResponse;
import com.ebizlatin.xsd.DocumentosRequest;
import com.ebizlatin.xsd.DocumentosResponse;

public interface DocumentService {
	
	DocumentosResponse validatedQuery(DocumentosRequest request);
	DocumentoResponse validatedDocument(DocumentoRequest request);
	
	DocumentoResponse send(DocumentoRequest request);
	
	DocumentosResponse query(DocumentosRequest request);
	
	
	void sendOrdenCompra(DocumentoRequest request) throws XmlRequestException,XsdException;;
	void sendMovimiento(DocumentoRequest request) throws Exception;
	void sendComprobatePago(DocumentoRequest request) throws Exception;
	void sendOrdenCompraAnular(DocumentoRequest request) throws Exception;
	void sendPagoPublicacion(DocumentoRequest request) throws Exception;
	void sendComprobantePagoAnulacion(DocumentoRequest request) throws Exception;
	void sendComprobantePagoActualizacion(DocumentoRequest request) throws Exception;
	
}
