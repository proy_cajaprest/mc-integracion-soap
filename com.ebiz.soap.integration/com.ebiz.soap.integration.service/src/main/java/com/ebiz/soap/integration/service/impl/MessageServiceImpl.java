package com.ebiz.soap.integration.service.impl;


import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;

import com.ebiz.soap.integration.service.MessageService;

@Service
public class MessageServiceImpl implements MessageService {

	@Autowired
    private MessageSource messageSource;

    @Override
    public String getMessage(String key) {
        Locale locale = LocaleContextHolder.getLocale();

        return messageSource.getMessage(key,null,locale);
    }

    @Override
    public String[] getErrorInfo(String key) {
        Locale locale = LocaleContextHolder.getLocale();
        String[] errorInfo = messageSource.getMessage(key,null,locale).split("##");
        return errorInfo;
    }

	
}
