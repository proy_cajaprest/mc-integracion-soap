package com.ebiz.soap.integration.service;

public enum CodigoAccion {
	CODIGO_ACCION_ORDEN_COMPRA_CREAR("O1"),
	CODIGO_ACCION_MOVIMIENTO_PUBLICACION("M1"),
	CODIGO_ACCION_COMPROMANTE_PAGO_PUBLICACION("F3"),
    CODIGO_ACCION_ORDEN_COMPRA_ANULAR("OC"),
    CODIGO_ACCION_PAGO_PUBLICACION("FA"),
	CODIGO_ACCION_COMPROBANTE_PAGO_ANULACION("FC"),
	CODIGO_ACCION_COMPROBANTE_PAGO_ACTUALIZACION("FG")
    ;

    private final String text;

    /**
     * @param text
     */
    private CodigoAccion(final String text) {
        this.text = text;
    }

    /* (non-Javadoc)
     * @see java.lang.Enum#toString()
     */
    @Override
    public String toString() {
        return text;
    }
}