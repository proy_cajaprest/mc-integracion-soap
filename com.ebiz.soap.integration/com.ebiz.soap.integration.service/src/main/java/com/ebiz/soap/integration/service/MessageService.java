package com.ebiz.soap.integration.service;

public interface MessageService {
	
	String getMessage(String key);
	String[] getErrorInfo(String key);

}
