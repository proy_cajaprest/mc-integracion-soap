package com.ebiz.soap.integration;


import org.apache.commons.lang3.StringUtils;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.ebiz.soap.integration.api.ordenes.model.CDataDeserializer;
import com.fasterxml.jackson.databind.ObjectMapper;

public class CDataDeserializerTest {

	private ObjectMapper mapper;
	private CDataDeserializer cdataDeserializer;

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		mapper = new ObjectMapper();
		cdataDeserializer = new CDataDeserializer();
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
		mapper = null;
	}

	@Test
	public void clearCDATA()  {
		
	 String rp =	StringUtils.replaceEach("<![CDATA[some &lt;![CDATA[stuffsss]]&gt;]]>", new String[]{"<![CDATA[", "]]>","&lt;![CDATA[","]]&gt;"}, new String[]{"", "","",""});
	 
	 
	 
	 Assert.assertEquals("some stuffsss", rp);
		
		
	}
}
