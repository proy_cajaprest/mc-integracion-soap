package com.ebiz.soap.integration.api.ordenes.model;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

public class CDataDeserializer extends JsonDeserializer<String> {

	@Override
	public String deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException, JsonProcessingException {
		
		String text = jp.getText().trim();
		
		return removeCDATA(text);
	}
	
	
	public String removeCDATA (String text) {
	    String resultString = "";
	    Pattern regex = Pattern.compile("(?s)<(\\\\w+)(?: [^>]*)?>(\\\\s*<!\\\\[CDATA\\\\[.*?\\\\]\\\\]>\\\\s*|.*?)</\\\\1>");
	    Matcher regexMatcher = regex.matcher(text);
	    while (regexMatcher.find()) {
	        resultString += regexMatcher.group();
	    }
	    return resultString;
	}
	

}
