package com.ebiz.soap.integration.api.ordenes.model;


import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "NombreOperador",
    "MailOperador",
    "OrgIDOperador"
})
public class OperadorLogistico {

    @JsonProperty("NombreOperador")
    private String nombreOperador;
    @JsonProperty("MailOperador")
    private String mailOperador;
    @JsonProperty("OrgIDOperador")
    private String orgIDOperador;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("NombreOperador")
    public String getNombreOperador() {
        return nombreOperador;
    }

    @JsonProperty("NombreOperador")
    public void setNombreOperador(String nombreOperador) {
        this.nombreOperador = nombreOperador;
    }

    @JsonProperty("MailOperador")
    public String getMailOperador() {
        return mailOperador;
    }

    @JsonProperty("MailOperador")
    public void setMailOperador(String mailOperador) {
        this.mailOperador = mailOperador;
    }

    @JsonProperty("OrgIDOperador")
    public String getOrgIDOperador() {
        return orgIDOperador;
    }

    @JsonProperty("OrgIDOperador")
    public void setOrgIDOperador(String orgIDOperador) {
        this.orgIDOperador = orgIDOperador;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
