package com.ebiz.soap.integration.api.ordenes.proxy.impl;


import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.ResponseErrorHandler;
import org.springframework.web.client.RestTemplate;

import com.ebiz.soap.integration.api.ordenes.model.Orden;
import com.ebiz.soap.integration.api.ordenes.model.RequestCreateOC;
import com.ebiz.soap.integration.api.ordenes.proxy.OrdenesApiProxy;
import com.ebiz.soap.integration.api.ordenes.proxy.contract.OrdenListServiceResponse;


@Repository
public class OrdenesApiProxyImpl implements OrdenesApiProxy {
	
	private static final String STATUS_CODE="0000";
	private static final Logger LOGGER = LoggerFactory.getLogger(OrdenesApiProxyImpl.class);

	@Autowired
	private RestTemplate restTemplate;
	

	@Value("${api.oc.client.query-uri}")
	private String queryUri;
	
	@Value("${api.oc.client.create-uri}")
	private String saveUri;
	
	
	
	
	private final static String TIPO_EMPRESA="C";
	private final static String ORIGEN_DATOS="PEB2M";
	
	@Override
	public Orden queryStatus(String organizationId,String documentId,String tokenBearer) {


		
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.set("Authorization", String.format("Bearer %s",tokenBearer));
		
		headers.set("tipo_empresa", TIPO_EMPRESA);
		headers.set("org_id", organizationId);
		headers.set("origen_datos", ORIGEN_DATOS);
		
		HttpEntity<String>entity = new HttpEntity<>(headers);
		
		ResponseEntity<OrdenListServiceResponse> response = null;
		
		try {
			response = restTemplate.exchange(queryUri, HttpMethod.GET, entity,OrdenListServiceResponse.class , documentId);
		}catch(Exception ex) {
			LOGGER.error("", ex);
		}
		
		
		if(response!=null && response.getStatusCode()==HttpStatus.OK && STATUS_CODE.compareTo(response.getBody().getStatuscode())==0 && !response.getBody().getData().isEmpty()) {
		
			return response.getBody().getData().get(0);
			
		}else {
			return null;
		}
				
		
	
	}
	

	@Override
	public void save(String organizationId, RequestCreateOC request, String tokenBearer) {
		
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.set("Authorization", String.format("Bearer %s",tokenBearer));
		headers.set("origen_datos", "ERP");
		headers.set("tipo_empresa", TIPO_EMPRESA);
		headers.set("org_id", organizationId);
		
		HttpEntity<RequestCreateOC>entity = new HttpEntity<>(request,headers);
		
		//restTemplate.exchange(saveUri, HttpMethod.POST, entity,OrdenListServiceResponse.class);
		restTemplate.exchange(saveUri, HttpMethod.POST, entity,String.class);//nuevo xc
		restTemplate.setErrorHandler(new ResponseErrorHandler() {
			 public boolean isError(HttpStatus status) {
			        HttpStatus.Series series = status.series();
			        return (HttpStatus.Series.CLIENT_ERROR.equals(series)
			                || HttpStatus.Series.SERVER_ERROR.equals(series));
			    }
			 
			@Override
			public boolean hasError(ClientHttpResponse response) throws IOException {
				
				LOGGER.error("Response error: {} {}", response.getStatusCode(), response.getStatusText());

				return isError(response.getStatusCode());
			}
			
			@Override
			public void handleError(ClientHttpResponse response) throws IOException {
				// TODO Auto-generated method stub
				//return RestUtil.isError(response.getStatusCode());
				LOGGER.error("Response error: {} {}", response.getStatusCode(), response.getStatusText());

				
			}
		});
		
		
	}


	

}
