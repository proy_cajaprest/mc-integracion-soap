package com.ebiz.soap.integration.api.ordenes.model;


import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "StateIni",
    "StateFin",
    "Frecuency",
    "Msgmail"
})
public class ServicioCorreo {

    @JsonProperty("StateIni")
    private String stateIni;
    @JsonProperty("StateFin")
    private String stateFin;
    @JsonProperty("Frecuency")
    private String frecuency;
    @JsonProperty("Msgmail")
    private String msgmail;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("StateIni")
    public String getStateIni() {
        return stateIni;
    }

    @JsonProperty("StateIni")
    public void setStateIni(String stateIni) {
        this.stateIni = stateIni;
    }

    @JsonProperty("StateFin")
    public String getStateFin() {
        return stateFin;
    }

    @JsonProperty("StateFin")
    public void setStateFin(String stateFin) {
        this.stateFin = stateFin;
    }

    @JsonProperty("Frecuency")
    public String getFrecuency() {
        return frecuency;
    }

    @JsonProperty("Frecuency")
    public void setFrecuency(String frecuency) {
        this.frecuency = frecuency;
    }

    @JsonProperty("Msgmail")
    public String getMsgmail() {
        return msgmail;
    }

    @JsonProperty("Msgmail")
    public void setMsgmail(String msgmail) {
        this.msgmail = msgmail;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
