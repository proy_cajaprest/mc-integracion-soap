package com.ebiz.soap.integration.api.ordenes.model;


import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "fl_valortotal",
    "NumeroOrden",
    "Fecha",
    "TipoOrden",
    "MonedaOrden",
    "OrgIDVendedor",
    "IDVendedor",
    "NombreVendedor",
    "PaisVendedor",
    "NITVendedor",
    "PrioridadOrden",
    "EnviarComprobanteA",
    "EmitirA",
    "CobrarA",
    "NumeroAP",
    "TipoTransporte",
    "CondicionesPago",
    "FechaIniContrato",
    "FechaFinContrato",
    "GrupoCompra",
    "AtencionA",
    "Contactos",
    "Contacto",
    "CreadoPor",
    "FechaEntrega",
    "TerminosEntrega",
    "FechaEnvio",
    "InspeccionRequerida",
    "Observacion",
    "ValorVenta",
    "Terminos",
    "Descuentos",
    "ValorVentaNeto",
    "OtrosCostos",
    "Impuestos",
    "PorcentajeImpuestos",
    "MontoAPagar",
    "AprobadoPor",
    "FechaAutorizacion",
    "ConsignarA",
    "ValorCondicionesPago",
    "UnidadCondicionesPago",
    "CodigoAlmacenEntrega",
    "Narrativa",
    "CondicionesGenerales",
    "Version",
    "CondicionesEnvio",
    "Expeditor",
    "AgenteInspeccion",
    "Personalizado",
    "TipoPrecio",
    "EmbarqueParcial",
    "NumeroInspeccion",
    "Seguro",
    "PaisEmbarque",
    "PuertoDesembarque",
    "RegionEmbarque",
    "NumeroCotizacionProveedor",
    "ISC",
    "OtroImpuesto",
    "FechaCambioOrden",
    "IndicadorCambioOrden",
    "PrecioCotizacion",
    "TipoCambio",
    "Embarcador",
    "Aduana",
    "Cargo",
    "TiempoRecordatorio",
    "PrecioFOB",
    "Lab",
    "FlagOrigen",
    "TasaCambio",
    "DireccionFactura",
    "EmailContacto",
    "Logo",
    "Firma",
    "FlagOcDirecta",
    "Iva",
    "Utilidades",
    "GastosGen",
    "Cotizacion",
    "ServicioCorreo",
    "OperadorLogistico",
    "AdjuntoOrden"
})
public class Orden {

    @JsonProperty("EstadoOrden")
    private String estado;

    @JsonProperty("EstadoOrden")
    public String getEstado() {
		return estado;
	}
    
    @JsonProperty("EstadoOrden")
	public void setEstado(String estado) {
		this.estado = estado;
	}

	@JsonProperty("TipoOrden")
	private String tipo;
    
    
    
    @JsonProperty("fl_valortotal")
    private String flValortotal;
    @JsonProperty("NumeroOrden")
    private String numeroOrden;
    @JsonProperty("Fecha")
    private String fecha;
    @JsonProperty("TipoOrden")
    private String tipoOrden;
    @JsonProperty("MonedaOrden")
    private String monedaOrden;
    @JsonProperty("OrgIDVendedor")
    private String orgIDVendedor;
    @JsonProperty("IDVendedor")
    private String iDVendedor;
    @JsonProperty("NombreVendedor")
    private String nombreVendedor;
    @JsonProperty("PaisVendedor")
    private String paisVendedor;
    @JsonProperty("NITVendedor")
    private String nITVendedor;
    @JsonProperty("PrioridadOrden")
    private String prioridadOrden;
    @JsonProperty("EnviarComprobanteA")
    private String enviarComprobanteA;
    @JsonProperty("EmitirA")
    private String emitirA;
    @JsonProperty("CobrarA")
    private String cobrarA;
    @JsonProperty("NumeroAP")
    private String numeroAP;
    @JsonProperty("TipoTransporte")
    private String tipoTransporte;
    @JsonProperty("CondicionesPago")
    private String condicionesPago;
    @JsonProperty("FechaIniContrato")
    private String fechaIniContrato;
    @JsonProperty("FechaFinContrato")
    private String fechaFinContrato;
    @JsonProperty("GrupoCompra")
    private String grupoCompra;
    @JsonProperty("AtencionA")
    private String atencionA;
    @JsonProperty("Contactos")
    private Contacto contactos;
    @JsonProperty("Contacto")
    private String contacto;
    @JsonProperty("CreadoPor")
    private String creadoPor;
    @JsonProperty("FechaEntrega")
    private String fechaEntrega;
    @JsonProperty("TerminosEntrega")
    private String terminosEntrega;
    @JsonProperty("FechaEnvio")
    private String fechaEnvio;
    @JsonProperty("InspeccionRequerida")
    private String inspeccionRequerida;
    @JsonProperty("Observacion")
    private String observacion;
    @JsonProperty("ValorVenta")
    private String valorVenta;
    @JsonProperty("Terminos")
    private String terminos;
    @JsonProperty("Descuentos")
    private String descuentos;
    @JsonProperty("ValorVentaNeto")
    private String valorVentaNeto;
    @JsonProperty("OtrosCostos")
    private String otrosCostos;
    @JsonProperty("Impuestos")
    private String impuestos;
    @JsonProperty("PorcentajeImpuestos")
    private String porcentajeImpuestos;
    @JsonProperty("MontoAPagar")
    private String montoAPagar;
    @JsonProperty("AprobadoPor")
    private String aprobadoPor;
    @JsonProperty("FechaAutorizacion")
    private String fechaAutorizacion;
    @JsonProperty("ConsignarA")
    private String consignarA;
    @JsonProperty("ValorCondicionesPago")
    private String valorCondicionesPago;
    @JsonProperty("UnidadCondicionesPago")
    private String unidadCondicionesPago;
    @JsonProperty("CodigoAlmacenEntrega")
    private String codigoAlmacenEntrega;
    @JsonProperty("Narrativa")
    private String narrativa;
    @JsonProperty("CondicionesGenerales")
    private String condicionesGenerales;
    @JsonProperty("Version")
    private String version;
    @JsonProperty("CondicionesEnvio")
    private String condicionesEnvio;
    @JsonProperty("Expeditor")
    private String expeditor;
    @JsonProperty("AgenteInspeccion")
    private String agenteInspeccion;
    @JsonProperty("Personalizado")
    private String personalizado;
    @JsonProperty("TipoPrecio")
    private String tipoPrecio;
    @JsonProperty("EmbarqueParcial")
    private String embarqueParcial;
    @JsonProperty("NumeroInspeccion")
    private String numeroInspeccion;
    @JsonProperty("Seguro")
    private String seguro;
    @JsonProperty("PaisEmbarque")
    private String paisEmbarque;
    @JsonProperty("PuertoDesembarque")
    private String puertoDesembarque;
    @JsonProperty("RegionEmbarque")
    private String regionEmbarque;
    @JsonProperty("NumeroCotizacionProveedor")
    private String numeroCotizacionProveedor;
    @JsonProperty("ISC")
    private String iSC;
    @JsonProperty("OtroImpuesto")
    private String otroImpuesto;
    @JsonProperty("FechaCambioOrden")
    private String fechaCambioOrden;
    @JsonProperty("IndicadorCambioOrden")
    private String indicadorCambioOrden;
    @JsonProperty("PrecioCotizacion")
    private String precioCotizacion;
    @JsonProperty("TipoCambio")
    private String tipoCambio;
    @JsonProperty("Embarcador")
    private String embarcador;
    @JsonProperty("Aduana")
    private String aduana;
    @JsonProperty("Cargo")
    private String cargo;
    @JsonProperty("TiempoRecordatorio")
    private String tiempoRecordatorio;
    @JsonProperty("PrecioFOB")
    private String precioFOB;
    @JsonProperty("Lab")
    private String lab;
    @JsonProperty("FlagOrigen")
    private String flagOrigen;
    @JsonProperty("TasaCambio")
    private String tasaCambio;
    @JsonProperty("DireccionFactura")
    private String direccionFactura;
    @JsonProperty("EmailContacto")
    private String emailContacto;
    @JsonProperty("Logo")
    private String logo;
    @JsonProperty("Firma")
    private String firma;
    @JsonProperty("FlagOcDirecta")
    private String flagOcDirecta;
    @JsonProperty("Iva")
    private String iva;
    @JsonProperty("Utilidades")
    private String utilidades;
    @JsonProperty("GastosGen")
    private String gastosGen;
    @JsonProperty("Cotizacion")
    private Cotizacion cotizacion;
    @JsonProperty("ServicioCorreo")
    private ServicioCorreo servicioCorreo;
    @JsonProperty("OperadorLogistico")
    private OperadorLogistico operadorLogistico;
    @JsonProperty("AdjuntoOrden")
    private List<AdjuntoOrden> adjuntoOrden = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("fl_valortotal")
    public String getFlValortotal() {
        return flValortotal;
    }

    @JsonProperty("fl_valortotal")
    public void setFlValortotal(String flValortotal) {
        this.flValortotal = flValortotal;
    }

    @JsonProperty("NumeroOrden")
    public String getNumeroOrden() {
        return numeroOrden;
    }

    @JsonProperty("NumeroOrden")
    public void setNumeroOrden(String numeroOrden) {
        this.numeroOrden = numeroOrden;
    }

    @JsonProperty("Fecha")
    public String getFecha() {
        return fecha;
    }

    @JsonProperty("Fecha")
    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    @JsonProperty("TipoOrden")
    public String getTipoOrden() {
        return tipoOrden;
    }

    @JsonProperty("TipoOrden")
    public void setTipoOrden(String tipoOrden) {
        this.tipoOrden = tipoOrden;
    }

    @JsonProperty("MonedaOrden")
    public String getMonedaOrden() {
        return monedaOrden;
    }

    @JsonProperty("MonedaOrden")
    public void setMonedaOrden(String monedaOrden) {
        this.monedaOrden = monedaOrden;
    }

    @JsonProperty("OrgIDVendedor")
    public String getOrgIDVendedor() {
        return orgIDVendedor;
    }

    @JsonProperty("OrgIDVendedor")
    public void setOrgIDVendedor(String orgIDVendedor) {
        this.orgIDVendedor = orgIDVendedor;
    }

    @JsonProperty("IDVendedor")
    public String getIDVendedor() {
        return iDVendedor;
    }

    @JsonProperty("IDVendedor")
    public void setIDVendedor(String iDVendedor) {
        this.iDVendedor = iDVendedor;
    }

    @JsonProperty("NombreVendedor")
    public String getNombreVendedor() {
        return nombreVendedor;
    }

    @JsonProperty("NombreVendedor")
    public void setNombreVendedor(String nombreVendedor) {
        this.nombreVendedor = nombreVendedor;
    }

    @JsonProperty("PaisVendedor")
    public String getPaisVendedor() {
        return paisVendedor;
    }

    @JsonProperty("PaisVendedor")
    public void setPaisVendedor(String paisVendedor) {
        this.paisVendedor = paisVendedor;
    }

    @JsonProperty("NITVendedor")
    public String getNITVendedor() {
        return nITVendedor;
    }

    @JsonProperty("NITVendedor")
    public void setNITVendedor(String nITVendedor) {
        this.nITVendedor = nITVendedor;
    }

    @JsonProperty("PrioridadOrden")
    public String getPrioridadOrden() {
        return prioridadOrden;
    }

    @JsonProperty("PrioridadOrden")
    public void setPrioridadOrden(String prioridadOrden) {
        this.prioridadOrden = prioridadOrden;
    }

    @JsonProperty("EnviarComprobanteA")
    public String getEnviarComprobanteA() {
        return enviarComprobanteA;
    }

    @JsonProperty("EnviarComprobanteA")
    public void setEnviarComprobanteA(String enviarComprobanteA) {
        this.enviarComprobanteA = enviarComprobanteA;
    }

    @JsonProperty("EmitirA")
    public String getEmitirA() {
        return emitirA;
    }

    @JsonProperty("EmitirA")
    public void setEmitirA(String emitirA) {
        this.emitirA = emitirA;
    }

    @JsonProperty("CobrarA")
    public String getCobrarA() {
        return cobrarA;
    }

    @JsonProperty("CobrarA")
    public void setCobrarA(String cobrarA) {
        this.cobrarA = cobrarA;
    }

    @JsonProperty("NumeroAP")
    public String getNumeroAP() {
        return numeroAP;
    }

    @JsonProperty("NumeroAP")
    public void setNumeroAP(String numeroAP) {
        this.numeroAP = numeroAP;
    }

    @JsonProperty("TipoTransporte")
    public String getTipoTransporte() {
        return tipoTransporte;
    }

    @JsonProperty("TipoTransporte")
    public void setTipoTransporte(String tipoTransporte) {
        this.tipoTransporte = tipoTransporte;
    }

    @JsonProperty("CondicionesPago")
    public String getCondicionesPago() {
        return condicionesPago;
    }

    @JsonProperty("CondicionesPago")
    public void setCondicionesPago(String condicionesPago) {
        this.condicionesPago = condicionesPago;
    }

    @JsonProperty("FechaIniContrato")
    public String getFechaIniContrato() {
        return fechaIniContrato;
    }

    @JsonProperty("FechaIniContrato")
    public void setFechaIniContrato(String fechaIniContrato) {
        this.fechaIniContrato = fechaIniContrato;
    }

    @JsonProperty("FechaFinContrato")
    public String getFechaFinContrato() {
        return fechaFinContrato;
    }

    @JsonProperty("FechaFinContrato")
    public void setFechaFinContrato(String fechaFinContrato) {
        this.fechaFinContrato = fechaFinContrato;
    }

    @JsonProperty("GrupoCompra")
    public String getGrupoCompra() {
        return grupoCompra;
    }

    @JsonProperty("GrupoCompra")
    public void setGrupoCompra(String grupoCompra) {
        this.grupoCompra = grupoCompra;
    }

    @JsonProperty("AtencionA")
    public String getAtencionA() {
        return atencionA;
    }

    @JsonProperty("AtencionA")
    public void setAtencionA(String atencionA) {
        this.atencionA = atencionA;
    }

    @JsonProperty("Contactos")
    public Contacto getContactos() {
        return contactos;
    }

    @JsonProperty("Contactos")
    public void setContactos(Contacto contactos) {
        this.contactos = contactos;
    }

    @JsonProperty("Contacto")
    public String getContacto() {
        return contacto;
    }

    @JsonProperty("Contacto")
    public void setContacto(String contacto) {
        this.contacto = contacto;
    }

    @JsonProperty("CreadoPor")
    public String getCreadoPor() {
        return creadoPor;
    }

    @JsonProperty("CreadoPor")
    public void setCreadoPor(String creadoPor) {
        this.creadoPor = creadoPor;
    }

    @JsonProperty("FechaEntrega")
    public String getFechaEntrega() {
        return fechaEntrega;
    }

    @JsonProperty("FechaEntrega")
    public void setFechaEntrega(String fechaEntrega) {
        this.fechaEntrega = fechaEntrega;
    }

    @JsonProperty("TerminosEntrega")
    public String getTerminosEntrega() {
        return terminosEntrega;
    }

    @JsonProperty("TerminosEntrega")
    public void setTerminosEntrega(String terminosEntrega) {
        this.terminosEntrega = terminosEntrega;
    }

    @JsonProperty("FechaEnvio")
    public String getFechaEnvio() {
        return fechaEnvio;
    }

    @JsonProperty("FechaEnvio")
    public void setFechaEnvio(String fechaEnvio) {
        this.fechaEnvio = fechaEnvio;
    }

    @JsonProperty("InspeccionRequerida")
    public String getInspeccionRequerida() {
        return inspeccionRequerida;
    }

    @JsonProperty("InspeccionRequerida")
    public void setInspeccionRequerida(String inspeccionRequerida) {
        this.inspeccionRequerida = inspeccionRequerida;
    }

    @JsonProperty("Observacion")
    public String getObservacion() {
        return observacion;
    }

    @JsonProperty("Observacion")
    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    @JsonProperty("ValorVenta")
    public String getValorVenta() {
        return valorVenta;
    }

    @JsonProperty("ValorVenta")
    public void setValorVenta(String valorVenta) {
        this.valorVenta = valorVenta;
    }

    @JsonProperty("Terminos")
    public String getTerminos() {
        return terminos;
    }

    @JsonProperty("Terminos")
    public void setTerminos(String terminos) {
        this.terminos = terminos;
    }

    @JsonProperty("Descuentos")
    public String getDescuentos() {
        return descuentos;
    }

    @JsonProperty("Descuentos")
    public void setDescuentos(String descuentos) {
        this.descuentos = descuentos;
    }

    @JsonProperty("ValorVentaNeto")
    public String getValorVentaNeto() {
        return valorVentaNeto;
    }

    @JsonProperty("ValorVentaNeto")
    public void setValorVentaNeto(String valorVentaNeto) {
        this.valorVentaNeto = valorVentaNeto;
    }

    @JsonProperty("OtrosCostos")
    public String getOtrosCostos() {
        return otrosCostos;
    }

    @JsonProperty("OtrosCostos")
    public void setOtrosCostos(String otrosCostos) {
        this.otrosCostos = otrosCostos;
    }

    @JsonProperty("Impuestos")
    public String getImpuestos() {
        return impuestos;
    }

    @JsonProperty("Impuestos")
    public void setImpuestos(String impuestos) {
        this.impuestos = impuestos;
    }

    @JsonProperty("PorcentajeImpuestos")
    public String getPorcentajeImpuestos() {
        return porcentajeImpuestos;
    }

    @JsonProperty("PorcentajeImpuestos")
    public void setPorcentajeImpuestos(String porcentajeImpuestos) {
        this.porcentajeImpuestos = porcentajeImpuestos;
    }

    @JsonProperty("MontoAPagar")
    public String getMontoAPagar() {
        return montoAPagar;
    }

    @JsonProperty("MontoAPagar")
    public void setMontoAPagar(String montoAPagar) {
        this.montoAPagar = montoAPagar;
    }

    @JsonProperty("AprobadoPor")
    public String getAprobadoPor() {
        return aprobadoPor;
    }

    @JsonProperty("AprobadoPor")
    public void setAprobadoPor(String aprobadoPor) {
        this.aprobadoPor = aprobadoPor;
    }

    @JsonProperty("FechaAutorizacion")
    public String getFechaAutorizacion() {
        return fechaAutorizacion;
    }

    @JsonProperty("FechaAutorizacion")
    public void setFechaAutorizacion(String fechaAutorizacion) {
        this.fechaAutorizacion = fechaAutorizacion;
    }

    @JsonProperty("ConsignarA")
    public String getConsignarA() {
        return consignarA;
    }

    @JsonProperty("ConsignarA")
    public void setConsignarA(String consignarA) {
        this.consignarA = consignarA;
    }

    @JsonProperty("ValorCondicionesPago")
    public String getValorCondicionesPago() {
        return valorCondicionesPago;
    }

    @JsonProperty("ValorCondicionesPago")
    public void setValorCondicionesPago(String valorCondicionesPago) {
        this.valorCondicionesPago = valorCondicionesPago;
    }

    @JsonProperty("UnidadCondicionesPago")
    public String getUnidadCondicionesPago() {
        return unidadCondicionesPago;
    }

    @JsonProperty("UnidadCondicionesPago")
    public void setUnidadCondicionesPago(String unidadCondicionesPago) {
        this.unidadCondicionesPago = unidadCondicionesPago;
    }

    @JsonProperty("CodigoAlmacenEntrega")
    public String getCodigoAlmacenEntrega() {
        return codigoAlmacenEntrega;
    }

    @JsonProperty("CodigoAlmacenEntrega")
    public void setCodigoAlmacenEntrega(String codigoAlmacenEntrega) {
        this.codigoAlmacenEntrega = codigoAlmacenEntrega;
    }

    @JsonProperty("Narrativa")
    public String getNarrativa() {
        return narrativa;
    }

    @JsonProperty("Narrativa")
    public void setNarrativa(String narrativa) {
        this.narrativa = narrativa;
    }

    @JsonProperty("CondicionesGenerales")
    public String getCondicionesGenerales() {
        return condicionesGenerales;
    }

    @JsonProperty("CondicionesGenerales")
    public void setCondicionesGenerales(String condicionesGenerales) {
        this.condicionesGenerales = condicionesGenerales;
    }

    @JsonProperty("Version")
    public String getVersion() {
        return version;
    }

    @JsonProperty("Version")
    public void setVersion(String version) {
        this.version = version;
    }

    @JsonProperty("CondicionesEnvio")
    public String getCondicionesEnvio() {
        return condicionesEnvio;
    }

    @JsonProperty("CondicionesEnvio")
    public void setCondicionesEnvio(String condicionesEnvio) {
        this.condicionesEnvio = condicionesEnvio;
    }

    @JsonProperty("Expeditor")
    public String getExpeditor() {
        return expeditor;
    }

    @JsonProperty("Expeditor")
    public void setExpeditor(String expeditor) {
        this.expeditor = expeditor;
    }

    @JsonProperty("AgenteInspeccion")
    public String getAgenteInspeccion() {
        return agenteInspeccion;
    }

    @JsonProperty("AgenteInspeccion")
    public void setAgenteInspeccion(String agenteInspeccion) {
        this.agenteInspeccion = agenteInspeccion;
    }

    @JsonProperty("Personalizado")
    public String getPersonalizado() {
        return personalizado;
    }

    @JsonProperty("Personalizado")
    public void setPersonalizado(String personalizado) {
        this.personalizado = personalizado;
    }

    @JsonProperty("TipoPrecio")
    public String getTipoPrecio() {
        return tipoPrecio;
    }

    @JsonProperty("TipoPrecio")
    public void setTipoPrecio(String tipoPrecio) {
        this.tipoPrecio = tipoPrecio;
    }

    @JsonProperty("EmbarqueParcial")
    public String getEmbarqueParcial() {
        return embarqueParcial;
    }

    @JsonProperty("EmbarqueParcial")
    public void setEmbarqueParcial(String embarqueParcial) {
        this.embarqueParcial = embarqueParcial;
    }

    @JsonProperty("NumeroInspeccion")
    public String getNumeroInspeccion() {
        return numeroInspeccion;
    }

    @JsonProperty("NumeroInspeccion")
    public void setNumeroInspeccion(String numeroInspeccion) {
        this.numeroInspeccion = numeroInspeccion;
    }

    @JsonProperty("Seguro")
    public String getSeguro() {
        return seguro;
    }

    @JsonProperty("Seguro")
    public void setSeguro(String seguro) {
        this.seguro = seguro;
    }

    @JsonProperty("PaisEmbarque")
    public String getPaisEmbarque() {
        return paisEmbarque;
    }

    @JsonProperty("PaisEmbarque")
    public void setPaisEmbarque(String paisEmbarque) {
        this.paisEmbarque = paisEmbarque;
    }

    @JsonProperty("PuertoDesembarque")
    public String getPuertoDesembarque() {
        return puertoDesembarque;
    }

    @JsonProperty("PuertoDesembarque")
    public void setPuertoDesembarque(String puertoDesembarque) {
        this.puertoDesembarque = puertoDesembarque;
    }

    @JsonProperty("RegionEmbarque")
    public String getRegionEmbarque() {
        return regionEmbarque;
    }

    @JsonProperty("RegionEmbarque")
    public void setRegionEmbarque(String regionEmbarque) {
        this.regionEmbarque = regionEmbarque;
    }

    @JsonProperty("NumeroCotizacionProveedor")
    public String getNumeroCotizacionProveedor() {
        return numeroCotizacionProveedor;
    }

    @JsonProperty("NumeroCotizacionProveedor")
    public void setNumeroCotizacionProveedor(String numeroCotizacionProveedor) {
        this.numeroCotizacionProveedor = numeroCotizacionProveedor;
    }

    @JsonProperty("ISC")
    public String getISC() {
        return iSC;
    }

    @JsonProperty("ISC")
    public void setISC(String iSC) {
        this.iSC = iSC;
    }

    @JsonProperty("OtroImpuesto")
    public String getOtroImpuesto() {
        return otroImpuesto;
    }

    @JsonProperty("OtroImpuesto")
    public void setOtroImpuesto(String otroImpuesto) {
        this.otroImpuesto = otroImpuesto;
    }

    @JsonProperty("FechaCambioOrden")
    public String getFechaCambioOrden() {
        return fechaCambioOrden;
    }

    @JsonProperty("FechaCambioOrden")
    public void setFechaCambioOrden(String fechaCambioOrden) {
        this.fechaCambioOrden = fechaCambioOrden;
    }

    @JsonProperty("IndicadorCambioOrden")
    public String getIndicadorCambioOrden() {
        return indicadorCambioOrden;
    }

    @JsonProperty("IndicadorCambioOrden")
    public void setIndicadorCambioOrden(String indicadorCambioOrden) {
        this.indicadorCambioOrden = indicadorCambioOrden;
    }

    @JsonProperty("PrecioCotizacion")
    public String getPrecioCotizacion() {
        return precioCotizacion;
    }

    @JsonProperty("PrecioCotizacion")
    public void setPrecioCotizacion(String precioCotizacion) {
        this.precioCotizacion = precioCotizacion;
    }

    @JsonProperty("TipoCambio")
    public String getTipoCambio() {
        return tipoCambio;
    }

    @JsonProperty("TipoCambio")
    public void setTipoCambio(String tipoCambio) {
        this.tipoCambio = tipoCambio;
    }

    @JsonProperty("Embarcador")
    public String getEmbarcador() {
        return embarcador;
    }

    @JsonProperty("Embarcador")
    public void setEmbarcador(String embarcador) {
        this.embarcador = embarcador;
    }

    @JsonProperty("Aduana")
    public String getAduana() {
        return aduana;
    }

    @JsonProperty("Aduana")
    public void setAduana(String aduana) {
        this.aduana = aduana;
    }

    @JsonProperty("Cargo")
    public String getCargo() {
        return cargo;
    }

    @JsonProperty("Cargo")
    public void setCargo(String cargo) {
        this.cargo = cargo;
    }

    @JsonProperty("TiempoRecordatorio")
    public String getTiempoRecordatorio() {
        return tiempoRecordatorio;
    }

    @JsonProperty("TiempoRecordatorio")
    public void setTiempoRecordatorio(String tiempoRecordatorio) {
        this.tiempoRecordatorio = tiempoRecordatorio;
    }

    @JsonProperty("PrecioFOB")
    public String getPrecioFOB() {
        return precioFOB;
    }

    @JsonProperty("PrecioFOB")
    public void setPrecioFOB(String precioFOB) {
        this.precioFOB = precioFOB;
    }

    @JsonProperty("Lab")
    public String getLab() {
        return lab;
    }

    @JsonProperty("Lab")
    public void setLab(String lab) {
        this.lab = lab;
    }

    @JsonProperty("FlagOrigen")
    public String getFlagOrigen() {
        return flagOrigen;
    }

    @JsonProperty("FlagOrigen")
    public void setFlagOrigen(String flagOrigen) {
        this.flagOrigen = flagOrigen;
    }

    @JsonProperty("TasaCambio")
    public String getTasaCambio() {
        return tasaCambio;
    }

    @JsonProperty("TasaCambio")
    public void setTasaCambio(String tasaCambio) {
        this.tasaCambio = tasaCambio;
    }

    @JsonProperty("DireccionFactura")
    public String getDireccionFactura() {
        return direccionFactura;
    }

    @JsonProperty("DireccionFactura")
    public void setDireccionFactura(String direccionFactura) {
        this.direccionFactura = direccionFactura;
    }

    @JsonProperty("EmailContacto")
    public String getEmailContacto() {
        return emailContacto;
    }

    @JsonProperty("EmailContacto")
    public void setEmailContacto(String emailContacto) {
        this.emailContacto = emailContacto;
    }

    @JsonProperty("Logo")
    public String getLogo() {
        return logo;
    }

    @JsonProperty("Logo")
    public void setLogo(String logo) {
        this.logo = logo;
    }

    @JsonProperty("Firma")
    public String getFirma() {
        return firma;
    }

    @JsonProperty("Firma")
    public void setFirma(String firma) {
        this.firma = firma;
    }

    @JsonProperty("FlagOcDirecta")
    public String getFlagOcDirecta() {
        return flagOcDirecta;
    }

    @JsonProperty("FlagOcDirecta")
    public void setFlagOcDirecta(String flagOcDirecta) {
        this.flagOcDirecta = flagOcDirecta;
    }

    @JsonProperty("Iva")
    public String getIva() {
        return iva;
    }

    @JsonProperty("Iva")
    public void setIva(String iva) {
        this.iva = iva;
    }

    @JsonProperty("Utilidades")
    public String getUtilidades() {
        return utilidades;
    }

    @JsonProperty("Utilidades")
    public void setUtilidades(String utilidades) {
        this.utilidades = utilidades;
    }

    @JsonProperty("GastosGen")
    public String getGastosGen() {
        return gastosGen;
    }

    @JsonProperty("GastosGen")
    public void setGastosGen(String gastosGen) {
        this.gastosGen = gastosGen;
    }

    @JsonProperty("Cotizacion")
    public Cotizacion getCotizacion() {
        return cotizacion;
    }

    @JsonProperty("Cotizacion")
    public void setCotizacion(Cotizacion cotizacion) {
        this.cotizacion = cotizacion;
    }

    @JsonProperty("ServicioCorreo")
    public ServicioCorreo getServicioCorreo() {
        return servicioCorreo;
    }

    @JsonProperty("ServicioCorreo")
    public void setServicioCorreo(ServicioCorreo servicioCorreo) {
        this.servicioCorreo = servicioCorreo;
    }

    @JsonProperty("OperadorLogistico")
    public OperadorLogistico getOperadorLogistico() {
        return operadorLogistico;
    }

    @JsonProperty("OperadorLogistico")
    public void setOperadorLogistico(OperadorLogistico operadorLogistico) {
        this.operadorLogistico = operadorLogistico;
    }

    @JsonProperty("AdjuntoOrden")
    public List<AdjuntoOrden> getAdjuntoOrden() {
        return adjuntoOrden;
    }

    @JsonProperty("AdjuntoOrden")
    public void setAdjuntoOrden(List<AdjuntoOrden> adjuntoOrden) {
        this.adjuntoOrden = adjuntoOrden;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
