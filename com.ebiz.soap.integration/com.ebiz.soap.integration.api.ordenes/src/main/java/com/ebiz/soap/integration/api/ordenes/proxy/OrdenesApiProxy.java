package com.ebiz.soap.integration.api.ordenes.proxy;

import com.ebiz.soap.integration.api.ordenes.model.Orden;
import com.ebiz.soap.integration.api.ordenes.model.RequestAnularOC;
import com.ebiz.soap.integration.api.ordenes.model.RequestCreateOC;

public interface OrdenesApiProxy {
	
	Orden queryStatus(String organizationId,String documentId,String tokenBearer);
	void save(String organizationId,RequestCreateOC request, String tokenBearer);

}
