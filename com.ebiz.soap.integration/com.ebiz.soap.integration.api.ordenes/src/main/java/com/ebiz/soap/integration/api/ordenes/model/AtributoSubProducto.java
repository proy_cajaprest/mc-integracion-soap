package com.ebiz.soap.integration.api.ordenes.model;


import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "-Nombre",
    "-Modificable",
    "-Obligatorio",
    "OperadorAtributoSubProducto",
    "ValorAtributoSubProducto",
    "UnidadAtributoSubProducto"
})
public class AtributoSubProducto {

    @JsonProperty("-Nombre")
    private String nombre;
    @JsonProperty("-Modificable")
    private String modificable;
    @JsonProperty("-Obligatorio")
    private String obligatorio;
    @JsonProperty("OperadorAtributoSubProducto")
    private String operadorAtributoSubProducto;
    @JsonProperty("ValorAtributoSubProducto")
    private String valorAtributoSubProducto;
    @JsonProperty("UnidadAtributoSubProducto")
    private String unidadAtributoSubProducto;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("-Nombre")
    public String getNombre() {
        return nombre;
    }

    @JsonProperty("-Nombre")
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @JsonProperty("-Modificable")
    public String getModificable() {
        return modificable;
    }

    @JsonProperty("-Modificable")
    public void setModificable(String modificable) {
        this.modificable = modificable;
    }

    @JsonProperty("-Obligatorio")
    public String getObligatorio() {
        return obligatorio;
    }

    @JsonProperty("-Obligatorio")
    public void setObligatorio(String obligatorio) {
        this.obligatorio = obligatorio;
    }

    @JsonProperty("OperadorAtributoSubProducto")
    public String getOperadorAtributoSubProducto() {
        return operadorAtributoSubProducto;
    }

    @JsonProperty("OperadorAtributoSubProducto")
    public void setOperadorAtributoSubProducto(String operadorAtributoSubProducto) {
        this.operadorAtributoSubProducto = operadorAtributoSubProducto;
    }

    @JsonProperty("ValorAtributoSubProducto")
    public String getValorAtributoSubProducto() {
        return valorAtributoSubProducto;
    }

    @JsonProperty("ValorAtributoSubProducto")
    public void setValorAtributoSubProducto(String valorAtributoSubProducto) {
        this.valorAtributoSubProducto = valorAtributoSubProducto;
    }

    @JsonProperty("UnidadAtributoSubProducto")
    public String getUnidadAtributoSubProducto() {
        return unidadAtributoSubProducto;
    }

    @JsonProperty("UnidadAtributoSubProducto")
    public void setUnidadAtributoSubProducto(String unidadAtributoSubProducto) {
        this.unidadAtributoSubProducto = unidadAtributoSubProducto;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
