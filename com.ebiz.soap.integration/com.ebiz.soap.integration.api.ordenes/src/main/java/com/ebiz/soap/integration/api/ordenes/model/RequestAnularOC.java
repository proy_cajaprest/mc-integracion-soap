package com.ebiz.soap.integration.api.ordenes.model;


import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "iddoc",
    "numeroseguimiento",
    "estadoactual",
    "accion",
    "comentario"
})
public class RequestAnularOC {
	
	 @JsonProperty("iddoc")
	    private String iddoc;
	    @JsonProperty("numeroseguimiento")
	    private String numeroseguimiento;
	    @JsonProperty("estadoactual")
	    private String estadoactual;
	    @JsonProperty("accion")
	    private String accion;
	    @JsonProperty("comentario")
	    private String comentario;
	    @JsonIgnore
	    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	    @JsonProperty("iddoc")
	    public String getIddoc() {
	        return iddoc;
	    }

	    @JsonProperty("iddoc")
	    public void setIddoc(String iddoc) {
	        this.iddoc = iddoc;
	    }

	    @JsonProperty("numeroseguimiento")
	    public String getNumeroseguimiento() {
	        return numeroseguimiento;
	    }

	    @JsonProperty("numeroseguimiento")
	    public void setNumeroseguimiento(String numeroseguimiento) {
	        this.numeroseguimiento = numeroseguimiento;
	    }

	    @JsonProperty("estadoactual")
	    public String getEstadoactual() {
	        return estadoactual;
	    }

	    @JsonProperty("estadoactual")
	    public void setEstadoactual(String estadoactual) {
	        this.estadoactual = estadoactual;
	    }

	    @JsonProperty("accion")
	    public String getAccion() {
	        return accion;
	    }

	    @JsonProperty("accion")
	    public void setAccion(String accion) {
	        this.accion = accion;
	    }

	    @JsonProperty("comentario")
	    public String getComentario() {
	        return comentario;
	    }

	    @JsonProperty("comentario")
	    public void setComentario(String comentario) {
	        this.comentario = comentario;
	    }

	    @JsonAnyGetter
	    public Map<String, Object> getAdditionalProperties() {
	        return this.additionalProperties;
	    }

	    @JsonAnySetter
	    public void setAdditionalProperty(String name, Object value) {
	        this.additionalProperties.put(name, value);
	    }

}
