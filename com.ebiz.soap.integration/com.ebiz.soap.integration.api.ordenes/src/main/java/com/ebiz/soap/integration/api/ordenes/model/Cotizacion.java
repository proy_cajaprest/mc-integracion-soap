package com.ebiz.soap.integration.api.ordenes.model;


import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "NumeroRfq",
    "NombreRfq",
    "NumeroCotizacion",
    "VersionCotizacion",
    "Producto"
})
public class Cotizacion {

    @JsonProperty("NumeroRfq")
    private String numeroRfq;
    @JsonProperty("NombreRfq")
    private String nombreRfq;
    @JsonProperty("NumeroCotizacion")
    private String numeroCotizacion;
    @JsonProperty("VersionCotizacion")
    private String versionCotizacion;
    @JsonProperty("Producto")
    private List<Producto> producto = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("NumeroRfq")
    public String getNumeroRfq() {
        return numeroRfq;
    }

    @JsonProperty("NumeroRfq")
    public void setNumeroRfq(String numeroRfq) {
        this.numeroRfq = numeroRfq;
    }

    @JsonProperty("NombreRfq")
    public String getNombreRfq() {
        return nombreRfq;
    }

    @JsonProperty("NombreRfq")
    public void setNombreRfq(String nombreRfq) {
        this.nombreRfq = nombreRfq;
    }

    @JsonProperty("NumeroCotizacion")
    public String getNumeroCotizacion() {
        return numeroCotizacion;
    }

    @JsonProperty("NumeroCotizacion")
    public void setNumeroCotizacion(String numeroCotizacion) {
        this.numeroCotizacion = numeroCotizacion;
    }

    @JsonProperty("VersionCotizacion")
    public String getVersionCotizacion() {
        return versionCotizacion;
    }

    @JsonProperty("VersionCotizacion")
    public void setVersionCotizacion(String versionCotizacion) {
        this.versionCotizacion = versionCotizacion;
    }

    @JsonProperty("Producto")
    public List<Producto> getProducto() {
        return producto;
    }

    @JsonProperty("Producto")
    public void setProducto(List<Producto> producto) {
        this.producto = producto;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
