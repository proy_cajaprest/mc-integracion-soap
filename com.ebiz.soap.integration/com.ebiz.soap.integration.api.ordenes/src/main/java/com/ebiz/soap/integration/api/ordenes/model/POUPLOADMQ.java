package com.ebiz.soap.integration.api.ordenes.model;


import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "Usuario",
    "Clave",
    "CompradorOrgID",
    "CompradorUsuarioID",
    "Orden"
})
public class POUPLOADMQ {

    @JsonProperty("Usuario")
    private String usuario;
    @JsonProperty("Clave")
    private String clave;
    @JsonProperty("CompradorOrgID")
    private String compradorOrgID;
    @JsonProperty("CompradorUsuarioID")
    private String compradorUsuarioID;
    @JsonProperty("Orden")
    private List<Orden> orden = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("Usuario")
    public String getUsuario() {
        return usuario;
    }

    @JsonProperty("Usuario")
    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    @JsonProperty("Clave")
    public String getClave() {
        return clave;
    }

    @JsonProperty("Clave")
    public void setClave(String clave) {
        this.clave = clave;
    }

    @JsonProperty("CompradorOrgID")
    public String getCompradorOrgID() {
        return compradorOrgID;
    }

    @JsonProperty("CompradorOrgID")
    public void setCompradorOrgID(String compradorOrgID) {
        this.compradorOrgID = compradorOrgID;
    }

    @JsonProperty("CompradorUsuarioID")
    public String getCompradorUsuarioID() {
        return compradorUsuarioID;
    }

    @JsonProperty("CompradorUsuarioID")
    public void setCompradorUsuarioID(String compradorUsuarioID) {
        this.compradorUsuarioID = compradorUsuarioID;
    }

    @JsonProperty("Orden")
    public List<Orden> getOrden() {
        return orden;
    }

    @JsonProperty("Orden")
    public void setOrden(List<Orden> orden) {
        this.orden = orden;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
