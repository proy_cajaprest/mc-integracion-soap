package com.ebiz.soap.integration.api.ordenes.model;


import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "CodigoSubProducto",
    "FechaEntregaSubProducto",
    "PrecioSubProducto",
    "ImpuestoSubProducto",
    "CantidadSubProducto",
    "FactorDecimalSubProducto",
    "PrecioUnitarioSubProducto",
    "UnidadSubProducto",
    "DescripcionSubProducto",
    "DescripcionDetalladaSubProducto",
    "PosicionSubProducto",
    "AtributoSubProducto"
})
public class SubProducto {

    @JsonProperty("CodigoSubProducto")
    private String codigoSubProducto;
    @JsonProperty("FechaEntregaSubProducto")
    private String fechaEntregaSubProducto;
    @JsonProperty("PrecioSubProducto")
    private String precioSubProducto;
    @JsonProperty("ImpuestoSubProducto")
    private String impuestoSubProducto;
    @JsonProperty("CantidadSubProducto")
    private String cantidadSubProducto;
    @JsonProperty("FactorDecimalSubProducto")
    private String factorDecimalSubProducto;
    @JsonProperty("PrecioUnitarioSubProducto")
    private String precioUnitarioSubProducto;
    @JsonProperty("UnidadSubProducto")
    private String unidadSubProducto;
    @JsonProperty("DescripcionSubProducto")
    private String descripcionSubProducto;
    @JsonProperty("DescripcionDetalladaSubProducto")
    private String descripcionDetalladaSubProducto;
    @JsonProperty("PosicionSubProducto")
    private String posicionSubProducto;
    @JsonProperty("AtributoSubProducto")
    private List<AtributoSubProducto> atributoSubProducto = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("CodigoSubProducto")
    public String getCodigoSubProducto() {
        return codigoSubProducto;
    }

    @JsonProperty("CodigoSubProducto")
    public void setCodigoSubProducto(String codigoSubProducto) {
        this.codigoSubProducto = codigoSubProducto;
    }

    @JsonProperty("FechaEntregaSubProducto")
    public String getFechaEntregaSubProducto() {
        return fechaEntregaSubProducto;
    }

    @JsonProperty("FechaEntregaSubProducto")
    public void setFechaEntregaSubProducto(String fechaEntregaSubProducto) {
        this.fechaEntregaSubProducto = fechaEntregaSubProducto;
    }

    @JsonProperty("PrecioSubProducto")
    public String getPrecioSubProducto() {
        return precioSubProducto;
    }

    @JsonProperty("PrecioSubProducto")
    public void setPrecioSubProducto(String precioSubProducto) {
        this.precioSubProducto = precioSubProducto;
    }

    @JsonProperty("ImpuestoSubProducto")
    public String getImpuestoSubProducto() {
        return impuestoSubProducto;
    }

    @JsonProperty("ImpuestoSubProducto")
    public void setImpuestoSubProducto(String impuestoSubProducto) {
        this.impuestoSubProducto = impuestoSubProducto;
    }

    @JsonProperty("CantidadSubProducto")
    public String getCantidadSubProducto() {
        return cantidadSubProducto;
    }

    @JsonProperty("CantidadSubProducto")
    public void setCantidadSubProducto(String cantidadSubProducto) {
        this.cantidadSubProducto = cantidadSubProducto;
    }

    @JsonProperty("FactorDecimalSubProducto")
    public String getFactorDecimalSubProducto() {
        return factorDecimalSubProducto;
    }

    @JsonProperty("FactorDecimalSubProducto")
    public void setFactorDecimalSubProducto(String factorDecimalSubProducto) {
        this.factorDecimalSubProducto = factorDecimalSubProducto;
    }

    @JsonProperty("PrecioUnitarioSubProducto")
    public String getPrecioUnitarioSubProducto() {
        return precioUnitarioSubProducto;
    }

    @JsonProperty("PrecioUnitarioSubProducto")
    public void setPrecioUnitarioSubProducto(String precioUnitarioSubProducto) {
        this.precioUnitarioSubProducto = precioUnitarioSubProducto;
    }

    @JsonProperty("UnidadSubProducto")
    public String getUnidadSubProducto() {
        return unidadSubProducto;
    }

    @JsonProperty("UnidadSubProducto")
    public void setUnidadSubProducto(String unidadSubProducto) {
        this.unidadSubProducto = unidadSubProducto;
    }

    @JsonProperty("DescripcionSubProducto")
    public String getDescripcionSubProducto() {
        return descripcionSubProducto;
    }

    @JsonProperty("DescripcionSubProducto")
    public void setDescripcionSubProducto(String descripcionSubProducto) {
        this.descripcionSubProducto = descripcionSubProducto;
    }

    @JsonProperty("DescripcionDetalladaSubProducto")
    public String getDescripcionDetalladaSubProducto() {
        return descripcionDetalladaSubProducto;
    }

    @JsonProperty("DescripcionDetalladaSubProducto")
    public void setDescripcionDetalladaSubProducto(String descripcionDetalladaSubProducto) {
        this.descripcionDetalladaSubProducto = descripcionDetalladaSubProducto;
    }

    @JsonProperty("PosicionSubProducto")
    public String getPosicionSubProducto() {
        return posicionSubProducto;
    }

    @JsonProperty("PosicionSubProducto")
    public void setPosicionSubProducto(String posicionSubProducto) {
        this.posicionSubProducto = posicionSubProducto;
    }

    @JsonProperty("AtributoSubProducto")
    public List<AtributoSubProducto> getAtributoSubProducto() {
        return atributoSubProducto;
    }

    @JsonProperty("AtributoSubProducto")
    public void setAtributoSubProducto(List<AtributoSubProducto> atributoSubProducto) {
        this.atributoSubProducto = atributoSubProducto;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
