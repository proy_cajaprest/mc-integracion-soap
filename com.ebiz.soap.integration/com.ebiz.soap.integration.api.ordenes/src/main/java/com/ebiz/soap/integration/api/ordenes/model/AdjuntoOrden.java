package com.ebiz.soap.integration.api.ordenes.model;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "NombreArchivo",
    "DescripcionArchivo",
    "ContenidoArchivo"
})
public class AdjuntoOrden {

    @JsonProperty("NombreArchivo")
    private String nombreArchivo;
    @JsonProperty("DescripcionArchivo")
    private String descripcionArchivo;
    @JsonProperty("ContenidoArchivo")
    private String contenidoArchivo;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("NombreArchivo")
    public String getNombreArchivo() {
        return nombreArchivo;
    }

    @JsonProperty("NombreArchivo")
    public void setNombreArchivo(String nombreArchivo) {
        this.nombreArchivo = nombreArchivo;
    }

    @JsonProperty("DescripcionArchivo")
    public String getDescripcionArchivo() {
        return descripcionArchivo;
    }

    @JsonProperty("DescripcionArchivo")
    public void setDescripcionArchivo(String descripcionArchivo) {
        this.descripcionArchivo = descripcionArchivo;
    }

    @JsonProperty("ContenidoArchivo")
    public String getContenidoArchivo() {
        return contenidoArchivo;
    }

    @JsonProperty("ContenidoArchivo")
    public void setContenidoArchivo(String contenidoArchivo) {
        this.contenidoArchivo = contenidoArchivo;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
