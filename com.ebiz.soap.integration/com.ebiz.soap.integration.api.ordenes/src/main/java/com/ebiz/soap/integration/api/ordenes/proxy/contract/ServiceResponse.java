package com.ebiz.soap.integration.api.ordenes.proxy.contract;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ServiceResponse {

	@JsonProperty("statuscode")
	protected String statuscode;
	
	@JsonProperty("message")
	protected String message;
	
	public String getStatuscode() {
		return statuscode;
	}
	public void setStatuscode(String statuscode) {
		this.statuscode = statuscode;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
	
	
}
