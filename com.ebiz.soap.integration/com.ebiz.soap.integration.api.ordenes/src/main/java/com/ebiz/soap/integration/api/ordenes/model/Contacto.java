package com.ebiz.soap.integration.api.ordenes.model;


import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "NombreContactoVendedor",
    "EmailContactoVendedor",
    "OtrosDatosVendedor",
    "NombreContactoComprador",
    "EmailContactoComprador",
    "OtrosDatosComprador"
})
public class Contacto {

    @JsonProperty("NombreContactoVendedor")
    private String nombreContactoVendedor;
    @JsonProperty("EmailContactoVendedor")
    private String emailContactoVendedor;
    @JsonProperty("OtrosDatosVendedor")
    private String otrosDatosVendedor;
    @JsonProperty("NombreContactoComprador")
    private String nombreContactoComprador;
    @JsonProperty("EmailContactoComprador")
    private String emailContactoComprador;
    @JsonProperty("OtrosDatosComprador")
    private String otrosDatosComprador;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("NombreContactoVendedor")
    public String getNombreContactoVendedor() {
        return nombreContactoVendedor;
    }

    @JsonProperty("NombreContactoVendedor")
    public void setNombreContactoVendedor(String nombreContactoVendedor) {
        this.nombreContactoVendedor = nombreContactoVendedor;
    }

    @JsonProperty("EmailContactoVendedor")
    public String getEmailContactoVendedor() {
        return emailContactoVendedor;
    }

    @JsonProperty("EmailContactoVendedor")
    public void setEmailContactoVendedor(String emailContactoVendedor) {
        this.emailContactoVendedor = emailContactoVendedor;
    }

    @JsonProperty("OtrosDatosVendedor")
    public String getOtrosDatosVendedor() {
        return otrosDatosVendedor;
    }

    @JsonProperty("OtrosDatosVendedor")
    public void setOtrosDatosVendedor(String otrosDatosVendedor) {
        this.otrosDatosVendedor = otrosDatosVendedor;
    }

    @JsonProperty("NombreContactoComprador")
    public String getNombreContactoComprador() {
        return nombreContactoComprador;
    }

    @JsonProperty("NombreContactoComprador")
    public void setNombreContactoComprador(String nombreContactoComprador) {
        this.nombreContactoComprador = nombreContactoComprador;
    }

    @JsonProperty("EmailContactoComprador")
    public String getEmailContactoComprador() {
        return emailContactoComprador;
    }

    @JsonProperty("EmailContactoComprador")
    public void setEmailContactoComprador(String emailContactoComprador) {
        this.emailContactoComprador = emailContactoComprador;
    }

    @JsonProperty("OtrosDatosComprador")
    public String getOtrosDatosComprador() {
        return otrosDatosComprador;
    }

    @JsonProperty("OtrosDatosComprador")
    public void setOtrosDatosComprador(String otrosDatosComprador) {
        this.otrosDatosComprador = otrosDatosComprador;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
