package com.ebiz.soap.integration.api.ordenes.model;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "CodigoProducto",
    "CodigoProductoMercado",
    "TipoProducto",
    "EstadoProducto",
    "NumeroSolicitud",
    "NumeroProductoSolicitud",
    "NumeroInventario",
    "FechaEntregaProducto",
    "PrecioProducto",
    "ImpuestoProducto",
    "CantidadProducto",
    "PrecioUnitarioProducto",
    "UnidadProducto",
    "DescripcionProducto",
    "DescripcionDetalladaProducto",
    "PosicionProducto",
    "Mercancia",
    "ProductoCat",
    "IdCategoria",
    "ItemRequisicion",
    "CantidadProductoPend",
    "IdProdB2M",
    "IdRequerimiento",
    "PosicionReq",
    "CantidadRecepcionada",
    "AtributoProducto",
    "SubProducto"
})
public class Producto {

    @JsonProperty("CodigoProducto")
    private String codigoProducto;
    @JsonProperty("CodigoProductoMercado")
    private String codigoProductoMercado;
    @JsonProperty("TipoProducto")
    private String tipoProducto;
    @JsonProperty("EstadoProducto")
    private String estadoProducto;
    @JsonProperty("NumeroSolicitud")
    private String numeroSolicitud;
    @JsonProperty("NumeroProductoSolicitud")
    private String numeroProductoSolicitud;
    @JsonProperty("NumeroInventario")
    private String numeroInventario;
    @JsonProperty("FechaEntregaProducto")
    private String fechaEntregaProducto;
    
    @JsonProperty("PrecioProducto")
    private String precioProducto;
    
    @JsonProperty("ImpuestoProducto")
    private String impuestoProducto;
    
    @JsonProperty("CantidadProducto")
    private String cantidadProducto;
    @JsonProperty("PrecioUnitarioProducto")
    private String precioUnitarioProducto;
    @JsonProperty("UnidadProducto")
    private String unidadProducto;
    @JsonProperty("DescripcionProducto")
    private String descripcionProducto;
    @JsonProperty("DescripcionDetalladaProducto")
    private String descripcionDetalladaProducto;
    @JsonProperty("PosicionProducto")
    private String posicionProducto;
    @JsonProperty("Mercancia")
    private String mercancia;
    @JsonProperty("ProductoCat")
    private String productoCat;
    @JsonProperty("IdCategoria")
    private String idCategoria;
    @JsonProperty("ItemRequisicion")
    private String itemRequisicion;
    @JsonProperty("CantidadProductoPend")
    private String cantidadProductoPend;
    @JsonProperty("IdProdB2M")
    private String idProdB2M;
    @JsonProperty("IdRequerimiento")
    private String idRequerimiento;
    @JsonProperty("PosicionReq")
    private String posicionReq;
    @JsonProperty("CantidadRecepcionada")
    private String cantidadRecepcionada;
    @JsonProperty("AtributoProducto")
    private List<AtributoProducto> atributoProducto = null;
    @JsonProperty("SubProducto")
    private List<SubProducto> subProducto = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("CodigoProducto")
    public String getCodigoProducto() {
        return codigoProducto;
    }

    @JsonProperty("CodigoProducto")
    public void setCodigoProducto(String codigoProducto) {
        this.codigoProducto = codigoProducto;
    }

    @JsonProperty("CodigoProductoMercado")
    public String getCodigoProductoMercado() {
        return codigoProductoMercado;
    }

    @JsonProperty("CodigoProductoMercado")
    public void setCodigoProductoMercado(String codigoProductoMercado) {
        this.codigoProductoMercado = codigoProductoMercado;
    }

    @JsonProperty("TipoProducto")
    public String getTipoProducto() {
        return tipoProducto;
    }

    @JsonProperty("TipoProducto")
    public void setTipoProducto(String tipoProducto) {
        this.tipoProducto = tipoProducto;
    }

    @JsonProperty("EstadoProducto")
    public String getEstadoProducto() {
        return estadoProducto;
    }

    @JsonProperty("EstadoProducto")
    public void setEstadoProducto(String estadoProducto) {
        this.estadoProducto = estadoProducto;
    }

    @JsonProperty("NumeroSolicitud")
    public String getNumeroSolicitud() {
        return numeroSolicitud;
    }

    @JsonProperty("NumeroSolicitud")
    public void setNumeroSolicitud(String numeroSolicitud) {
        this.numeroSolicitud = numeroSolicitud;
    }

    @JsonProperty("NumeroProductoSolicitud")
    public String getNumeroProductoSolicitud() {
        return numeroProductoSolicitud;
    }

    @JsonProperty("NumeroProductoSolicitud")
    public void setNumeroProductoSolicitud(String numeroProductoSolicitud) {
        this.numeroProductoSolicitud = numeroProductoSolicitud;
    }

    @JsonProperty("NumeroInventario")
    public String getNumeroInventario() {
        return numeroInventario;
    }

    @JsonProperty("NumeroInventario")
    public void setNumeroInventario(String numeroInventario) {
        this.numeroInventario = numeroInventario;
    }

    @JsonProperty("FechaEntregaProducto")
    public String getFechaEntregaProducto() {
        return fechaEntregaProducto;
    }

    @JsonProperty("FechaEntregaProducto")
    public void setFechaEntregaProducto(String fechaEntregaProducto) {
        this.fechaEntregaProducto = fechaEntregaProducto;
    }

    @JsonProperty("PrecioProducto")
    public String getPrecioProducto() {
        return precioProducto;
    }

    @JsonProperty("PrecioProducto")
    public void setPrecioProducto(String precioProducto) {
        this.precioProducto = precioProducto;
    }

    @JsonProperty("CantidadProducto")
    public String getCantidadProducto() {
        return cantidadProducto;
    }

    @JsonProperty("CantidadProducto")
    public void setCantidadProducto(String cantidadProducto) {
        this.cantidadProducto = cantidadProducto;
    }

    @JsonProperty("PrecioUnitarioProducto")
    public String getPrecioUnitarioProducto() {
        return precioUnitarioProducto;
    }

    @JsonProperty("PrecioUnitarioProducto")
    public void setPrecioUnitarioProducto(String precioUnitarioProducto) {
        this.precioUnitarioProducto = precioUnitarioProducto;
    }

    @JsonProperty("UnidadProducto")
    public String getUnidadProducto() {
        return unidadProducto;
    }

    @JsonProperty("UnidadProducto")
    public void setUnidadProducto(String unidadProducto) {
        this.unidadProducto = unidadProducto;
    }

    @JsonProperty("DescripcionProducto")
    public String getDescripcionProducto() {
        return descripcionProducto;
    }

    @JsonProperty("DescripcionProducto")
    public void setDescripcionProducto(String descripcionProducto) {
        this.descripcionProducto = descripcionProducto;
    }

    @JsonProperty("DescripcionDetalladaProducto")
    public String getDescripcionDetalladaProducto() {
        return descripcionDetalladaProducto;
    }

    @JsonProperty("DescripcionDetalladaProducto")
    public void setDescripcionDetalladaProducto(String descripcionDetalladaProducto) {
        this.descripcionDetalladaProducto = descripcionDetalladaProducto;
    }

    @JsonProperty("PosicionProducto")
    public String getPosicionProducto() {
        return posicionProducto;
    }

    @JsonProperty("PosicionProducto")
    public void setPosicionProducto(String posicionProducto) {
        this.posicionProducto = posicionProducto;
    }

    @JsonProperty("Mercancia")
    public String getMercancia() {
        return mercancia;
    }

    @JsonProperty("Mercancia")
    public void setMercancia(String mercancia) {
        this.mercancia = mercancia;
    }

    @JsonProperty("ProductoCat")
    public String getProductoCat() {
        return productoCat;
    }

    @JsonProperty("ProductoCat")
    public void setProductoCat(String productoCat) {
        this.productoCat = productoCat;
    }

    @JsonProperty("IdCategoria")
    public String getIdCategoria() {
        return idCategoria;
    }

    @JsonProperty("IdCategoria")
    public void setIdCategoria(String idCategoria) {
        this.idCategoria = idCategoria;
    }

    @JsonProperty("ItemRequisicion")
    public String getItemRequisicion() {
        return itemRequisicion;
    }

    @JsonProperty("ItemRequisicion")
    public void setItemRequisicion(String itemRequisicion) {
        this.itemRequisicion = itemRequisicion;
    }

    @JsonProperty("CantidadProductoPend")
    public String getCantidadProductoPend() {
        return cantidadProductoPend;
    }

    @JsonProperty("CantidadProductoPend")
    public void setCantidadProductoPend(String cantidadProductoPend) {
        this.cantidadProductoPend = cantidadProductoPend;
    }

    @JsonProperty("IdProdB2M")
    public String getIdProdB2M() {
        return idProdB2M;
    }

    @JsonProperty("IdProdB2M")
    public void setIdProdB2M(String idProdB2M) {
        this.idProdB2M = idProdB2M;
    }

    @JsonProperty("IdRequerimiento")
    public String getIdRequerimiento() {
        return idRequerimiento;
    }

    @JsonProperty("IdRequerimiento")
    public void setIdRequerimiento(String idRequerimiento) {
        this.idRequerimiento = idRequerimiento;
    }

    @JsonProperty("PosicionReq")
    public String getPosicionReq() {
        return posicionReq;
    }

    @JsonProperty("PosicionReq")
    public void setPosicionReq(String posicionReq) {
        this.posicionReq = posicionReq;
    }

    @JsonProperty("CantidadRecepcionada")
    public String getCantidadRecepcionada() {
        return cantidadRecepcionada;
    }

    @JsonProperty("CantidadRecepcionada")
    public void setCantidadRecepcionada(String cantidadRecepcionada) {
        this.cantidadRecepcionada = cantidadRecepcionada;
    }

    @JsonProperty("AtributoProducto")
    public List<AtributoProducto> getAtributoProducto() {
        return atributoProducto;
    }

    @JsonProperty("AtributoProducto")
    public void setAtributoProducto(List<AtributoProducto> atributoProducto) {
        this.atributoProducto = atributoProducto;
    }

    @JsonProperty("SubProducto")
    public List<SubProducto> getSubProducto() {
        return subProducto;
    }

    @JsonProperty("SubProducto")
    public void setSubProducto(List<SubProducto> subProducto) {
        this.subProducto = subProducto;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

	public String getImpuestoProducto() {
		return impuestoProducto;
	}

	public void setImpuestoProducto(String impuestoProducto) {
		this.impuestoProducto = impuestoProducto;
	}
    
    

}
