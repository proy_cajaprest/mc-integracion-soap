
package com.ebiz.soap.integration.api.ordenes.model;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "-Nombre",
    "-Modificable",
    "-Obligatorio",
    "OperadorAtributoProducto",
    "ValorAtributoProducto",
    "UnidadAtributoProducto",
    "OfferingAttributeEquiv"
})
public class AtributoProducto {

    @JsonProperty("-Nombre")
    private String nombre;
    @JsonProperty("-Modificable")
    private String modificable;
    @JsonProperty("-Obligatorio")
    private String obligatorio;
    @JsonProperty("OperadorAtributoProducto")
    private String operadorAtributoProducto;
    @JsonProperty("ValorAtributoProducto")
    private String valorAtributoProducto;
    @JsonProperty("UnidadAtributoProducto")
    private String unidadAtributoProducto;
    @JsonProperty("OfferingAttributeEquiv")
    private String offeringAttributeEquiv;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("-Nombre")
    public String getNombre() {
        return nombre;
    }

    @JsonProperty("-Nombre")
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @JsonProperty("-Modificable")
    public String getModificable() {
        return modificable;
    }

    @JsonProperty("-Modificable")
    public void setModificable(String modificable) {
        this.modificable = modificable;
    }

    @JsonProperty("-Obligatorio")
    public String getObligatorio() {
        return obligatorio;
    }

    @JsonProperty("-Obligatorio")
    public void setObligatorio(String obligatorio) {
        this.obligatorio = obligatorio;
    }

    @JsonProperty("OperadorAtributoProducto")
    public String getOperadorAtributoProducto() {
        return operadorAtributoProducto;
    }

    @JsonProperty("OperadorAtributoProducto")
    public void setOperadorAtributoProducto(String operadorAtributoProducto) {
        this.operadorAtributoProducto = operadorAtributoProducto;
    }

    @JsonProperty("ValorAtributoProducto")
    public String getValorAtributoProducto() {
        return valorAtributoProducto;
    }

    @JsonProperty("ValorAtributoProducto")
    public void setValorAtributoProducto(String valorAtributoProducto) {
        this.valorAtributoProducto = valorAtributoProducto;
    }

    @JsonProperty("UnidadAtributoProducto")
    public String getUnidadAtributoProducto() {
        return unidadAtributoProducto;
    }

    @JsonProperty("UnidadAtributoProducto")
    public void setUnidadAtributoProducto(String unidadAtributoProducto) {
        this.unidadAtributoProducto = unidadAtributoProducto;
    }

    @JsonProperty("OfferingAttributeEquiv")
    public String getOfferingAttributeEquiv() {
        return offeringAttributeEquiv;
    }

    @JsonProperty("OfferingAttributeEquiv")
    public void setOfferingAttributeEquiv(String offeringAttributeEquiv) {
        this.offeringAttributeEquiv = offeringAttributeEquiv;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
