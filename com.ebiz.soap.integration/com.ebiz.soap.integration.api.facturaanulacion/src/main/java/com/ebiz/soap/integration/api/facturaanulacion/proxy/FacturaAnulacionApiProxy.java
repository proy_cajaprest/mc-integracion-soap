package com.ebiz.soap.integration.api.facturaanulacion.proxy;

import com.ebiz.soap.integration.api.facturaanulacion.model.RequestFacturaAnulacion;

public interface FacturaAnulacionApiProxy {

	/***
	 * 
	 * @param organizationId
	 * @param request
	 * @param tokenBearer
	 */
	void save(String organizationId,RequestFacturaAnulacion request, String tokenBearer);
	
}
