package com.ebiz.soap.integration.api.facturaanulacion.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"FACCAMBIOESTADO"
})
public class RequestFacturaAnulacion {

    @JsonProperty("FACCAMBIOESTADO")
    private FACCAMBIOESTADO fACCAMBIOESTADO;

    @JsonProperty("FACCAMBIOESTADO")
    public FACCAMBIOESTADO getFACCAMBIOESTADO() {
        return fACCAMBIOESTADO;
    }

    @JsonProperty("FACCAMBIOESTADO")
    public void setFACCAMBIOESTADO(FACCAMBIOESTADO fACCAMBIOESTADO) {
        this.fACCAMBIOESTADO = fACCAMBIOESTADO;
    }

}