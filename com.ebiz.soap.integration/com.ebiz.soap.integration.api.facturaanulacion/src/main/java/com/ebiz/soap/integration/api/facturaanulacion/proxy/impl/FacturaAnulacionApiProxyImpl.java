package com.ebiz.soap.integration.api.facturaanulacion.proxy.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;
import com.ebiz.soap.integration.api.facturaanulacion.model.FacturaRequestJson;
import com.ebiz.soap.integration.api.facturaanulacion.model.RequestFacturaAnulacion;
import com.ebiz.soap.integration.api.facturaanulacion.proxy.FacturaAnulacionApiProxy;


@Repository
public class FacturaAnulacionApiProxyImpl implements FacturaAnulacionApiProxy {
	
	
	private static final String STATUS_CODE = "0000";
	private static final Logger logger = LoggerFactory.getLogger(FacturaAnulacionApiProxyImpl.class);

	@Autowired
	private RestTemplate restTemplate;

	@Value("${api.comprobantepago.client.cancel-uri}")
	private String saveUri;

	private final static String TIPO_EMPRESA = "C";
	private final static String ORIGEN_DATOS = "ERP";
	

	@Override
	public void save(String organizationId, RequestFacturaAnulacion request, String tokenBearer) {
		
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.set("Authorization", String.format("Bearer %s", tokenBearer));
		headers.set("origen_datos", ORIGEN_DATOS);
		headers.set("tipo_empresa", TIPO_EMPRESA);
		headers.set("org_id", organizationId);

		
		FacturaRequestJson requestJson = new FacturaRequestJson();
		requestJson.setAccion(request.getFACCAMBIOESTADO().getFactura().getAccion());
		requestJson.setNumeroseguimiento(request.getFACCAMBIOESTADO().getFactura().getNumeroFactura());
		requestJson.setEstadoactual(request.getFACCAMBIOESTADO().getFactura().getEstadoactual());
		requestJson.setIddoc(request.getFACCAMBIOESTADO().getFactura().getIddoc());
		requestJson.setRucCliente(request.getFACCAMBIOESTADO().getRucCliente());
		requestJson.setRucProveedor(request.getFACCAMBIOESTADO().getFactura().getRucProveedor());

		HttpEntity<FacturaRequestJson> entity = new HttpEntity<>(requestJson, headers);

		ResponseEntity<String> response = null;

		try {
			response = restTemplate.exchange(saveUri, HttpMethod.PUT, entity, String.class);
		} catch (Exception ex) {
			logger.error("save", ex);
		}

		if (response != null && response.getStatusCode() == HttpStatus.OK) {// &&
																			// STATUS_CODE.compareTo(response.getBody().getStatuscode())==0
																			// &&
																			// !response.getBody().getData().isEmpty())
																			// {

		}
		
	}

}
