package com.ebiz.soap.integration.api.facturaanulacion.model;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"iddoc",
"numeroFactura",
"estadoactual",
"accion",
"rucProveedor",
"rucComprador"
})
public class Factura {

    @JsonProperty("iddoc")
    private String iddoc;
    @JsonProperty("numeroFactura")
    private String numeroFactura;
    @JsonProperty("estadoactual")
    private String estadoactual;
    @JsonProperty("accion")
    private String accion;
    @JsonProperty("rucProveedor")
    private String rucProveedor;
    @JsonProperty("rucComprador")
    private String rucComprador;

    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("iddoc")
    public String getIddoc() {
        return iddoc;
    }

    @JsonProperty("iddoc")
    public void setIddoc(String iddoc) {
        this.iddoc = iddoc;
    }

    @JsonProperty("numeroFactura")
    public String getNumeroFactura() {
        return numeroFactura;
    }

    @JsonProperty("numeroFactura")
    public void setNumeroFactura(String numeroFactura) {
        this.numeroFactura = numeroFactura;
    }

    @JsonProperty("estadoactual")
    public String getEstadoactual() {
        return estadoactual;
    }

    @JsonProperty("estadoactual")
    public void setEstadoactual(String estadoactual) {
        this.estadoactual = estadoactual;
    }

    @JsonProperty("Accion")
    public String getAccion() {
        return accion;
    }

    @JsonProperty("Accion")
    public void setAccion(String accion) {
        this.accion = accion;
    }

    @JsonProperty("rucProveedor")
    public String getRucProveedor() {
        return rucProveedor;
    }

    @JsonProperty("rucProveedor")
    public void setRucProveedor(String rucProveedor) {
        this.rucProveedor = rucProveedor;
    }

    @JsonProperty("rucComprador")
    public String getRucComprador() {
        return rucComprador;
    }

    @JsonProperty("rucComprador")
    public void setRucComprador(String rucComprador) {
        this.rucComprador = rucComprador;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }
}