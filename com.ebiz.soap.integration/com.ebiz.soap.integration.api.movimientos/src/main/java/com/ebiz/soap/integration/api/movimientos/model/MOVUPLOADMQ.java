package com.ebiz.soap.integration.api.movimientos.model;


import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "RucCliente",
    "RazonSocialCliente",
    "MovimientoMaterial"
})
public class MOVUPLOADMQ {

    @JsonProperty("RucCliente")
    private String rucCliente;
    
    @JsonProperty("RazonSocialCliente")
    private String razonSocialCliente;
    
    @JsonProperty("MovimientoMaterial")
    private List<MovimientoMaterial> movimientoMaterial;
    
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("RucCliente")
    public String getRucCliente() {
        return rucCliente;
    }

    @JsonProperty("RucCliente")
    public void setRucCliente(String rucCliente) {
        this.rucCliente = rucCliente;
    }

    @JsonProperty("RazonSocialCliente")
    public String getRazonSocialCliente() {
        return razonSocialCliente;
    }

    @JsonProperty("RazonSocialCliente")
    public void setRazonSocialCliente(String razonSocialCliente) {
        this.razonSocialCliente = razonSocialCliente;
    }

    @JsonProperty("MovimientoMaterial")
    public List<MovimientoMaterial> getMovimientoMaterial() {
        return movimientoMaterial;
    }

    @JsonProperty("MovimientoMaterial")
    public void setMovimientoMaterial(List<MovimientoMaterial> movimientoMaterial) {
        this.movimientoMaterial = movimientoMaterial;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
