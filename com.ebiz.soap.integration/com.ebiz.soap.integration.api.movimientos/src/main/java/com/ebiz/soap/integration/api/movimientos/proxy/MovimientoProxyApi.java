package com.ebiz.soap.integration.api.movimientos.proxy;

import com.ebiz.soap.integration.api.movimientos.model.RequestCreateMovimiento;

public interface MovimientoProxyApi {

	void save(String organizationId,RequestCreateMovimiento request, String tokenBearer);
	
}
