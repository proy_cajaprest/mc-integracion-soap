package com.ebiz.soap.integration.api.movimientos.proxy.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

import com.ebiz.soap.integration.api.movimientos.model.RequestCreateMovimiento;
import com.ebiz.soap.integration.api.movimientos.proxy.MovimientoProxyApi;

@Repository
public class MovimientoProxyApiImpl implements MovimientoProxyApi {

	private static final String STATUS_CODE = "0000";
	private static final Logger logger = LoggerFactory.getLogger(MovimientoProxyApiImpl.class);

	@Autowired
	private RestTemplate restTemplate;

	@Value("${api.movimiento.client.create-uri}")
	private String saveUri;

	private final static String TIPO_EMPRESA = "C";
	private final static String ORIGEN_DATOS = "ERP";

	@Override
	public void save(String organizationId, RequestCreateMovimiento request, String tokenBearer) {

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.set("Authorization", String.format("Bearer %s", tokenBearer));
		headers.set("origen_datos", ORIGEN_DATOS);
		headers.set("tipo_empresa", TIPO_EMPRESA);
		headers.set("org_id", organizationId);

		HttpEntity<RequestCreateMovimiento> entity = new HttpEntity<>(request, headers);

		ResponseEntity<String> response = null;

		try {
			response = restTemplate.exchange(saveUri, HttpMethod.POST, entity, String.class);
		} catch (Exception ex) {
			logger.error("save", ex);
		}

		if (response != null && response.getStatusCode() == HttpStatus.OK) {// &&
																			// STATUS_CODE.compareTo(response.getBody().getStatuscode())==0
																			// &&
																			// !response.getBody().getData().isEmpty())
																			// {

		}
	}

}
