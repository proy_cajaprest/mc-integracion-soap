
package com.ebiz.soap.integration.api.movimientos.model;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "DocumentoMaterialRef",
    "PosicionMaterialRef",
    "EjercicioMaterialRef",
    "DocumentoGuiaRef"
})
public class Referencia {

    @JsonProperty("DocumentoMaterialRef")
    private String documentoMaterialRef;
    @JsonProperty("PosicionMaterialRef")
    private String posicionMaterialRef;
    @JsonProperty("EjercicioMaterialRef")
    private String ejercicioMaterialRef;
    @JsonProperty("DocumentoGuiaRef")
    private String documentoGuiaRef;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("DocumentoMaterialRef")
    public String getDocumentoMaterialRef() {
        return documentoMaterialRef;
    }

    @JsonProperty("DocumentoMaterialRef")
    public void setDocumentoMaterialRef(String documentoMaterialRef) {
        this.documentoMaterialRef = documentoMaterialRef;
    }

    @JsonProperty("PosicionMaterialRef")
    public String getPosicionMaterialRef() {
        return posicionMaterialRef;
    }

    @JsonProperty("PosicionMaterialRef")
    public void setPosicionMaterialRef(String posicionMaterialRef) {
        this.posicionMaterialRef = posicionMaterialRef;
    }

    @JsonProperty("EjercicioMaterialRef")
    public String getEjercicioMaterialRef() {
        return ejercicioMaterialRef;
    }

    @JsonProperty("EjercicioMaterialRef")
    public void setEjercicioMaterialRef(String ejercicioMaterialRef) {
        this.ejercicioMaterialRef = ejercicioMaterialRef;
    }

    @JsonProperty("DocumentoGuiaRef")
    public String getDocumentoGuiaRef() {
        return documentoGuiaRef;
    }

    @JsonProperty("DocumentoGuiaRef")
    public void setDocumentoGuiaRef(String documentoGuiaRef) {
        this.documentoGuiaRef = documentoGuiaRef;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
