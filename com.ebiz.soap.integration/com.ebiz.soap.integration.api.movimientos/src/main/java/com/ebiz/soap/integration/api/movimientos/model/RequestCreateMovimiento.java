package com.ebiz.soap.integration.api.movimientos.model;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "MOVUPLOADMQ"
})
public class RequestCreateMovimiento {

    @JsonProperty("MOVUPLOADMQ")
    private MOVUPLOADMQ mOVUPLOADMQ;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("MOVUPLOADMQ")
    public MOVUPLOADMQ getMOVUPLOADMQ() {
        return mOVUPLOADMQ;
    }

    @JsonProperty("MOVUPLOADMQ")
    public void setMOVUPLOADMQ(MOVUPLOADMQ mOVUPLOADMQ) {
        this.mOVUPLOADMQ = mOVUPLOADMQ;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}

