
package com.ebiz.soap.integration.api.movimientos.model;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "NumeroGuia",
    "RucProveedor",
    "RazonSocialProveedor",
    "DireccionProveedor",
    "DocumentoMaterial",
    "EjercicioDocMaterial",
    "PosicionDocMaterial",
    "ClaseDocumentoMaterial",
    "ClaseOperacionMaterial",
    "FechaEmision",
    "FechaContabilizacion",
    "FechaRegistro",
    "FechaEnvio",
    "TipoGuia",
    "RucGrupoEmpresarial",
    "UsuarioGuia",
    "CodigoProveedorERP",
    "ObservacionGuia",
    "CodigoSociedad",
    "ClaseMovimiento",
    "DescripcionMovimiento",
    "SignoOperacion",
    "NumeroOC",
    "NumeroItemOC",
    "AlmacenMaterial",
    "NumeroParte",
    "Descripcion",
    "CantidadMovimiento",
    "UnidadMedida",
    "Moneda",
    "CentroEntrega",
    "GrupoArticulos",
    "CantidadPedidoItemOC",
    "UnidadMedidaItemOC",
    "CantidadMovimientoItemOC",
    "UnidadMedidaPrecioItemOC",
    "CantidadMovimientoPrecioItemOC",
    "PrecioItemOC",
    "CantidadBaseItemOC",
    "SubTotalLocal",
    "SubTotal",
    "SubTotalItemOC",
    "SubTotalBrutoItemOC",
    "IndicadorRecepcionFactura",
    "CondicionPago",
    "DescripcionCondicionPago",
    "FechaEntregaItemOC",
    "EstadoOpcionItem",
    "DescripcionEstadoOpcionItem",
    "ObservacionItem",
    "CargoReceptor",
    "CorreoContactoProveedor",
    "RecibidoPor",
    "AutorizadoPor",
    "Estado",
    "ValorServicio",
    "CodigoServicio",
    "DireccionCliente",
    "TelefonoCliente",
    "Referencia"
})
public class MovimientoMaterial {

    @JsonProperty("NumeroGuia")
    private String numeroGuia;
    @JsonProperty("RucProveedor")
    private String rucProveedor;
    @JsonProperty("RazonSocialProveedor")
    private String razonSocialProveedor;
    @JsonProperty("DireccionProveedor")
    private String direccionProveedor;
    @JsonProperty("DocumentoMaterial")
    private String documentoMaterial;
    @JsonProperty("EjercicioDocMaterial")
    private String ejercicioDocMaterial;
    @JsonProperty("PosicionDocMaterial")
    private String posicionDocMaterial;
    @JsonProperty("ClaseDocumentoMaterial")
    private String claseDocumentoMaterial;
    @JsonProperty("ClaseOperacionMaterial")
    private String claseOperacionMaterial;
    @JsonProperty("FechaEmision")
    private String fechaEmision;
    @JsonProperty("FechaContabilizacion")
    private String fechaContabilizacion;
    @JsonProperty("FechaRegistro")
    private String fechaRegistro;
    @JsonProperty("FechaEnvio")
    private String fechaEnvio;
    @JsonProperty("TipoGuia")
    private String tipoGuia;
    @JsonProperty("RucGrupoEmpresarial")
    private String rucGrupoEmpresarial;
    @JsonProperty("UsuarioGuia")
    private String usuarioGuia;
    @JsonProperty("CodigoProveedorERP")
    private String codigoProveedorERP;
    @JsonProperty("ObservacionGuia")
    private String observacionGuia;
    @JsonProperty("CodigoSociedad")
    private String codigoSociedad;
    @JsonProperty("ClaseMovimiento")
    private String claseMovimiento;
    @JsonProperty("DescripcionMovimiento")
    private String descripcionMovimiento;
    @JsonProperty("SignoOperacion")
    private String signoOperacion;
    @JsonProperty("NumeroOC")
    private String numeroOC;
    @JsonProperty("NumeroItemOC")
    private String numeroItemOC;
    @JsonProperty("AlmacenMaterial")
    private String almacenMaterial;
    @JsonProperty("NumeroParte")
    private String numeroParte;
    @JsonProperty("Descripcion")
    private String descripcion;
    @JsonProperty("CantidadMovimiento")
    private String cantidadMovimiento;
    @JsonProperty("UnidadMedida")
    private String unidadMedida;
    @JsonProperty("Moneda")
    private String moneda;
    @JsonProperty("CentroEntrega")
    private String centroEntrega;
    @JsonProperty("GrupoArticulos")
    private String grupoArticulos;
    @JsonProperty("CantidadPedidoItemOC")
    private String cantidadPedidoItemOC;
    @JsonProperty("UnidadMedidaItemOC")
    private String unidadMedidaItemOC;
    @JsonProperty("CantidadMovimientoItemOC")
    private String cantidadMovimientoItemOC;
    @JsonProperty("UnidadMedidaPrecioItemOC")
    private String unidadMedidaPrecioItemOC;
    @JsonProperty("CantidadMovimientoPrecioItemOC")
    private String cantidadMovimientoPrecioItemOC;
    @JsonProperty("PrecioItemOC")
    private String precioItemOC;
    @JsonProperty("CantidadBaseItemOC")
    private String cantidadBaseItemOC;
    @JsonProperty("SubTotalLocal")
    private String subTotalLocal;
    @JsonProperty("SubTotal")
    private String subTotal;
    @JsonProperty("SubTotalItemOC")
    private String subTotalItemOC;
    @JsonProperty("SubTotalBrutoItemOC")
    private String subTotalBrutoItemOC;
    @JsonProperty("IndicadorRecepcionFactura")
    private String indicadorRecepcionFactura;
    @JsonProperty("CondicionPago")
    private String condicionPago;
    @JsonProperty("DescripcionCondicionPago")
    private String descripcionCondicionPago;
    @JsonProperty("FechaEntregaItemOC")
    private String fechaEntregaItemOC;
    @JsonProperty("EstadoOpcionItem")
    private String estadoOpcionItem;
    @JsonProperty("DescripcionEstadoOpcionItem")
    private String descripcionEstadoOpcionItem;
    @JsonProperty("ObservacionItem")
    private String observacionItem;
    @JsonProperty("CargoReceptor")
    private String cargoReceptor;
    @JsonProperty("CorreoContactoProveedor")
    private String correoContactoProveedor;
    @JsonProperty("RecibidoPor")
    private String recibidoPor;
    @JsonProperty("AutorizadoPor")
    private String autorizadoPor;
    @JsonProperty("Estado")
    private String estado;
    @JsonProperty("ValorServicio")
    private String valorServicio;
    @JsonProperty("CodigoServicio")
    private String codigoServicio;
    @JsonProperty("DireccionCliente")
    private String direccionCliente;
    @JsonProperty("TelefonoCliente")
    private String telefonoCliente;
    @JsonProperty("Referencia")
    private Referencia referencia;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("NumeroGuia")
    public String getNumeroGuia() {
        return numeroGuia;
    }

    @JsonProperty("NumeroGuia")
    public void setNumeroGuia(String numeroGuia) {
        this.numeroGuia = numeroGuia;
    }

    @JsonProperty("RucProveedor")
    public String getRucProveedor() {
        return rucProveedor;
    }

    @JsonProperty("RucProveedor")
    public void setRucProveedor(String rucProveedor) {
        this.rucProveedor = rucProveedor;
    }

    @JsonProperty("RazonSocialProveedor")
    public String getRazonSocialProveedor() {
        return razonSocialProveedor;
    }

    @JsonProperty("RazonSocialProveedor")
    public void setRazonSocialProveedor(String razonSocialProveedor) {
        this.razonSocialProveedor = razonSocialProveedor;
    }

    @JsonProperty("DireccionProveedor")
    public String getDireccionProveedor() {
        return direccionProveedor;
    }

    @JsonProperty("DireccionProveedor")
    public void setDireccionProveedor(String direccionProveedor) {
        this.direccionProveedor = direccionProveedor;
    }

    @JsonProperty("DocumentoMaterial")
    public String getDocumentoMaterial() {
        return documentoMaterial;
    }

    @JsonProperty("DocumentoMaterial")
    public void setDocumentoMaterial(String documentoMaterial) {
        this.documentoMaterial = documentoMaterial;
    }

    @JsonProperty("EjercicioDocMaterial")
    public String getEjercicioDocMaterial() {
        return ejercicioDocMaterial;
    }

    @JsonProperty("EjercicioDocMaterial")
    public void setEjercicioDocMaterial(String ejercicioDocMaterial) {
        this.ejercicioDocMaterial = ejercicioDocMaterial;
    }

    @JsonProperty("PosicionDocMaterial")
    public String getPosicionDocMaterial() {
        return posicionDocMaterial;
    }

    @JsonProperty("PosicionDocMaterial")
    public void setPosicionDocMaterial(String posicionDocMaterial) {
        this.posicionDocMaterial = posicionDocMaterial;
    }

    @JsonProperty("ClaseDocumentoMaterial")
    public String getClaseDocumentoMaterial() {
        return claseDocumentoMaterial;
    }

    @JsonProperty("ClaseDocumentoMaterial")
    public void setClaseDocumentoMaterial(String claseDocumentoMaterial) {
        this.claseDocumentoMaterial = claseDocumentoMaterial;
    }

    @JsonProperty("ClaseOperacionMaterial")
    public String getClaseOperacionMaterial() {
        return claseOperacionMaterial;
    }

    @JsonProperty("ClaseOperacionMaterial")
    public void setClaseOperacionMaterial(String claseOperacionMaterial) {
        this.claseOperacionMaterial = claseOperacionMaterial;
    }

    @JsonProperty("FechaEmision")
    public String getFechaEmision() {
        return fechaEmision;
    }

    @JsonProperty("FechaEmision")
    public void setFechaEmision(String fechaEmision) {
        this.fechaEmision = fechaEmision;
    }

    @JsonProperty("FechaContabilizacion")
    public String getFechaContabilizacion() {
        return fechaContabilizacion;
    }

    @JsonProperty("FechaContabilizacion")
    public void setFechaContabilizacion(String fechaContabilizacion) {
        this.fechaContabilizacion = fechaContabilizacion;
    }

    @JsonProperty("FechaRegistro")
    public String getFechaRegistro() {
        return fechaRegistro;
    }

    @JsonProperty("FechaRegistro")
    public void setFechaRegistro(String fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    @JsonProperty("FechaEnvio")
    public String getFechaEnvio() {
        return fechaEnvio;
    }

    @JsonProperty("FechaEnvio")
    public void setFechaEnvio(String fechaEnvio) {
        this.fechaEnvio = fechaEnvio;
    }

    @JsonProperty("TipoGuia")
    public String getTipoGuia() {
        return tipoGuia;
    }

    @JsonProperty("TipoGuia")
    public void setTipoGuia(String tipoGuia) {
        this.tipoGuia = tipoGuia;
    }

    @JsonProperty("RucGrupoEmpresarial")
    public String getRucGrupoEmpresarial() {
        return rucGrupoEmpresarial;
    }

    @JsonProperty("RucGrupoEmpresarial")
    public void setRucGrupoEmpresarial(String rucGrupoEmpresarial) {
        this.rucGrupoEmpresarial = rucGrupoEmpresarial;
    }

    @JsonProperty("UsuarioGuia")
    public String getUsuarioGuia() {
        return usuarioGuia;
    }

    @JsonProperty("UsuarioGuia")
    public void setUsuarioGuia(String usuarioGuia) {
        this.usuarioGuia = usuarioGuia;
    }

    @JsonProperty("CodigoProveedorERP")
    public String getCodigoProveedorERP() {
        return codigoProveedorERP;
    }

    @JsonProperty("CodigoProveedorERP")
    public void setCodigoProveedorERP(String codigoProveedorERP) {
        this.codigoProveedorERP = codigoProveedorERP;
    }

    @JsonProperty("ObservacionGuia")
    public String getObservacionGuia() {
        return observacionGuia;
    }

    @JsonProperty("ObservacionGuia")
    public void setObservacionGuia(String observacionGuia) {
        this.observacionGuia = observacionGuia;
    }

    @JsonProperty("CodigoSociedad")
    public String getCodigoSociedad() {
        return codigoSociedad;
    }

    @JsonProperty("CodigoSociedad")
    public void setCodigoSociedad(String codigoSociedad) {
        this.codigoSociedad = codigoSociedad;
    }

    @JsonProperty("ClaseMovimiento")
    public String getClaseMovimiento() {
        return claseMovimiento;
    }

    @JsonProperty("ClaseMovimiento")
    public void setClaseMovimiento(String claseMovimiento) {
        this.claseMovimiento = claseMovimiento;
    }

    @JsonProperty("DescripcionMovimiento")
    public String getDescripcionMovimiento() {
        return descripcionMovimiento;
    }

    @JsonProperty("DescripcionMovimiento")
    public void setDescripcionMovimiento(String descripcionMovimiento) {
        this.descripcionMovimiento = descripcionMovimiento;
    }

    @JsonProperty("SignoOperacion")
    public String getSignoOperacion() {
        return signoOperacion;
    }

    @JsonProperty("SignoOperacion")
    public void setSignoOperacion(String signoOperacion) {
        this.signoOperacion = signoOperacion;
    }

    @JsonProperty("NumeroOC")
    public String getNumeroOC() {
        return numeroOC;
    }

    @JsonProperty("NumeroOC")
    public void setNumeroOC(String numeroOC) {
        this.numeroOC = numeroOC;
    }

    @JsonProperty("NumeroItemOC")
    public String getNumeroItemOC() {
        return numeroItemOC;
    }

    @JsonProperty("NumeroItemOC")
    public void setNumeroItemOC(String numeroItemOC) {
        this.numeroItemOC = numeroItemOC;
    }

    @JsonProperty("AlmacenMaterial")
    public String getAlmacenMaterial() {
        return almacenMaterial;
    }

    @JsonProperty("AlmacenMaterial")
    public void setAlmacenMaterial(String almacenMaterial) {
        this.almacenMaterial = almacenMaterial;
    }

    @JsonProperty("NumeroParte")
    public String getNumeroParte() {
        return numeroParte;
    }

    @JsonProperty("NumeroParte")
    public void setNumeroParte(String numeroParte) {
        this.numeroParte = numeroParte;
    }

    @JsonProperty("Descripcion")
    public String getDescripcion() {
        return descripcion;
    }

    @JsonProperty("Descripcion")
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @JsonProperty("CantidadMovimiento")
    public String getCantidadMovimiento() {
        return cantidadMovimiento;
    }

    @JsonProperty("CantidadMovimiento")
    public void setCantidadMovimiento(String cantidadMovimiento) {
        this.cantidadMovimiento = cantidadMovimiento;
    }

    @JsonProperty("UnidadMedida")
    public String getUnidadMedida() {
        return unidadMedida;
    }

    @JsonProperty("UnidadMedida")
    public void setUnidadMedida(String unidadMedida) {
        this.unidadMedida = unidadMedida;
    }

    @JsonProperty("Moneda")
    public String getMoneda() {
        return moneda;
    }

    @JsonProperty("Moneda")
    public void setMoneda(String moneda) {
        this.moneda = moneda;
    }

    @JsonProperty("CentroEntrega")
    public String getCentroEntrega() {
        return centroEntrega;
    }

    @JsonProperty("CentroEntrega")
    public void setCentroEntrega(String centroEntrega) {
        this.centroEntrega = centroEntrega;
    }

    @JsonProperty("GrupoArticulos")
    public String getGrupoArticulos() {
        return grupoArticulos;
    }

    @JsonProperty("GrupoArticulos")
    public void setGrupoArticulos(String grupoArticulos) {
        this.grupoArticulos = grupoArticulos;
    }

    @JsonProperty("CantidadPedidoItemOC")
    public String getCantidadPedidoItemOC() {
        return cantidadPedidoItemOC;
    }

    @JsonProperty("CantidadPedidoItemOC")
    public void setCantidadPedidoItemOC(String cantidadPedidoItemOC) {
        this.cantidadPedidoItemOC = cantidadPedidoItemOC;
    }

    @JsonProperty("UnidadMedidaItemOC")
    public String getUnidadMedidaItemOC() {
        return unidadMedidaItemOC;
    }

    @JsonProperty("UnidadMedidaItemOC")
    public void setUnidadMedidaItemOC(String unidadMedidaItemOC) {
        this.unidadMedidaItemOC = unidadMedidaItemOC;
    }

    @JsonProperty("CantidadMovimientoItemOC")
    public String getCantidadMovimientoItemOC() {
        return cantidadMovimientoItemOC;
    }

    @JsonProperty("CantidadMovimientoItemOC")
    public void setCantidadMovimientoItemOC(String cantidadMovimientoItemOC) {
        this.cantidadMovimientoItemOC = cantidadMovimientoItemOC;
    }

    @JsonProperty("UnidadMedidaPrecioItemOC")
    public String getUnidadMedidaPrecioItemOC() {
        return unidadMedidaPrecioItemOC;
    }

    @JsonProperty("UnidadMedidaPrecioItemOC")
    public void setUnidadMedidaPrecioItemOC(String unidadMedidaPrecioItemOC) {
        this.unidadMedidaPrecioItemOC = unidadMedidaPrecioItemOC;
    }

    @JsonProperty("CantidadMovimientoPrecioItemOC")
    public String getCantidadMovimientoPrecioItemOC() {
        return cantidadMovimientoPrecioItemOC;
    }

    @JsonProperty("CantidadMovimientoPrecioItemOC")
    public void setCantidadMovimientoPrecioItemOC(String cantidadMovimientoPrecioItemOC) {
        this.cantidadMovimientoPrecioItemOC = cantidadMovimientoPrecioItemOC;
    }

    @JsonProperty("PrecioItemOC")
    public String getPrecioItemOC() {
        return precioItemOC;
    }

    @JsonProperty("PrecioItemOC")
    public void setPrecioItemOC(String precioItemOC) {
        this.precioItemOC = precioItemOC;
    }

    @JsonProperty("CantidadBaseItemOC")
    public String getCantidadBaseItemOC() {
        return cantidadBaseItemOC;
    }

    @JsonProperty("CantidadBaseItemOC")
    public void setCantidadBaseItemOC(String cantidadBaseItemOC) {
        this.cantidadBaseItemOC = cantidadBaseItemOC;
    }

    @JsonProperty("SubTotalLocal")
    public String getSubTotalLocal() {
        return subTotalLocal;
    }

    @JsonProperty("SubTotalLocal")
    public void setSubTotalLocal(String subTotalLocal) {
        this.subTotalLocal = subTotalLocal;
    }

    @JsonProperty("SubTotal")
    public String getSubTotal() {
        return subTotal;
    }

    @JsonProperty("SubTotal")
    public void setSubTotal(String subTotal) {
        this.subTotal = subTotal;
    }

    @JsonProperty("SubTotalItemOC")
    public String getSubTotalItemOC() {
        return subTotalItemOC;
    }

    @JsonProperty("SubTotalItemOC")
    public void setSubTotalItemOC(String subTotalItemOC) {
        this.subTotalItemOC = subTotalItemOC;
    }

    @JsonProperty("SubTotalBrutoItemOC")
    public String getSubTotalBrutoItemOC() {
        return subTotalBrutoItemOC;
    }

    @JsonProperty("SubTotalBrutoItemOC")
    public void setSubTotalBrutoItemOC(String subTotalBrutoItemOC) {
        this.subTotalBrutoItemOC = subTotalBrutoItemOC;
    }

    @JsonProperty("IndicadorRecepcionFactura")
    public String getIndicadorRecepcionFactura() {
        return indicadorRecepcionFactura;
    }

    @JsonProperty("IndicadorRecepcionFactura")
    public void setIndicadorRecepcionFactura(String indicadorRecepcionFactura) {
        this.indicadorRecepcionFactura = indicadorRecepcionFactura;
    }

    @JsonProperty("CondicionPago")
    public String getCondicionPago() {
        return condicionPago;
    }

    @JsonProperty("CondicionPago")
    public void setCondicionPago(String condicionPago) {
        this.condicionPago = condicionPago;
    }

    @JsonProperty("DescripcionCondicionPago")
    public String getDescripcionCondicionPago() {
        return descripcionCondicionPago;
    }

    @JsonProperty("DescripcionCondicionPago")
    public void setDescripcionCondicionPago(String descripcionCondicionPago) {
        this.descripcionCondicionPago = descripcionCondicionPago;
    }

    @JsonProperty("FechaEntregaItemOC")
    public String getFechaEntregaItemOC() {
        return fechaEntregaItemOC;
    }

    @JsonProperty("FechaEntregaItemOC")
    public void setFechaEntregaItemOC(String fechaEntregaItemOC) {
        this.fechaEntregaItemOC = fechaEntregaItemOC;
    }

    @JsonProperty("EstadoOpcionItem")
    public String getEstadoOpcionItem() {
        return estadoOpcionItem;
    }

    @JsonProperty("EstadoOpcionItem")
    public void setEstadoOpcionItem(String estadoOpcionItem) {
        this.estadoOpcionItem = estadoOpcionItem;
    }

    @JsonProperty("DescripcionEstadoOpcionItem")
    public String getDescripcionEstadoOpcionItem() {
        return descripcionEstadoOpcionItem;
    }

    @JsonProperty("DescripcionEstadoOpcionItem")
    public void setDescripcionEstadoOpcionItem(String descripcionEstadoOpcionItem) {
        this.descripcionEstadoOpcionItem = descripcionEstadoOpcionItem;
    }

    @JsonProperty("ObservacionItem")
    public String getObservacionItem() {
        return observacionItem;
    }

    @JsonProperty("ObservacionItem")
    public void setObservacionItem(String observacionItem) {
        this.observacionItem = observacionItem;
    }

    @JsonProperty("CargoReceptor")
    public String getCargoReceptor() {
        return cargoReceptor;
    }

    @JsonProperty("CargoReceptor")
    public void setCargoReceptor(String cargoReceptor) {
        this.cargoReceptor = cargoReceptor;
    }

    @JsonProperty("CorreoContactoProveedor")
    public String getCorreoContactoProveedor() {
        return correoContactoProveedor;
    }

    @JsonProperty("CorreoContactoProveedor")
    public void setCorreoContactoProveedor(String correoContactoProveedor) {
        this.correoContactoProveedor = correoContactoProveedor;
    }

    @JsonProperty("RecibidoPor")
    public String getRecibidoPor() {
        return recibidoPor;
    }

    @JsonProperty("RecibidoPor")
    public void setRecibidoPor(String recibidoPor) {
        this.recibidoPor = recibidoPor;
    }

    @JsonProperty("AutorizadoPor")
    public String getAutorizadoPor() {
        return autorizadoPor;
    }

    @JsonProperty("AutorizadoPor")
    public void setAutorizadoPor(String autorizadoPor) {
        this.autorizadoPor = autorizadoPor;
    }

    @JsonProperty("Estado")
    public String getEstado() {
        return estado;
    }

    @JsonProperty("Estado")
    public void setEstado(String estado) {
        this.estado = estado;
    }

    @JsonProperty("ValorServicio")
    public String getValorServicio() {
        return valorServicio;
    }

    @JsonProperty("ValorServicio")
    public void setValorServicio(String valorServicio) {
        this.valorServicio = valorServicio;
    }

    @JsonProperty("CodigoServicio")
    public String getCodigoServicio() {
        return codigoServicio;
    }

    @JsonProperty("CodigoServicio")
    public void setCodigoServicio(String codigoServicio) {
        this.codigoServicio = codigoServicio;
    }

    @JsonProperty("DireccionCliente")
    public String getDireccionCliente() {
        return direccionCliente;
    }

    @JsonProperty("DireccionCliente")
    public void setDireccionCliente(String direccionCliente) {
        this.direccionCliente = direccionCliente;
    }

    @JsonProperty("TelefonoCliente")
    public String getTelefonoCliente() {
        return telefonoCliente;
    }

    @JsonProperty("TelefonoCliente")
    public void setTelefonoCliente(String telefonoCliente) {
        this.telefonoCliente = telefonoCliente;
    }

    @JsonProperty("Referencia")
    public Referencia getReferencia() {
        return referencia;
    }

    @JsonProperty("Referencia")
    public void setReferencia(Referencia referencia) {
        this.referencia = referencia;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
