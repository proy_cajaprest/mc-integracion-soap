package com.ebiz.soap.integration.api.comprobantepago.proxy.impl;



import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

import com.ebiz.soap.integration.api.comprobantepago.exception.ParseJsonException;
import com.ebiz.soap.integration.api.comprobantepago.model.FACUPLOADMQ;
import com.ebiz.soap.integration.api.comprobantepago.model.RequestCreateComprobantePago;
import com.ebiz.soap.integration.api.comprobantepago.proxy.ComprobantePagoApiProxy;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;


@Repository
public class ComprobantePagoApiProxyImpl implements ComprobantePagoApiProxy {
	
	
	private static final String STATUS_CODE = "0000";
	private static final Logger logger = LoggerFactory.getLogger(ComprobantePagoApiProxyImpl.class);

	@Autowired
	private RestTemplate restTemplate;

	@Value("${api.comprobantepago.client.create-uri}")
	private String saveUri;

	private final static String TIPO_EMPRESA = "C";
	private final static String ORIGEN_DATOS = "ERP";
	
	@Autowired
	private ObjectMapper objectMapper;

	

	@Override
	public void save(String organizationId, RequestCreateComprobantePago request, String tokenBearer) {
		
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.set("Authorization", String.format("Bearer %s", tokenBearer));
		headers.set("origen_datos", ORIGEN_DATOS);
		headers.set("tipo_empresa", TIPO_EMPRESA);
		headers.set("org_id", organizationId);

		HttpEntity<RequestCreateComprobantePago> entity = new HttpEntity<>(request, headers);

		ResponseEntity<String> response = null;

		try {
			response = restTemplate.exchange(saveUri, HttpMethod.POST, entity, String.class);
		} catch (Exception ex) {
			logger.error("save", ex);
		}

		if (response != null && response.getStatusCode() == HttpStatus.OK) {// &&
																			// STATUS_CODE.compareTo(response.getBody().getStatuscode())==0
																			// &&
																			// !response.getBody().getData().isEmpty())
																			// {

		}
		
	}


	@Override
	public FACUPLOADMQ unmarshalGuiaCompraRequest(String json) throws ParseJsonException {
		
		RequestCreateComprobantePago gdUploadMq = null;
		
		try {
			
			gdUploadMq = objectMapper.readValue(json, RequestCreateComprobantePago.class);
			
			logger.debug("Guia Compra");
			
		} catch (JsonParseException e) {
			logger.error("JsonParseException",e);
		} catch (JsonMappingException e) {
			logger.error("JsonMappingException",e);
		} catch (IOException e) {
			logger.error("IOException",e);
		}
		
		return gdUploadMq.getFACUPLOADMQ();
	}

}
