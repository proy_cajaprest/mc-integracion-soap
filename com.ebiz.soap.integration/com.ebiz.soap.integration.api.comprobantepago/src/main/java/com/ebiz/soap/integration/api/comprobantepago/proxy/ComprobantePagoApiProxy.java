package com.ebiz.soap.integration.api.comprobantepago.proxy;

import com.ebiz.soap.integration.api.comprobantepago.exception.ParseJsonException;
import com.ebiz.soap.integration.api.comprobantepago.model.FACUPLOADMQ;
import com.ebiz.soap.integration.api.comprobantepago.model.RequestCreateComprobantePago;


public interface ComprobantePagoApiProxy {

	/***
	 * 
	 * @param organizationId
	 * @param request
	 * @param tokenBearer
	 */
	void save(String organizationId,RequestCreateComprobantePago request, String tokenBearer);
	
	
	FACUPLOADMQ unmarshalGuiaCompraRequest(String json) throws ParseJsonException;
	
}
