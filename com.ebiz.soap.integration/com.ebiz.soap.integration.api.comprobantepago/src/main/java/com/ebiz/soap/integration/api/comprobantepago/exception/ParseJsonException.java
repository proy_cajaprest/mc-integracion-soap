package com.ebiz.soap.integration.api.comprobantepago.exception;

public class ParseJsonException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ParseJsonException(String message, String request, Throwable cause) {
		super(message, cause);
		this.request = request;
	}

	public ParseJsonException(String message, Throwable cause) {
		super(message, cause);
		this.request = null;
	}

	private final String request;

	public String getRequest() {
		return request;
	}

}
