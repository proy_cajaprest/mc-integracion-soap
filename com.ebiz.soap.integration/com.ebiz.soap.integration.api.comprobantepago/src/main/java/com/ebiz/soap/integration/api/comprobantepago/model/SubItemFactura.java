
package com.ebiz.soap.integration.api.comprobantepago.model;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "NumeroSubItem",
    "CodigoServicioSubItem",
    "DescripcionSubItem",
    "CantidadSubItem",
    "UnidadMedidaSubItem",
    "SubTotalSubItem",
    "PrecioSubItem"
})
public class SubItemFactura {

    @JsonProperty("NumeroSubItem")
    private String numeroSubItem;
    @JsonProperty("CodigoServicioSubItem")
    private String codigoServicioSubItem;
    @JsonProperty("DescripcionSubItem")
    private String descripcionSubItem;
    @JsonProperty("CantidadSubItem")
    private String cantidadSubItem;
    @JsonProperty("UnidadMedidaSubItem")
    private String unidadMedidaSubItem;
    @JsonProperty("SubTotalSubItem")
    private String subTotalSubItem;
    @JsonProperty("PrecioSubItem")
    private String precioSubItem;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("NumeroSubItem")
    public String getNumeroSubItem() {
        return numeroSubItem;
    }

    @JsonProperty("NumeroSubItem")
    public void setNumeroSubItem(String numeroSubItem) {
        this.numeroSubItem = numeroSubItem;
    }

    @JsonProperty("CodigoServicioSubItem")
    public String getCodigoServicioSubItem() {
        return codigoServicioSubItem;
    }

    @JsonProperty("CodigoServicioSubItem")
    public void setCodigoServicioSubItem(String codigoServicioSubItem) {
        this.codigoServicioSubItem = codigoServicioSubItem;
    }

    @JsonProperty("DescripcionSubItem")
    public String getDescripcionSubItem() {
        return descripcionSubItem;
    }

    @JsonProperty("DescripcionSubItem")
    public void setDescripcionSubItem(String descripcionSubItem) {
        this.descripcionSubItem = descripcionSubItem;
    }

    @JsonProperty("CantidadSubItem")
    public String getCantidadSubItem() {
        return cantidadSubItem;
    }

    @JsonProperty("CantidadSubItem")
    public void setCantidadSubItem(String cantidadSubItem) {
        this.cantidadSubItem = cantidadSubItem;
    }

    @JsonProperty("UnidadMedidaSubItem")
    public String getUnidadMedidaSubItem() {
        return unidadMedidaSubItem;
    }

    @JsonProperty("UnidadMedidaSubItem")
    public void setUnidadMedidaSubItem(String unidadMedidaSubItem) {
        this.unidadMedidaSubItem = unidadMedidaSubItem;
    }

    @JsonProperty("SubTotalSubItem")
    public String getSubTotalSubItem() {
        return subTotalSubItem;
    }

    @JsonProperty("SubTotalSubItem")
    public void setSubTotalSubItem(String subTotalSubItem) {
        this.subTotalSubItem = subTotalSubItem;
    }

    @JsonProperty("PrecioSubItem")
    public String getPrecioSubItem() {
        return precioSubItem;
    }

    @JsonProperty("PrecioSubItem")
    public void setPrecioSubItem(String precioSubItem) {
        this.precioSubItem = precioSubItem;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
