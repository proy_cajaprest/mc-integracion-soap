
package com.ebiz.soap.integration.api.comprobantepago.model;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "NumeroGuiaERP",
    "EjercicioGuiaERP",
    "PosicionGuiaERP",
    "CantidadGuiaERP",
    "SubTotalItemGuiaERP"
})
public class DocumentoMaterial {

    @JsonProperty("NumeroGuiaERP")
    private String numeroGuiaERP;
    @JsonProperty("EjercicioGuiaERP")
    private String ejercicioGuiaERP;
    @JsonProperty("PosicionGuiaERP")
    private String posicionGuiaERP;
    @JsonProperty("CantidadGuiaERP")
    private String cantidadGuiaERP;
    @JsonProperty("SubTotalItemGuiaERP")
    private String subTotalItemGuiaERP;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("NumeroGuiaERP")
    public String getNumeroGuiaERP() {
        return numeroGuiaERP;
    }

    @JsonProperty("NumeroGuiaERP")
    public void setNumeroGuiaERP(String numeroGuiaERP) {
        this.numeroGuiaERP = numeroGuiaERP;
    }

    @JsonProperty("EjercicioGuiaERP")
    public String getEjercicioGuiaERP() {
        return ejercicioGuiaERP;
    }

    @JsonProperty("EjercicioGuiaERP")
    public void setEjercicioGuiaERP(String ejercicioGuiaERP) {
        this.ejercicioGuiaERP = ejercicioGuiaERP;
    }

    @JsonProperty("PosicionGuiaERP")
    public String getPosicionGuiaERP() {
        return posicionGuiaERP;
    }

    @JsonProperty("PosicionGuiaERP")
    public void setPosicionGuiaERP(String posicionGuiaERP) {
        this.posicionGuiaERP = posicionGuiaERP;
    }

    @JsonProperty("CantidadGuiaERP")
    public String getCantidadGuiaERP() {
        return cantidadGuiaERP;
    }

    @JsonProperty("CantidadGuiaERP")
    public void setCantidadGuiaERP(String cantidadGuiaERP) {
        this.cantidadGuiaERP = cantidadGuiaERP;
    }

    @JsonProperty("SubTotalItemGuiaERP")
    public String getSubTotalItemGuiaERP() {
        return subTotalItemGuiaERP;
    }

    @JsonProperty("SubTotalItemGuiaERP")
    public void setSubTotalItemGuiaERP(String subTotalItemGuiaERP) {
        this.subTotalItemGuiaERP = subTotalItemGuiaERP;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
