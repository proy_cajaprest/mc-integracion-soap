
package com.ebiz.soap.integration.api.comprobantepago.model;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "NumGuiaItem",
    "NumeroItemOC",
    "NumeroParte",
    "DescripcionItem",
    "CantidadItem",
    "PrecioUnitario",
    "PrecioTotal",
    "NumeroGuiaItem",
    "NumeroOcItem",
    "Posicion",
    "CodigoGuiaERP",
    "EjercicioGuia",
    "UnidadMedidaItem",
    "TipoItem",
    "DocumentoMaterial"
})
public class ItemFactura {

    @JsonProperty("NumGuiaItem")
    private String numGuiaItem;
    @JsonProperty("NumeroItemOC")
    private String numeroItemOC;
    @JsonProperty("NumeroParte")
    private String numeroParte;
    @JsonProperty("DescripcionItem")
    private String descripcionItem;
    @JsonProperty("CantidadItem")
    private String cantidadItem;
    @JsonProperty("PrecioUnitario")
    private String precioUnitario;
    @JsonProperty("PrecioTotal")
    private String precioTotal;
    @JsonProperty("NumeroGuiaItem")
    private String numeroGuiaItem;
    @JsonProperty("NumeroOcItem")
    private String numeroOcItem;
    @JsonProperty("Posicion")
    private String posicion;
    @JsonProperty("CodigoGuiaERP")
    private String codigoGuiaERP;
    @JsonProperty("EjercicioGuia")
    private String ejercicioGuia;
    @JsonProperty("UnidadMedidaItem")
    private String unidadMedidaItem;
    @JsonProperty("TipoItem")
    private String tipoItem;
    @JsonProperty("DocumentoMaterial")
    private List<DocumentoMaterial> documentoMaterial = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("NumGuiaItem")
    public String getNumGuiaItem() {
        return numGuiaItem;
    }

    @JsonProperty("NumGuiaItem")
    public void setNumGuiaItem(String numGuiaItem) {
        this.numGuiaItem = numGuiaItem;
    }

    @JsonProperty("NumeroItemOC")
    public String getNumeroItemOC() {
        return numeroItemOC;
    }

    @JsonProperty("NumeroItemOC")
    public void setNumeroItemOC(String numeroItemOC) {
        this.numeroItemOC = numeroItemOC;
    }

    @JsonProperty("NumeroParte")
    public String getNumeroParte() {
        return numeroParte;
    }

    @JsonProperty("NumeroParte")
    public void setNumeroParte(String numeroParte) {
        this.numeroParte = numeroParte;
    }

    @JsonProperty("DescripcionItem")
    public String getDescripcionItem() {
        return descripcionItem;
    }

    @JsonProperty("DescripcionItem")
    public void setDescripcionItem(String descripcionItem) {
        this.descripcionItem = descripcionItem;
    }

    @JsonProperty("CantidadItem")
    public String getCantidadItem() {
        return cantidadItem;
    }

    @JsonProperty("CantidadItem")
    public void setCantidadItem(String cantidadItem) {
        this.cantidadItem = cantidadItem;
    }

    @JsonProperty("PrecioUnitario")
    public String getPrecioUnitario() {
        return precioUnitario;
    }

    @JsonProperty("PrecioUnitario")
    public void setPrecioUnitario(String precioUnitario) {
        this.precioUnitario = precioUnitario;
    }

    @JsonProperty("PrecioTotal")
    public String getPrecioTotal() {
        return precioTotal;
    }

    @JsonProperty("PrecioTotal")
    public void setPrecioTotal(String precioTotal) {
        this.precioTotal = precioTotal;
    }

    @JsonProperty("NumeroGuiaItem")
    public String getNumeroGuiaItem() {
        return numeroGuiaItem;
    }

    @JsonProperty("NumeroGuiaItem")
    public void setNumeroGuiaItem(String numeroGuiaItem) {
        this.numeroGuiaItem = numeroGuiaItem;
    }

    @JsonProperty("NumeroOcItem")
    public String getNumeroOcItem() {
        return numeroOcItem;
    }

    @JsonProperty("NumeroOcItem")
    public void setNumeroOcItem(String numeroOcItem) {
        this.numeroOcItem = numeroOcItem;
    }

    @JsonProperty("Posicion")
    public String getPosicion() {
        return posicion;
    }

    @JsonProperty("Posicion")
    public void setPosicion(String posicion) {
        this.posicion = posicion;
    }

    @JsonProperty("CodigoGuiaERP")
    public String getCodigoGuiaERP() {
        return codigoGuiaERP;
    }

    @JsonProperty("CodigoGuiaERP")
    public void setCodigoGuiaERP(String codigoGuiaERP) {
        this.codigoGuiaERP = codigoGuiaERP;
    }

    @JsonProperty("EjercicioGuia")
    public String getEjercicioGuia() {
        return ejercicioGuia;
    }

    @JsonProperty("EjercicioGuia")
    public void setEjercicioGuia(String ejercicioGuia) {
        this.ejercicioGuia = ejercicioGuia;
    }

    @JsonProperty("UnidadMedidaItem")
    public String getUnidadMedidaItem() {
        return unidadMedidaItem;
    }

    @JsonProperty("UnidadMedidaItem")
    public void setUnidadMedidaItem(String unidadMedidaItem) {
        this.unidadMedidaItem = unidadMedidaItem;
    }

    @JsonProperty("TipoItem")
    public String getTipoItem() {
        return tipoItem;
    }

    @JsonProperty("TipoItem")
    public void setTipoItem(String tipoItem) {
        this.tipoItem = tipoItem;
    }

    @JsonProperty("DocumentoMaterial")
    public List<DocumentoMaterial> getDocumentoMaterial() {
        return documentoMaterial;
    }

    @JsonProperty("DocumentoMaterial")
    public void setDocumentoMaterial(List<DocumentoMaterial> documentoMaterial) {
        this.documentoMaterial = documentoMaterial;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
