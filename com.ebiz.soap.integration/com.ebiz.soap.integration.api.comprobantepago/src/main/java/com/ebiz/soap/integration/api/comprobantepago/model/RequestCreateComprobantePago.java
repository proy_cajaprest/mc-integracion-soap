
package com.ebiz.soap.integration.api.comprobantepago.model;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "FACUPLOADMQ"
})
public class RequestCreateComprobantePago {

    @JsonProperty("FACUPLOADMQ")
    private FACUPLOADMQ fACUPLOADMQ;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("FACUPLOADMQ")
    public FACUPLOADMQ getFACUPLOADMQ() {
        return fACUPLOADMQ;
    }

    @JsonProperty("FACUPLOADMQ")
    public void setFACUPLOADMQ(FACUPLOADMQ fACUPLOADMQ) {
        this.fACUPLOADMQ = fACUPLOADMQ;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
