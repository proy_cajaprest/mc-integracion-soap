
package com.ebiz.soap.integration.api.comprobantepago.model;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "NumeroFactura",
    "IdOrgCliente",
    "idComprobante",
    "NumeroGuia",
    "NumeroOC",
    "RucCliente",
    "RazonSocialCliente",
    "FechaCreacion",
    "FechaEnvio",
    "FechaEmision",
    "PlazoPago",
    "PeriodoPlazoPago",
    "CondicionPago",
    "SubTotal",
    "Impuesto1",
    "Impuesto2",
    "Impuesto3",
    "ImporteReferencial",
    "ImpuestoGVR",
    "Total",
    "Observaciones",
    "FechaVencimiento",
    "FechaRecepcion",
    "FechaRegistro",
    "FechaPago",
    "FechaProgramadaPago",
    "FormaPago",
    "DocumentoPago",
    "DocumentoERP",
    "PagoTipoDocumento",
    "PagoNroDocumento",
    "PagoMoneda",
    "PagoMontoPagado",
    "PagoBanco",
    "DctoTipoDocumento",
    "DctoNroDocumento",
    "DctoMoneda",
    "DctoMonto",
    "Almacen",
    "IdCierres",
    "Status",
    "Estado",
    "EstadoComprador",
    "CodigoERPProveedor",
    "CodigoSociedad",
    "IndicadorDetraccion",
    "CodigoBien",
    "DescripcionBien",
    "PorcentajeDetraccion",
    "IdCondicionPago",
    "DescripcionCondicionPago",
    "IndImpuesto",
    "TipoRegistro",
    "CodigoErp",
    "NumeroNota",
    "TipoNota",
    "Motivo",
    "Actividad",
    "PorcentajeImpuestoRetenido",
    "TipoComprobante",
    "Moneda",
    "LlaveERP",
    "ItemFactura"
})
public class Factura {

    @JsonProperty("NumeroFactura")
    private String numeroFactura;
    @JsonProperty("idComprobante")
    private String idComprobante;
    @JsonProperty("IdOrgCliente")
    private String IdOrgCliente;
    @JsonProperty("NumeroGuia")
    private String numeroGuia;
    @JsonProperty("NumeroOC")
    private String numeroOC;
    @JsonProperty("RucCliente")
    private String rucCliente;
    @JsonProperty("RazonSocialCliente")
    private String razonSocialCliente;
    @JsonProperty("FechaCreacion")
    private String fechaCreacion;
    @JsonProperty("FechaEnvio")
    private String fechaEnvio;
    @JsonProperty("FechaEmision")
    private String fechaEmision;
    @JsonProperty("PlazoPago")
    private String plazoPago;
    @JsonProperty("PeriodoPlazoPago")
    private String periodoPlazoPago;
    @JsonProperty("CondicionPago")
    private String condicionPago;
    @JsonProperty("SubTotal")
    private String subTotal;
    @JsonProperty("Impuesto1")
    private String impuesto1;
    @JsonProperty("Impuesto2")
    private String impuesto2;
    @JsonProperty("Impuesto3")
    private String impuesto3;
    @JsonProperty("ImporteReferencial")
    private String importeReferencial;
    @JsonProperty("ImpuestoGVR")
    private String impuestoGVR;
    @JsonProperty("Total")
    private String total;
    @JsonProperty("Observaciones")
    private String observaciones;
    @JsonProperty("FechaVencimiento")
    private String fechaVencimiento;
    @JsonProperty("FechaRecepcion")
    private String fechaRecepcion;
    @JsonProperty("FechaRegistro")
    private String fechaRegistro;
    @JsonProperty("FechaPago")
    private String fechaPago;
    @JsonProperty("FechaProgramadaPago")
    private String fechaProgramadaPago;
    @JsonProperty("FormaPago")
    private String formaPago;
    @JsonProperty("DocumentoPago")
    private String documentoPago;
    @JsonProperty("DocumentoERP")
    private String documentoERP;
    @JsonProperty("PagoTipoDocumento")
    private String pagoTipoDocumento;
    @JsonProperty("PagoNroDocumento")
    private String pagoNroDocumento;
    @JsonProperty("PagoMoneda")
    private String pagoMoneda;
    @JsonProperty("PagoMontoPagado")
    private String pagoMontoPagado;
    @JsonProperty("PagoBanco")
    private String pagoBanco;
    @JsonProperty("DctoTipoDocumento")
    private String dctoTipoDocumento;
    @JsonProperty("DctoNroDocumento")
    private String dctoNroDocumento;
    @JsonProperty("DctoMoneda")
    private String dctoMoneda;
    @JsonProperty("DctoMonto")
    private String dctoMonto;
    @JsonProperty("Almacen")
    private String almacen;
    @JsonProperty("IdCierres")
    private String idCierres;
    @JsonProperty("Status")
    private String status;
    @JsonProperty("Estado")
    private String estado;
    @JsonProperty("EstadoComprador")
    private String estadoComprador;
    @JsonProperty("CodigoERPProveedor")
    private String codigoERPProveedor;
    @JsonProperty("CodigoSociedad")
    private String codigoSociedad;
    @JsonProperty("IndicadorDetraccion")
    private String indicadorDetraccion;
    @JsonProperty("CodigoBien")
    private String codigoBien;
    @JsonProperty("DescripcionBien")
    private String descripcionBien;
    @JsonProperty("PorcentajeDetraccion")
    private String porcentajeDetraccion;
    @JsonProperty("IdCondicionPago")
    private String idCondicionPago;
    @JsonProperty("DescripcionCondicionPago")
    private String descripcionCondicionPago;
    @JsonProperty("IndImpuesto")
    private String indImpuesto;
    @JsonProperty("TipoRegistro")
    private String tipoRegistro;
    @JsonProperty("CodigoErp")
    private String codigoErp;
    @JsonProperty("NumeroNota")
    private String numeroNota;
    @JsonProperty("TipoNota")
    private String tipoNota;
    @JsonProperty("Motivo")
    private String motivo;
    @JsonProperty("Actividad")
    private String actividad;
    @JsonProperty("PorcentajeImpuestoRetenido")
    private String porcentajeImpuestoRetenido;
    @JsonProperty("TipoComprobante")
    private String tipoComprobante;
    @JsonProperty("Moneda")
    private String moneda;
    @JsonProperty("LlaveERP")
    private String llaveERP;
    @JsonProperty("ItemFactura")
    private List<ItemFactura> itemFactura = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("NumeroFactura")
    public String getNumeroFactura() {
        return numeroFactura;
    }

    @JsonProperty("NumeroFactura")
    public void setNumeroFactura(String numeroFactura) {
        this.numeroFactura = numeroFactura;
    }

    @JsonProperty("idComprobante")
    public String getIdComprobante() {
        return idComprobante;
    }

    @JsonProperty("idComprobante")
    public void setIdComprobante(String idComprobante) {
        this.idComprobante = idComprobante;
    }

    @JsonProperty("IdOrgCliente")
    public String getIdOrgCliente() {
        return IdOrgCliente;
    }
    
    @JsonProperty("IdOrgCliente")
    public void setIdOrgCliente(String idOrgCliente) {
        IdOrgCliente = idOrgCliente;
    }

    @JsonProperty("NumeroGuia")
    public String getNumeroGuia() {
        return numeroGuia;
    }

    @JsonProperty("NumeroGuia")
    public void setNumeroGuia(String numeroGuia) {
        this.numeroGuia = numeroGuia;
    }

    @JsonProperty("NumeroOC")
    public String getNumeroOC() {
        return numeroOC;
    }

    @JsonProperty("NumeroOC")
    public void setNumeroOC(String numeroOC) {
        this.numeroOC = numeroOC;
    }

    @JsonProperty("RucCliente")
    public String getRucCliente() {
        return rucCliente;
    }

    @JsonProperty("RucCliente")
    public void setRucCliente(String rucCliente) {
        this.rucCliente = rucCliente;
    }

    @JsonProperty("RazonSocialCliente")
    public String getRazonSocialCliente() {
        return razonSocialCliente;
    }

    @JsonProperty("RazonSocialCliente")
    public void setRazonSocialCliente(String razonSocialCliente) {
        this.razonSocialCliente = razonSocialCliente;
    }
    
    @JsonProperty("FechaCreacion")
    public String getFechaCreacion() {
		return fechaCreacion;
	}
    
    @JsonProperty("FechaCreacion")
	public void setFechaCreacion(String fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	@JsonProperty("FechaEnvio")
    public String getFechaEnvio() {
        return fechaEnvio;
    }

    @JsonProperty("FechaEnvio")
    public void setFechaEnvio(String fechaEnvio) {
        this.fechaEnvio = fechaEnvio;
    }

    @JsonProperty("FechaEmision")
    public String getFechaEmision() {
        return fechaEmision;
    }

    @JsonProperty("FechaEmision")
    public void setFechaEmision(String fechaEmision) {
        this.fechaEmision = fechaEmision;
    }

    @JsonProperty("PlazoPago")
    public String getPlazoPago() {
        return plazoPago;
    }

    @JsonProperty("PlazoPago")
    public void setPlazoPago(String plazoPago) {
        this.plazoPago = plazoPago;
    }

    @JsonProperty("PeriodoPlazoPago")
    public String getPeriodoPlazoPago() {
        return periodoPlazoPago;
    }

    @JsonProperty("PeriodoPlazoPago")
    public void setPeriodoPlazoPago(String periodoPlazoPago) {
        this.periodoPlazoPago = periodoPlazoPago;
    }

    @JsonProperty("CondicionPago")
    public String getCondicionPago() {
        return condicionPago;
    }

    @JsonProperty("CondicionPago")
    public void setCondicionPago(String condicionPago) {
        this.condicionPago = condicionPago;
    }

    @JsonProperty("SubTotal")
    public String getSubTotal() {
        return subTotal;
    }

    @JsonProperty("SubTotal")
    public void setSubTotal(String subTotal) {
        this.subTotal = subTotal;
    }

    @JsonProperty("Impuesto1")
    public String getImpuesto1() {
        return impuesto1;
    }

    @JsonProperty("Impuesto1")
    public void setImpuesto1(String impuesto1) {
        this.impuesto1 = impuesto1;
    }

    @JsonProperty("Impuesto2")
    public String getImpuesto2() {
        return impuesto2;
    }

    @JsonProperty("Impuesto2")
    public void setImpuesto2(String impuesto2) {
        this.impuesto2 = impuesto2;
    }

    @JsonProperty("Impuesto3")
    public String getImpuesto3() {
        return impuesto3;
    }

    @JsonProperty("Impuesto3")
    public void setImpuesto3(String impuesto3) {
        this.impuesto3 = impuesto3;
    }

    @JsonProperty("ImporteReferencial")
    public String getImporteReferencial() {
        return importeReferencial;
    }

    @JsonProperty("ImporteReferencial")
    public void setImporteReferencial(String importeReferencial) {
        this.importeReferencial = importeReferencial;
    }

    @JsonProperty("ImpuestoGVR")
    public String getImpuestoGVR() {
        return impuestoGVR;
    }

    @JsonProperty("ImpuestoGVR")
    public void setImpuestoGVR(String impuestoGVR) {
        this.impuestoGVR = impuestoGVR;
    }

    @JsonProperty("Total")
    public String getTotal() {
        return total;
    }

    @JsonProperty("Total")
    public void setTotal(String total) {
        this.total = total;
    }

    @JsonProperty("Observaciones")
    public String getObservaciones() {
        return observaciones;
    }

    @JsonProperty("Observaciones")
    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    @JsonProperty("FechaVencimiento")
    public String getFechaVencimiento() {
        return fechaVencimiento;
    }

    @JsonProperty("FechaVencimiento")
    public void setFechaVencimiento(String fechaVencimiento) {
        this.fechaVencimiento = fechaVencimiento;
    }

    @JsonProperty("FechaRecepcion")
    public String getFechaRecepcion() {
        return fechaRecepcion;
    }

    @JsonProperty("FechaRecepcion")
    public void setFechaRecepcion(String fechaRecepcion) {
        this.fechaRecepcion = fechaRecepcion;
    }

    @JsonProperty("FechaRegistro")
    public String getFechaRegistro() {
        return fechaRegistro;
    }

    @JsonProperty("FechaRegistro")
    public void setFechaRegistro(String fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    @JsonProperty("FechaPago")
    public String getFechaPago() {
        return fechaPago;
    }

    @JsonProperty("FechaPago")
    public void setFechaPago(String fechaPago) {
        this.fechaPago = fechaPago;
    }

    @JsonProperty("FechaProgramadaPago")
    public String getFechaProgramadaPago() {
        return fechaProgramadaPago;
    }

    @JsonProperty("FechaProgramadaPago")
    public void setFechaProgramadaPago(String fechaProgramadaPago) {
        this.fechaProgramadaPago = fechaProgramadaPago;
    }

    @JsonProperty("FormaPago")
    public String getFormaPago() {
        return formaPago;
    }

    @JsonProperty("FormaPago")
    public void setFormaPago(String formaPago) {
        this.formaPago = formaPago;
    }

    @JsonProperty("DocumentoPago")
    public String getDocumentoPago() {
        return documentoPago;
    }

    @JsonProperty("DocumentoPago")
    public void setDocumentoPago(String documentoPago) {
        this.documentoPago = documentoPago;
    }

    @JsonProperty("DocumentoERP")
    public String getDocumentoERP() {
        return documentoERP;
    }

    @JsonProperty("DocumentoERP")
    public void setDocumentoERP(String documentoERP) {
        this.documentoERP = documentoERP;
    }

    @JsonProperty("PagoTipoDocumento")
    public String getPagoTipoDocumento() {
        return pagoTipoDocumento;
    }

    @JsonProperty("PagoTipoDocumento")
    public void setPagoTipoDocumento(String pagoTipoDocumento) {
        this.pagoTipoDocumento = pagoTipoDocumento;
    }

    @JsonProperty("PagoNroDocumento")
    public String getPagoNroDocumento() {
        return pagoNroDocumento;
    }

    @JsonProperty("PagoNroDocumento")
    public void setPagoNroDocumento(String pagoNroDocumento) {
        this.pagoNroDocumento = pagoNroDocumento;
    }

    @JsonProperty("PagoMoneda")
    public String getPagoMoneda() {
        return pagoMoneda;
    }

    @JsonProperty("PagoMoneda")
    public void setPagoMoneda(String pagoMoneda) {
        this.pagoMoneda = pagoMoneda;
    }

    @JsonProperty("PagoMontoPagado")
    public String getPagoMontoPagado() {
        return pagoMontoPagado;
    }

    @JsonProperty("PagoMontoPagado")
    public void setPagoMontoPagado(String pagoMontoPagado) {
        this.pagoMontoPagado = pagoMontoPagado;
    }

    @JsonProperty("PagoBanco")
    public String getPagoBanco() {
        return pagoBanco;
    }

    @JsonProperty("PagoBanco")
    public void setPagoBanco(String pagoBanco) {
        this.pagoBanco = pagoBanco;
    }

    @JsonProperty("DctoTipoDocumento")
    public String getDctoTipoDocumento() {
        return dctoTipoDocumento;
    }

    @JsonProperty("DctoTipoDocumento")
    public void setDctoTipoDocumento(String dctoTipoDocumento) {
        this.dctoTipoDocumento = dctoTipoDocumento;
    }

    @JsonProperty("DctoNroDocumento")
    public String getDctoNroDocumento() {
        return dctoNroDocumento;
    }

    @JsonProperty("DctoNroDocumento")
    public void setDctoNroDocumento(String dctoNroDocumento) {
        this.dctoNroDocumento = dctoNroDocumento;
    }

    @JsonProperty("DctoMoneda")
    public String getDctoMoneda() {
        return dctoMoneda;
    }

    @JsonProperty("DctoMoneda")
    public void setDctoMoneda(String dctoMoneda) {
        this.dctoMoneda = dctoMoneda;
    }

    @JsonProperty("DctoMonto")
    public String getDctoMonto() {
        return dctoMonto;
    }

    @JsonProperty("DctoMonto")
    public void setDctoMonto(String dctoMonto) {
        this.dctoMonto = dctoMonto;
    }

    @JsonProperty("Almacen")
    public String getAlmacen() {
        return almacen;
    }

    @JsonProperty("Almacen")
    public void setAlmacen(String almacen) {
        this.almacen = almacen;
    }

    @JsonProperty("IdCierres")
    public String getIdCierres() {
        return idCierres;
    }

    @JsonProperty("IdCierres")
    public void setIdCierres(String idCierres) {
        this.idCierres = idCierres;
    }

    @JsonProperty("Status")
    public String getStatus() {
        return status;
    }

    @JsonProperty("Status")
    public void setStatus(String status) {
        this.status = status;
    }

    @JsonProperty("Estado")
    public String getEstado() {
        return estado;
    }

    @JsonProperty("Estado")
    public void setEstado(String estado) {
        this.estado = estado;
    }

    @JsonProperty("EstadoComprador")
    public String getEstadoComprador() {
        return estadoComprador;
    }

    @JsonProperty("EstadoComprador")
    public void setEstadoComprador(String estadoComprador) {
        this.estadoComprador = estadoComprador;
    }

    @JsonProperty("CodigoERPProveedor")
    public String getCodigoERPProveedor() {
        return codigoERPProveedor;
    }

    @JsonProperty("CodigoERPProveedor")
    public void setCodigoERPProveedor(String codigoERPProveedor) {
        this.codigoERPProveedor = codigoERPProveedor;
    }

    @JsonProperty("CodigoSociedad")
    public String getCodigoSociedad() {
        return codigoSociedad;
    }

    @JsonProperty("CodigoSociedad")
    public void setCodigoSociedad(String codigoSociedad) {
        this.codigoSociedad = codigoSociedad;
    }

    @JsonProperty("IndicadorDetraccion")
    public String getIndicadorDetraccion() {
        return indicadorDetraccion;
    }

    @JsonProperty("IndicadorDetraccion")
    public void setIndicadorDetraccion(String indicadorDetraccion) {
        this.indicadorDetraccion = indicadorDetraccion;
    }

    @JsonProperty("CodigoBien")
    public String getCodigoBien() {
        return codigoBien;
    }

    @JsonProperty("CodigoBien")
    public void setCodigoBien(String codigoBien) {
        this.codigoBien = codigoBien;
    }

    @JsonProperty("DescripcionBien")
    public String getDescripcionBien() {
        return descripcionBien;
    }

    @JsonProperty("DescripcionBien")
    public void setDescripcionBien(String descripcionBien) {
        this.descripcionBien = descripcionBien;
    }

    @JsonProperty("PorcentajeDetraccion")
    public String getPorcentajeDetraccion() {
        return porcentajeDetraccion;
    }

    @JsonProperty("PorcentajeDetraccion")
    public void setPorcentajeDetraccion(String porcentajeDetraccion) {
        this.porcentajeDetraccion = porcentajeDetraccion;
    }

    @JsonProperty("IdCondicionPago")
    public String getIdCondicionPago() {
        return idCondicionPago;
    }

    @JsonProperty("IdCondicionPago")
    public void setIdCondicionPago(String idCondicionPago) {
        this.idCondicionPago = idCondicionPago;
    }

    @JsonProperty("DescripcionCondicionPago")
    public String getDescripcionCondicionPago() {
        return descripcionCondicionPago;
    }

    @JsonProperty("DescripcionCondicionPago")
    public void setDescripcionCondicionPago(String descripcionCondicionPago) {
        this.descripcionCondicionPago = descripcionCondicionPago;
    }

    @JsonProperty("IndImpuesto")
    public String getIndImpuesto() {
        return indImpuesto;
    }

    @JsonProperty("IndImpuesto")
    public void setIndImpuesto(String indImpuesto) {
        this.indImpuesto = indImpuesto;
    }

    @JsonProperty("TipoRegistro")
    public String getTipoRegistro() {
        return tipoRegistro;
    }

    @JsonProperty("TipoRegistro")
    public void setTipoRegistro(String tipoRegistro) {
        this.tipoRegistro = tipoRegistro;
    }

    @JsonProperty("CodigoErp")
    public String getCodigoErp() {
        return codigoErp;
    }

    @JsonProperty("CodigoErp")
    public void setCodigoErp(String codigoErp) {
        this.codigoErp = codigoErp;
    }

    @JsonProperty("NumeroNota")
    public String getNumeroNota() {
        return numeroNota;
    }

    @JsonProperty("NumeroNota")
    public void setNumeroNota(String numeroNota) {
        this.numeroNota = numeroNota;
    }

    @JsonProperty("TipoNota")
    public String getTipoNota() {
        return tipoNota;
    }

    @JsonProperty("TipoNota")
    public void setTipoNota(String tipoNota) {
        this.tipoNota = tipoNota;
    }

    @JsonProperty("Motivo")
    public String getMotivo() {
        return motivo;
    }

    @JsonProperty("Motivo")
    public void setMotivo(String motivo) {
        this.motivo = motivo;
    }

    @JsonProperty("Actividad")
    public String getActividad() {
        return actividad;
    }

    @JsonProperty("Actividad")
    public void setActividad(String actividad) {
        this.actividad = actividad;
    }

    @JsonProperty("PorcentajeImpuestoRetenido")
    public String getPorcentajeImpuestoRetenido() {
        return porcentajeImpuestoRetenido;
    }

    @JsonProperty("PorcentajeImpuestoRetenido")
    public void setPorcentajeImpuestoRetenido(String porcentajeImpuestoRetenido) {
        this.porcentajeImpuestoRetenido = porcentajeImpuestoRetenido;
    }

    @JsonProperty("TipoComprobante")
    public String getTipoComprobante() {
        return tipoComprobante;
    }

    @JsonProperty("TipoComprobante")
    public void setTipoComprobante(String tipoComprobante) {
        this.tipoComprobante = tipoComprobante;
    }

    @JsonProperty("Moneda")
    public String getMoneda() {
        return moneda;
    }

    @JsonProperty("Moneda")
    public void setMoneda(String moneda) {
        this.moneda = moneda;
    }
    
    @JsonProperty("LlaveERP")
    public String getLlaveERP() {
		return llaveERP;
	}
    
    @JsonProperty("LlaveERP")
	public void setLlaveERP(String llaveERP) {
		this.llaveERP = llaveERP;
	}

	@JsonProperty("ItemFactura")
    public List<ItemFactura> getItemFactura() {
        return itemFactura;
    }

    @JsonProperty("ItemFactura")
    public void setItemFactura(List<ItemFactura> itemFactura) {
        this.itemFactura = itemFactura;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }
}
