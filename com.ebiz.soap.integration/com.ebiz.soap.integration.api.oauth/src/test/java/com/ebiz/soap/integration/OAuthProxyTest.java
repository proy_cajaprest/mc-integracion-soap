package com.ebiz.soap.integration;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import com.ebiz.soap.integration.api.oauth.IOAuthProxy;
import com.ebiz.soap.integration.api.oauth.impl.OAuthProxyImpl;





@RunWith(SpringRunner.class)
@ActiveProfiles("Default")
public class OAuthProxyTest {

	
	@Autowired
	private IOAuthProxy oauthProxy;
	
	@MockBean
	private RestTemplate restTemplate;
	
	@Test
	public void sanityTest() {
		assertNotNull(oauthProxy);
	}
	
	@Test
	public void encodeBase64Test() {
		String base64Text = oauthProxy.encodeBase64("abc123");
		assertEquals(base64Text, "YWJjMTIz");
	}
	
	@Test
	public void decodeBase64Test() {
		String base64Text = oauthProxy.decodeBase64("cGFzc3dvcmRkZW1v");
		assertEquals(base64Text, "passworddemo");
	}
	
	
	@Profile("Default")
	@Configuration
	static public class ConfigTest001 {
		
		
		@Bean
		@Primary
		public IOAuthProxy oauthProxy() {
			return new OAuthProxyImpl();
		}
		
	}
}
