package com.ebiz.soap.integration.api.oauth.impl;

import java.nio.charset.StandardCharsets;
import java.util.Base64;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.ebiz.soap.integration.api.oauth.IOAuthProxy;
import com.ebiz.soap.integration.api.oauth.model.TokenResponse;


@Repository
public class OAuthProxyImpl implements IOAuthProxy {
	
	@Autowired
	private RestTemplate restTemplate;
	
	@Value("${api.oauth2.client.token-uri}")
	private String tokenUri;
	
	@Value("${api.oauth2.client.grant_type}")
	private String grantType;
	
	@Value("${api.oauth2.client.client_id}")
	private String clientId;
	
	@Value("${api.oauth2.client.client_secret}")
	private String clientSecret;
	
	
	@Override
	public String decodeBase64(String base64) {
		return new String(Base64.getDecoder().decode(base64)); 
	}
	
	@Override
	public String encodeBase64(String text) {
		return Base64.getEncoder().encodeToString(text.getBytes(StandardCharsets.UTF_8) );
	}
	
	@Override
	public TokenResponse login(String userName, String passwordBase64) {
		
		String encodedString = encodeBase64(String.format("%s:%s", clientId,clientSecret));
		
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", String.format("Basic %s",encodedString));
		headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
		
		MultiValueMap<String, String> params= new LinkedMultiValueMap<>();
		params.add("grant_type", grantType);
		params.add("client_id", clientId);
		params.add("client_secret", clientSecret);
		params.add("username", userName);
		params.add("password", decodeBase64(passwordBase64));
		
		HttpEntity<MultiValueMap<String, String>> requestEntity = new HttpEntity<>(params, headers);
		
		ResponseEntity<TokenResponse> response = restTemplate.exchange(tokenUri, HttpMethod.POST, requestEntity,TokenResponse.class);
		
		
		return response.getBody();
	}

}
