package com.ebiz.soap.integration.api.oauth;

import com.ebiz.soap.integration.api.oauth.model.TokenResponse;


public interface IOAuthProxy {
	
	String decodeBase64(String base64);
	String encodeBase64(String text);
	TokenResponse login(String userName,String passwordBase64);

}
