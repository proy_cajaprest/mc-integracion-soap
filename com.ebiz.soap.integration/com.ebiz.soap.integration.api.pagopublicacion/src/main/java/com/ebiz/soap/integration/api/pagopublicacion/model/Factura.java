package com.ebiz.soap.integration.api.pagopublicacion.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"NumeroFactura",
"RucProveedor",
"NumeroOC",
"FechaPago",
"ObservacionesPago",
"TipoPago",
"NroDocumentoPago",
"Banco",
"MontoPago",
"MonedaPago",
"TipoDescuento",
"NroDocumentoDescuento",
"MontoDescuento",
"MonedaDescuento"
})
public class Factura {

    @JsonProperty("NumeroFactura")
    private String numeroFactura;
    @JsonProperty("RucProveedor")
    private String rucProveedor;
    @JsonProperty("NumeroOC")
    private String numeroOC;
    @JsonProperty("FechaPago")
    private String fechaPago;
    @JsonProperty("ObservacionesPago")
    private String observacionesPago;
    @JsonProperty("TipoPago")
    private String tipoPago;
    @JsonProperty("NroDocumentoPago")
    private String nroDocumentoPago;
    @JsonProperty("Banco")
    private String banco;
    @JsonProperty("MontoPago")
    private String montoPago;
    @JsonProperty("MonedaPago")
    private String monedaPago;
    @JsonProperty("TipoDescuento")
    private String tipoDescuento;
    @JsonProperty("NroDocumentoDescuento")
    private String nroDocumentoDescuento;
    @JsonProperty("MontoDescuento")
    private String montoDescuento;
    @JsonProperty("MonedaDescuento")
    private String monedaDescuento;

    @JsonProperty("NumeroFactura")
    public String getNumeroFactura() {
        return numeroFactura;
    }

    @JsonProperty("NumeroFactura")
    public void setNumeroFactura(String numeroFactura) {
        this.numeroFactura = numeroFactura;
    }

    @JsonProperty("RucProveedor")
    public String getRucProveedor() {
        return rucProveedor;
    }

    @JsonProperty("RucProveedor")
    public void setRucProveedor(String rucProveedor) {
        this.rucProveedor = rucProveedor;
    }

    @JsonProperty("NumeroOC")
    public String getNumeroOC() {
        return numeroOC;
    }

    @JsonProperty("NumeroOC")
    public void setNumeroOC(String numeroOC) {
        this.numeroOC = numeroOC;
    }

    @JsonProperty("FechaPago")
    public String getFechaPago() {
        return fechaPago;
    }

    @JsonProperty("FechaPago")
    public void setFechaPago(String fechaPago) {
        this.fechaPago = fechaPago;
    }

    @JsonProperty("ObservacionesPago")
    public String getObservacionesPago() {
        return observacionesPago;
    }

    @JsonProperty("ObservacionesPago")
    public void setObservacionesPago(String observacionesPago) {
        this.observacionesPago = observacionesPago;
    }

    @JsonProperty("TipoPago")
    public String getTipoPago() {
        return tipoPago;
    }

    @JsonProperty("TipoPago")
    public void setTipoPago(String tipoPago) {
        this.tipoPago = tipoPago;
    }

    @JsonProperty("NroDocumentoPago")
    public String getNroDocumentoPago() {
        return nroDocumentoPago;
    }

    @JsonProperty("NroDocumentoPago")
    public void setNroDocumentoPago(String nroDocumentoPago) {
        this.nroDocumentoPago = nroDocumentoPago;
    }

    @JsonProperty("Banco")
    public String getBanco() {
        return banco;
    }

    @JsonProperty("Banco")
    public void setBanco(String banco) {
        this.banco = banco;
    }

    @JsonProperty("MontoPago")
    public String getMontoPago() {
        return montoPago;
    }

    @JsonProperty("MontoPago")
    public void setMontoPago(String montoPago) {
        this.montoPago = montoPago;
    }

    @JsonProperty("MonedaPago")
    public String getMonedaPago() {
        return monedaPago;
    }

    @JsonProperty("MonedaPago")
    public void setMonedaPago(String monedaPago) {
        this.monedaPago = monedaPago;
    }

    @JsonProperty("TipoDescuento")
    public String getTipoDescuento() {
        return tipoDescuento;
    }

    @JsonProperty("TipoDescuento")
    public void setTipoDescuento(String tipoDescuento) {
        this.tipoDescuento = tipoDescuento;
    }

    @JsonProperty("NroDocumentoDescuento")
    public String getNroDocumentoDescuento() {
        return nroDocumentoDescuento;
    }

    @JsonProperty("NroDocumentoDescuento")
    public void setNroDocumentoDescuento(String nroDocumentoDescuento) {
        this.nroDocumentoDescuento = nroDocumentoDescuento;
    }

    @JsonProperty("MontoDescuento")
    public String getMontoDescuento() {
        return montoDescuento;
    }

    @JsonProperty("MontoDescuento")
    public void setMontoDescuento(String montoDescuento) {
        this.montoDescuento = montoDescuento;
    }

    @JsonProperty("MonedaDescuento")
    public String getMonedaDescuento() {
        return monedaDescuento;
    }

    @JsonProperty("MonedaDescuento")
    public void setMonedaDescuento(String monedaDescuento) {
        this.monedaDescuento = monedaDescuento;
    }

}