package com.ebiz.soap.integration.api.pagopublicacion.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"FACACTTESORMQ"
})
public class RequestPagoPublicacion {

    @JsonProperty("FACACTTESORMQ")
    private FACACTTESORMQ fACACTTESORMQ;

    @JsonProperty("FACACTTESORMQ")
    public FACACTTESORMQ getFACACTTESORMQ() {
        return fACACTTESORMQ;
    }

    @JsonProperty("FACACTTESORMQ")
    public void setFACACTTESORMQ(FACACTTESORMQ fACACTTESORMQ) {
        this.fACACTTESORMQ = fACACTTESORMQ;
    }

}