package com.ebiz.soap.integration.api.pagopublicacion.model;

import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"RucCliente",
"Factura"
})
public class FACACTTESORMQ {

    @JsonProperty("RucCliente")
    private String rucCliente;
    @JsonProperty("Factura")
    private List<Factura> factura = null;

    @JsonProperty("RucCliente")
    public String getRucCliente() {
        return rucCliente;
    }

    @JsonProperty("RucCliente")
    public void setRucCliente(String rucCliente) {
        this.rucCliente = rucCliente;
    }

    @JsonProperty("Factura")
    public List<Factura> getFactura() {
        return factura;
    }

    @JsonProperty("Factura")
    public void setFactura(List<Factura> factura) {
        this.factura = factura;
    }

}