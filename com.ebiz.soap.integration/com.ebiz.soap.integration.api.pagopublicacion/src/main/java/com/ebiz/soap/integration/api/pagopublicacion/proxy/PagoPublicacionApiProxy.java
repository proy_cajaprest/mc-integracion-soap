package com.ebiz.soap.integration.api.pagopublicacion.proxy;

import com.ebiz.soap.integration.api.pagopublicacion.model.RequestPagoPublicacion;

public interface PagoPublicacionApiProxy {

	/***
	 * 
	 * @param organizationId
	 * @param request
	 * @param tokenBearer
	 */
	void save(String organizationId,RequestPagoPublicacion request, String tokenBearer);
	
}
