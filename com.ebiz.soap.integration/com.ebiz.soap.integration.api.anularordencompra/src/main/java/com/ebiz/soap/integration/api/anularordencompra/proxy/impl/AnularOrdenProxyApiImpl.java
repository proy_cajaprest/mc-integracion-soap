package com.ebiz.soap.integration.api.anularordencompra.proxy.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.ebiz.soap.integration.api.anularordencompra.model.OrdenRequestJson;
import com.ebiz.soap.integration.api.anularordencompra.model.RequestAnularOrden;
import com.ebiz.soap.integration.api.anularordencompra.proxy.AnularOrdenProxyApi;

@Repository
public class AnularOrdenProxyApiImpl implements AnularOrdenProxyApi{
	private static final String STATUS_CODE = "0000";
	private static final Logger logger = LoggerFactory.getLogger(AnularOrdenProxyApiImpl.class);
	
	@Autowired
	private RestTemplate restTemplate;

	@Value("${api.oc.client.cancel-uri}")
	private String cancelUri;
	
	private final static String TIPO_EMPRESA = "C";
	private final static String ORIGEN_DATOS = "ERP";
	
	@Override
	public void save(String organizationId, RequestAnularOrden request, String tokenBearer) {
		
		/*UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(cancelUri)
				.queryParam("accion", request.getPOCANCEL().getOrden().getAccion());*/
		
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.set("Authorization", String.format("Bearer %s", tokenBearer));
		headers.set("origen_datos", ORIGEN_DATOS);
		headers.set("tipo_empresa", TIPO_EMPRESA);
		headers.set("org_id", organizationId);

		
		OrdenRequestJson requestJson = new OrdenRequestJson();
		requestJson.setAccion(request.getPOCANCEL().getOrden().getAccion());
		requestJson.setNumeroseguimiento(request.getPOCANCEL().getOrden().getNumeroOrden());
		requestJson.setEstadoactual(request.getPOCANCEL().getOrden().getEstadoactual());
		requestJson.setIddoc(request.getPOCANCEL().getOrden().getIddoc());
		
		HttpEntity<OrdenRequestJson> entity = new HttpEntity<>(requestJson, headers);

		ResponseEntity<String> response = null;

		try {
			response = restTemplate.exchange(cancelUri, HttpMethod.PUT, entity, String.class);
		} catch (Exception ex) {
			logger.error("save", ex);
		}

		if (response != null && response.getStatusCode() == HttpStatus.OK) {// &&
																			// STATUS_CODE.compareTo(response.getBody().getStatuscode())==0
																			// &&
																			// !response.getBody().getData().isEmpty())
																			// {

		}
	}
}
