package com.ebiz.soap.integration.api.anularordencompra.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class OrdenRequestJson {

	
	 	@JsonProperty("iddoc")
	    private String iddoc;
	    
	 	@JsonProperty("numeroseguimiento")
	    private String numeroseguimiento;
	    
	 	@JsonProperty("estadoactual")
	    private String estadoactual;
	    
	 	@JsonProperty("accion")
	    private String accion;

		public String getIddoc() {
			return iddoc;
		}

		public void setIddoc(String iddoc) {
			this.iddoc = iddoc;
		}

		public String getNumeroseguimiento() {
			return numeroseguimiento;
		}

		public void setNumeroseguimiento(String numeroseguimiento) {
			this.numeroseguimiento = numeroseguimiento;
		}

		public String getEstadoactual() {
			return estadoactual;
		}

		public void setEstadoactual(String estadoactual) {
			this.estadoactual = estadoactual;
		}

		public String getAccion() {
			return accion;
		}

		public void setAccion(String accion) {
			this.accion = accion;
		}
	 	
	 	
	 	
	    
}
