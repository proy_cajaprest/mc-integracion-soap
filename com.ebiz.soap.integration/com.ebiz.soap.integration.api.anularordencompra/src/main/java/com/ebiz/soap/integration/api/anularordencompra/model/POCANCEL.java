
package com.ebiz.soap.integration.api.anularordencompra.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "CompradorOrgID",
    "CompradorUsuarioID",
    "Orden"
})
public class POCANCEL {

    @JsonProperty("CompradorOrgID")
    private String compradorOrgID;
    @JsonProperty("CompradorUsuarioID")
    private String compradorUsuarioID;
    @JsonProperty("Orden")
    private Orden orden;

    @JsonProperty("CompradorOrgID")
    public String getCompradorOrgID() {
        return compradorOrgID;
    }

    @JsonProperty("CompradorOrgID")
    public void setCompradorOrgID(String compradorOrgID) {
        this.compradorOrgID = compradorOrgID;
    }

    @JsonProperty("CompradorUsuarioID")
    public String getCompradorUsuarioID() {
        return compradorUsuarioID;
    }

    @JsonProperty("CompradorUsuarioID")
    public void setCompradorUsuarioID(String compradorUsuarioID) {
        this.compradorUsuarioID = compradorUsuarioID;
    }

    @JsonProperty("Orden")
    public Orden getOrden() {
        return orden;
    }

    @JsonProperty("Orden")
    public void setOrden(Orden orden) {
        this.orden = orden;
    }

}
