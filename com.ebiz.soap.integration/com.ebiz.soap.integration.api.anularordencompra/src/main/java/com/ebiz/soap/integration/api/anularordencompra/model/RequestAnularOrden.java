
package com.ebiz.soap.integration.api.anularordencompra.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "POCANCEL"
})
public class RequestAnularOrden {

    @JsonProperty("POCANCEL")
    private POCANCEL pOCANCEL;

    @JsonProperty("POCANCEL")
    public POCANCEL getPOCANCEL() {
        return pOCANCEL;
    }

    @JsonProperty("POCANCEL")
    public void setPOCANCEL(POCANCEL pOCANCEL) {
        this.pOCANCEL = pOCANCEL;
    }

}
