package com.ebiz.soap.integration.api.anularordencompra.proxy;

import com.ebiz.soap.integration.api.anularordencompra.model.RequestAnularOrden;

public interface AnularOrdenProxyApi {
	void save(String organizationId,RequestAnularOrden request, String tokenBearer);
}
