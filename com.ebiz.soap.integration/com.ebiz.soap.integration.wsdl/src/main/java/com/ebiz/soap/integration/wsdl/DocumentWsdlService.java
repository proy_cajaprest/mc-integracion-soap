package com.ebiz.soap.integration.wsdl;

import com.ebiz.soap.integration.wsdl.exception.DocumentoRequestException;
import com.ebizlatin.xsd.DocumentoRequest;

public interface DocumentWsdlService {
	
	
	DocumentoRequest unmarshalDocumentoRequest(String request) throws DocumentoRequestException;
	String marshalDocumentoRequest(DocumentoRequest documentoRequest) throws DocumentoRequestException;
}
