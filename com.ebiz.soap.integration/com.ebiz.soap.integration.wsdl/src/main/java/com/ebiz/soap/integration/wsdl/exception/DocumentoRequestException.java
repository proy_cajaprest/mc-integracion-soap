package com.ebiz.soap.integration.wsdl.exception;

public class DocumentoRequestException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public DocumentoRequestException(String message, String request, Throwable cause) {
		super(message, cause);
		this.request = request;
	}

	public DocumentoRequestException(String message, Throwable cause) {
		super(message, cause);
		this.request = null;
	}

	private final String request;

	public String getRequest() {
		return request;
	}

}
