package com.ebiz.soap.integration.wsdl.impl;

import java.io.StringReader;
import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ebiz.soap.integration.wsdl.DocumentWsdlService;
import com.ebiz.soap.integration.wsdl.exception.DocumentoRequestException;
import com.ebizlatin.xsd.DocumentoRequest;

public class DocumentWsdlServiceImpl implements DocumentWsdlService {
	
	
	private static final Logger LOGGER = LoggerFactory.getLogger(DocumentWsdlServiceImpl.class);

	@Override
	public DocumentoRequest unmarshalDocumentoRequest(String request) throws DocumentoRequestException {
		
		final StringReader xmlReader = new StringReader(request);
		final StreamSource xmlSource = new StreamSource(xmlReader);
		
		DocumentoRequest documentoRequest = null;
		JAXBContext context;
		
		try {
			context = JAXBContext.newInstance(DocumentoRequest.class);
			Unmarshaller unmarshaller = context.createUnmarshaller();
			documentoRequest = (DocumentoRequest) unmarshaller.unmarshal(xmlSource,DocumentoRequest.class).getValue();
		} catch (JAXBException e) {
			LOGGER.error("transformRequest-error", e);
			throw new DocumentoRequestException("transformRequest-error",request,e);
		}
		
		return documentoRequest;
	}

	@Override
	public String marshalDocumentoRequest(DocumentoRequest documentoRequest) throws DocumentoRequestException {
		
		String payload=null;

		try {
			JAXBContext context = JAXBContext.newInstance(DocumentoRequest.class);
			Marshaller marshaller = context.createMarshaller();
			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

			StringWriter sw = new StringWriter();
			marshaller.marshal(documentoRequest, sw);

			 payload =	StringUtils.replaceEach(sw.toString(), new String[]{"<![CDATA[", "]]>","&lt;![CDATA[","]]&gt;"}, new String[]{"", "","",""});


		} catch (JAXBException ex) {
			LOGGER.error("Error generando payload", ex);
			throw new DocumentoRequestException("transformRequest-error",ex);
		}
			
		return payload;
		
	}


}
