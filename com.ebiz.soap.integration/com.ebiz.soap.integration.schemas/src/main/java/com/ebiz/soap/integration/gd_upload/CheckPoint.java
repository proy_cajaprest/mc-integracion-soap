//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.11 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2017.12.02 at 08:28:19 PM PET 
//


package com.ebiz.soap.integration.gd_upload;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{}NumeroItemOCChk"/&gt;
 *         &lt;element ref="{}FechaGuiaDespachoChk"/&gt;
 *         &lt;element ref="{}CantidadDespachadaChk"/&gt;
 *         &lt;element ref="{}UnidadMedidaChk"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "numeroItemOCChk",
    "fechaGuiaDespachoChk",
    "cantidadDespachadaChk",
    "unidadMedidaChk"
})
@XmlRootElement(name = "CheckPoint")
public class CheckPoint {

    @XmlElement(name = "NumeroItemOCChk", required = true)
    protected String numeroItemOCChk;
    @XmlElement(name = "FechaGuiaDespachoChk", required = true)
    protected String fechaGuiaDespachoChk;
    @XmlElement(name = "CantidadDespachadaChk", required = true)
    protected String cantidadDespachadaChk;
    @XmlElement(name = "UnidadMedidaChk", required = true)
    protected String unidadMedidaChk;

    /**
     * Gets the value of the numeroItemOCChk property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumeroItemOCChk() {
        return numeroItemOCChk;
    }

    /**
     * Sets the value of the numeroItemOCChk property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumeroItemOCChk(String value) {
        this.numeroItemOCChk = value;
    }

    /**
     * Gets the value of the fechaGuiaDespachoChk property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechaGuiaDespachoChk() {
        return fechaGuiaDespachoChk;
    }

    /**
     * Sets the value of the fechaGuiaDespachoChk property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaGuiaDespachoChk(String value) {
        this.fechaGuiaDespachoChk = value;
    }

    /**
     * Gets the value of the cantidadDespachadaChk property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCantidadDespachadaChk() {
        return cantidadDespachadaChk;
    }

    /**
     * Sets the value of the cantidadDespachadaChk property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCantidadDespachadaChk(String value) {
        this.cantidadDespachadaChk = value;
    }

    /**
     * Gets the value of the unidadMedidaChk property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUnidadMedidaChk() {
        return unidadMedidaChk;
    }

    /**
     * Sets the value of the unidadMedidaChk property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUnidadMedidaChk(String value) {
        this.unidadMedidaChk = value;
    }

}
