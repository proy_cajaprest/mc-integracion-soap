package com.ebiz.soap.integration.schemas;

import com.ebiz.soap.integration.fac_upload.FACUPLOADMQ;
import com.ebiz.soap.integration.gd_upload.GDUPLOADMQ;
import com.ebiz.soap.integration.mov_upload.MOVUPLOADMQ;
import com.ebiz.soap.integration.po_cancel_oc.POCANCEL;
import com.ebiz.soap.integration.po_upload.POUPLOADMQ;
import com.ebiz.soap.integration.fac_acttesor.FACACTTESORMQ;
import com.ebiz.soap.integration.fac_cambio_estado.FACCAMBIOESTADO;
import com.ebiz.soap.integration.schemas.exception.XmlRequestException;

public interface DocumentXsdService {

	POUPLOADMQ unmarshalOrdenCompraRequest(String request) throws XmlRequestException;
	String marshalDocumentoRequest(POUPLOADMQ request) throws XmlRequestException;

	FACUPLOADMQ unmarshalComprobantePagoRequest(String request) throws XmlRequestException;
	String marshalDocumentoRequest(FACUPLOADMQ request) throws XmlRequestException;

	MOVUPLOADMQ unmarshalMovimientoRequest(String request) throws XmlRequestException;
	String marshalDocumentoRequest(MOVUPLOADMQ request) throws XmlRequestException;

	FACACTTESORMQ unmarshalPagoPublicacionRequest(String request) throws XmlRequestException;
	String marshalDocumentoRequest(FACACTTESORMQ request) throws XmlRequestException;

	FACCAMBIOESTADO unmarshalFacturaAnulacionRequest(String request) throws XmlRequestException;
	String marshalDocumentoRequest(FACCAMBIOESTADO request) throws XmlRequestException;

	POCANCEL unmarshalAnularOrdenRequest(String request) throws XmlRequestException;
	String marshalDocumentoRequest(POCANCEL request) throws XmlRequestException;
	
	String marshalDocumentoRequest(GDUPLOADMQ request) throws XmlRequestException;

}
