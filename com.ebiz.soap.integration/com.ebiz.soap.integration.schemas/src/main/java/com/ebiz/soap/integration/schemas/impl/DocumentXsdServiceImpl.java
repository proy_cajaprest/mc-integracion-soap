package com.ebiz.soap.integration.schemas.impl;

import java.io.StringReader;
import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ebiz.soap.integration.fac_upload.FACUPLOADMQ;
import com.ebiz.soap.integration.gd_upload.GDUPLOADMQ;
import com.ebiz.soap.integration.mov_upload.MOVUPLOADMQ;
import com.ebiz.soap.integration.po_cancel_oc.POCANCEL;
import com.ebiz.soap.integration.po_upload.POUPLOADMQ;
import com.ebiz.soap.integration.fac_acttesor.FACACTTESORMQ;
import com.ebiz.soap.integration.fac_cambio_estado.FACCAMBIOESTADO;
import com.ebiz.soap.integration.schemas.DocumentXsdService;
import com.ebiz.soap.integration.schemas.exception.XmlRequestException;



public class DocumentXsdServiceImpl implements DocumentXsdService {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(DocumentXsdServiceImpl.class);

	
	@Override
	public POUPLOADMQ unmarshalOrdenCompraRequest(String request) throws XmlRequestException {
		
		
		final StringReader xmlReader = new StringReader(request);
		final StreamSource xmlSource = new StreamSource(xmlReader);
		
		POUPLOADMQ poUploadMq = null;
		JAXBContext context;
		
		try {
			context = JAXBContext.newInstance(POUPLOADMQ.class);
			Unmarshaller unmarshaller = context.createUnmarshaller();
			poUploadMq = (POUPLOADMQ) unmarshaller.unmarshal(xmlSource,POUPLOADMQ.class).getValue();
		} catch (JAXBException e) {
			LOGGER.error("transformRequest-error", e);
			throw new XmlRequestException("transformRequest-error",request,e);
		}
		
		return poUploadMq;
		
		
	}
	
	@Override
	public String marshalDocumentoRequest(POUPLOADMQ request) throws XmlRequestException {
		
		String payload=null;

		try {
			JAXBContext context = JAXBContext.newInstance(POUPLOADMQ.class);
			Marshaller marshaller = context.createMarshaller();
			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

			StringWriter sw = new StringWriter();
			marshaller.marshal(request, sw);

			payload = sw.toString();

		} catch (JAXBException ex) {
			LOGGER.error("Error generando payload", ex);
			throw new XmlRequestException("transformRequest-error",ex);
		}
			
		return payload;
		
	}

	@Override
	public FACUPLOADMQ unmarshalComprobantePagoRequest(String request) throws XmlRequestException {
		final StringReader xmlReader = new StringReader(request);
		final StreamSource xmlSource = new StreamSource(xmlReader);
		
		FACUPLOADMQ facUploadMQ = null;
		JAXBContext context;
		
		try {
			context = JAXBContext.newInstance(FACUPLOADMQ.class);
			Unmarshaller unmarshaller = context.createUnmarshaller();
			facUploadMQ = (FACUPLOADMQ) unmarshaller.unmarshal(xmlSource,FACUPLOADMQ.class).getValue();
		} catch (JAXBException e) {
			LOGGER.error("transformRequest-error", e);
			throw new XmlRequestException("transformRequest-error",request,e);
		}
		
		return facUploadMQ;
	}

	@Override
	public String marshalDocumentoRequest(FACUPLOADMQ request) throws XmlRequestException {

		String payload=null;

		try {
			JAXBContext context = JAXBContext.newInstance(FACUPLOADMQ.class);
			Marshaller marshaller = context.createMarshaller();
			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

			StringWriter sw = new StringWriter();
			marshaller.marshal(request, sw);

			payload = sw.toString();

		} catch (JAXBException ex) {
			LOGGER.error("Error generando payload", ex);
			throw new XmlRequestException("transformRequest-error",ex);
		}
			
		return payload;
	}

	@Override
	public MOVUPLOADMQ unmarshalMovimientoRequest(String request) throws XmlRequestException {
		final StringReader xmlReader = new StringReader(request);
		final StreamSource xmlSource = new StreamSource(xmlReader);
		
		MOVUPLOADMQ movUploadMq = null;
		JAXBContext context;
		
		try {
			context = JAXBContext.newInstance(MOVUPLOADMQ.class);
			Unmarshaller unmarshaller = context.createUnmarshaller();
			movUploadMq = (MOVUPLOADMQ) unmarshaller.unmarshal(xmlSource,MOVUPLOADMQ.class).getValue();
		} catch (JAXBException e) {
			LOGGER.error("transformRequest-error", e);
			throw new XmlRequestException("transformRequest-error",request,e);
		}
		
		return movUploadMq;
	}

	@Override
	public String marshalDocumentoRequest(MOVUPLOADMQ request) throws XmlRequestException {
		String payload=null;

		try {
			JAXBContext context = JAXBContext.newInstance(MOVUPLOADMQ.class);
			Marshaller marshaller = context.createMarshaller();
			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

			StringWriter sw = new StringWriter();
			marshaller.marshal(request, sw);

			payload = sw.toString();

		} catch (JAXBException ex) {
			LOGGER.error("Error generando payload", ex);
			throw new XmlRequestException("transformRequest-error",ex);
		}
			
		return payload;
	}

	@Override
	public FACACTTESORMQ unmarshalPagoPublicacionRequest(String request) throws XmlRequestException {
		final StringReader xmlReader = new StringReader(request);
		final StreamSource xmlSource = new StreamSource(xmlReader);
		
		FACACTTESORMQ facActContMQ = null;
		JAXBContext context;
		
		try {
			context = JAXBContext.newInstance(FACACTTESORMQ.class);
			Unmarshaller unmarshaller = context.createUnmarshaller();
			facActContMQ = (FACACTTESORMQ) unmarshaller.unmarshal(xmlSource,FACACTTESORMQ.class).getValue();
		} catch (JAXBException e) {
			LOGGER.error("transformRequest-error", e);
			throw new XmlRequestException("transformRequest-error",request,e);
		}
		
		return facActContMQ;
	}

	@Override
	public String marshalDocumentoRequest(FACACTTESORMQ request) throws XmlRequestException {
		String payload=null;

		try {
			JAXBContext context = JAXBContext.newInstance(FACACTTESORMQ.class);
			Marshaller marshaller = context.createMarshaller();
			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

			StringWriter sw = new StringWriter();
			marshaller.marshal(request, sw);

			payload = sw.toString();

		} catch (JAXBException ex) {
			LOGGER.error("Error generando payload", ex);
			throw new XmlRequestException("transformRequest-error",ex);
		}
			
		return payload;
	}
	
	@Override
	public FACCAMBIOESTADO unmarshalFacturaAnulacionRequest(String request) throws XmlRequestException {
		final StringReader xmlReader = new StringReader(request);
		final StreamSource xmlSource = new StreamSource(xmlReader);
		
		FACCAMBIOESTADO facCambioEstado = null;
		JAXBContext context;
		
		try {
			context = JAXBContext.newInstance(FACCAMBIOESTADO.class);
			Unmarshaller unmarshaller = context.createUnmarshaller();
			facCambioEstado = (FACCAMBIOESTADO) unmarshaller.unmarshal(xmlSource,FACCAMBIOESTADO.class).getValue();
		} catch (JAXBException e) {
			LOGGER.error("transformRequest-error", e);
			throw new XmlRequestException("transformRequest-error",request,e);
		}
		
		return facCambioEstado;
	}

	@Override
	public String marshalDocumentoRequest(FACCAMBIOESTADO request) throws XmlRequestException {
		String payload=null;

		try {
			JAXBContext context = JAXBContext.newInstance(FACCAMBIOESTADO.class);
			Marshaller marshaller = context.createMarshaller();
			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

			StringWriter sw = new StringWriter();
			marshaller.marshal(request, sw);

			payload = sw.toString();

		} catch (JAXBException ex) {
			LOGGER.error("Error generando payload", ex);
			throw new XmlRequestException("transformRequest-error",ex);
		}
			
		return payload;
	}

	@Override
	public POCANCEL unmarshalAnularOrdenRequest(String request) throws XmlRequestException {
		final StringReader xmlReader = new StringReader(request);
		final StreamSource xmlSource = new StreamSource(xmlReader);
		
		POCANCEL poCancel = null;
		JAXBContext context;
		
		try {
			context = JAXBContext.newInstance(POCANCEL.class);
			Unmarshaller unmarshaller = context.createUnmarshaller();
			poCancel = (POCANCEL) unmarshaller.unmarshal(xmlSource,POCANCEL.class).getValue();
		} catch (JAXBException e) {
			LOGGER.error("transformRequest-error", e);
			throw new XmlRequestException("transformRequest-error",request,e);
		}
		
		return poCancel;
	}

	@Override
	public String marshalDocumentoRequest(POCANCEL request) throws XmlRequestException {
		String payload=null;
		
				try {
					JAXBContext context = JAXBContext.newInstance(POCANCEL.class);
					Marshaller marshaller = context.createMarshaller();
					marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
		
					StringWriter sw = new StringWriter();
					marshaller.marshal(request, sw);
		
					payload = sw.toString();
		
				} catch (JAXBException ex) {
					LOGGER.error("Error generando payload", ex);
					throw new XmlRequestException("transformRequest-error",ex);
				}
					
				return payload;
	}
	

	@Override
	public String marshalDocumentoRequest(GDUPLOADMQ request) throws XmlRequestException {
		String payload=null;
		
		try {
			JAXBContext context = JAXBContext.newInstance(GDUPLOADMQ.class);
			Marshaller marshaller = context.createMarshaller();
			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

			StringWriter sw = new StringWriter();
			marshaller.marshal(request, sw);

			payload = sw.toString();

		} catch (JAXBException ex) {
			LOGGER.error("Error generando payload guia", ex);
			throw new XmlRequestException("transformRequest-error",ex);
		}
			
		return payload;
	}

}
