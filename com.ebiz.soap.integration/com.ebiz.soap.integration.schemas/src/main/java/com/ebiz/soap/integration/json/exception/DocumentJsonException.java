package com.ebiz.soap.integration.json.exception;

public class DocumentJsonException  extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public DocumentJsonException(String message, String request, Throwable cause) {
		super(message, cause);
		this.request = request;
	}

	public DocumentJsonException(String message, Throwable cause) {
		super(message, cause);
		this.request = null;
	}

	private final String request;

	public String getRequest() {
		return request;
	}

}
