package com.ebiz.soap.integration.schemas.exception;

public class XmlRequestException extends Exception {
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	public XmlRequestException(String message, String request) {
		super(message);
		this.request = request;
	}
	
	public XmlRequestException(String message, String request, Throwable cause) {
		super(message, cause);
		this.request = request;
	}

	public XmlRequestException(String message, Throwable cause) {
		super(message, cause);
		this.request = null;
	}

	private final String request;

	public String getRequest() {
		return request;
	}


}
