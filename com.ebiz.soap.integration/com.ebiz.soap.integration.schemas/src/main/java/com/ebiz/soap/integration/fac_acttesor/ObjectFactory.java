//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.11 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2017.12.02 at 08:28:15 PM PET 
//


package com.ebiz.soap.integration.fac_acttesor;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.ebiz.soap.integration.fac_acttesor package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _RucCliente_QNAME = new QName("", "RucCliente");
    private final static QName _NumeroFactura_QNAME = new QName("", "NumeroFactura");
    private final static QName _RucProveedor_QNAME = new QName("", "RucProveedor");
    private final static QName _NumeroOC_QNAME = new QName("", "NumeroOC");
    private final static QName _FechaPago_QNAME = new QName("", "FechaPago");
    private final static QName _ObservacionesPago_QNAME = new QName("", "ObservacionesPago");
    private final static QName _TipoPago_QNAME = new QName("", "TipoPago");
    private final static QName _NroDocumentoPago_QNAME = new QName("", "NroDocumentoPago");
    private final static QName _Banco_QNAME = new QName("", "Banco");
    private final static QName _MontoPago_QNAME = new QName("", "MontoPago");
    private final static QName _MonedaPago_QNAME = new QName("", "MonedaPago");
    private final static QName _Cheque_QNAME = new QName("", "Cheque");
    private final static QName _TipoDescuento_QNAME = new QName("", "TipoDescuento");
    private final static QName _NroDocumentoDescuento_QNAME = new QName("", "NroDocumentoDescuento");
    private final static QName _MontoDescuento_QNAME = new QName("", "MontoDescuento");
    private final static QName _MonedaDescuento_QNAME = new QName("", "MonedaDescuento");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.ebiz.soap.integration.fac_acttesor
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link FACACTTESORMQ }
     * 
     */
    public FACACTTESORMQ createFACACTTESORMQ() {
        return new FACACTTESORMQ();
    }

    /**
     * Create an instance of {@link Factura }
     * 
     */
    public Factura createFactura() {
        return new Factura();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "RucCliente")
    public JAXBElement<String> createRucCliente(String value) {
        return new JAXBElement<String>(_RucCliente_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "NumeroFactura")
    public JAXBElement<String> createNumeroFactura(String value) {
        return new JAXBElement<String>(_NumeroFactura_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "RucProveedor")
    public JAXBElement<String> createRucProveedor(String value) {
        return new JAXBElement<String>(_RucProveedor_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "NumeroOC")
    public JAXBElement<String> createNumeroOC(String value) {
        return new JAXBElement<String>(_NumeroOC_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "FechaPago")
    public JAXBElement<String> createFechaPago(String value) {
        return new JAXBElement<String>(_FechaPago_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ObservacionesPago")
    public JAXBElement<String> createObservacionesPago(String value) {
        return new JAXBElement<String>(_ObservacionesPago_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "TipoPago")
    public JAXBElement<String> createTipoPago(String value) {
        return new JAXBElement<String>(_TipoPago_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "NroDocumentoPago")
    public JAXBElement<String> createNroDocumentoPago(String value) {
        return new JAXBElement<String>(_NroDocumentoPago_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Banco")
    public JAXBElement<String> createBanco(String value) {
        return new JAXBElement<String>(_Banco_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "MontoPago")
    public JAXBElement<String> createMontoPago(String value) {
        return new JAXBElement<String>(_MontoPago_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "MonedaPago")
    public JAXBElement<String> createMonedaPago(String value) {
        return new JAXBElement<String>(_MonedaPago_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Cheque")
    public JAXBElement<String> createCheque(String value) {
        return new JAXBElement<String>(_Cheque_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "TipoDescuento")
    public JAXBElement<String> createTipoDescuento(String value) {
        return new JAXBElement<String>(_TipoDescuento_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "NroDocumentoDescuento")
    public JAXBElement<String> createNroDocumentoDescuento(String value) {
        return new JAXBElement<String>(_NroDocumentoDescuento_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "MontoDescuento")
    public JAXBElement<String> createMontoDescuento(String value) {
        return new JAXBElement<String>(_MontoDescuento_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "MonedaDescuento")
    public JAXBElement<String> createMonedaDescuento(String value) {
        return new JAXBElement<String>(_MonedaDescuento_QNAME, String.class, null, value);
    }

}
