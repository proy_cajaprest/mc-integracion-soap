package com.ebiz.soap.integration.json.impl;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;

import com.ebiz.soap.integration.json.DocumentoJsonService;
import com.ebiz.soap.integration.json.exception.DocumentJsonException;
import com.ebiz.soap.integration.json.exception.ParseJsonException;
import com.ebiz.soap.integration.json.model.DocumentJson;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class DocumentoJsonServiceImpl implements DocumentoJsonService {

	private static final Logger LOGGER = LoggerFactory.getLogger(DocumentoJsonServiceImpl.class);

	@Autowired
	private MappingJackson2HttpMessageConverter jacksonConverter;

	@Override
	public DocumentJson unmarshalDocumentRequest(String request) throws DocumentJsonException,ParseJsonException {
		

		ObjectMapper objectMapper = jacksonConverter.getObjectMapper();

		try {
			return objectMapper.readValue(request, DocumentJson.class);
		} catch (JsonParseException e) {
			LOGGER.error("unmarshalDocumentRequest", e);
			throw new DocumentJsonException("unmarshalDocumentRequest", request, e);
		} catch (JsonMappingException e) {
			LOGGER.error("unmarshalDocumentRequest", e);
			throw new DocumentJsonException("unmarshalDocumentRequest", request, e);
		} catch (IOException e) {
			LOGGER.error("unmarshalDocumentRequest", e);
			throw new DocumentJsonException("unmarshalDocumentRequest", request, e);
		}
	}

}
