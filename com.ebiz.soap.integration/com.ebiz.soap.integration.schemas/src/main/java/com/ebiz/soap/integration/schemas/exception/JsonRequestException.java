package com.ebiz.soap.integration.schemas.exception;

public class JsonRequestException extends Exception {
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public JsonRequestException(String message, String request, Throwable cause) {
		super(message, cause);
		this.request = request;
	}

	public JsonRequestException(String message, Throwable cause) {
		super(message, cause);
		this.request = null;
	}

	private final String request;

	public String getRequest() {
		return request;
	}


}
