package com.ebiz.soap.integration.json;

import com.ebiz.soap.integration.json.exception.DocumentJsonException;
import com.ebiz.soap.integration.json.exception.ParseJsonException;
import com.ebiz.soap.integration.json.model.DocumentJson;


public interface DocumentoJsonService {
	
	DocumentJson unmarshalDocumentRequest(String request) throws ParseJsonException,DocumentJsonException;
	

}
