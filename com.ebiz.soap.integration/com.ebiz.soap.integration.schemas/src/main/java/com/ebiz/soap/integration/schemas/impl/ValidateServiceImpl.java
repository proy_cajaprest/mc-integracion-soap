package com.ebiz.soap.integration.schemas.impl;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import com.ebiz.soap.integration.po_cancel_oc.POCANCEL;
import com.ebiz.soap.integration.schemas.DocumentXsdService;
import com.ebiz.soap.integration.schemas.ValidateService;
import com.ebiz.soap.integration.schemas.exception.XmlRequestException;
import com.ebiz.soap.integration.schemas.exception.XsdException;

public class ValidateServiceImpl implements ValidateService {

	private static final Logger LOGGER = LoggerFactory.getLogger(ValidateServiceImpl.class);
	
	@Autowired
	private DocumentXsdService documentXsdService;
	
	private void validate(String xml, String schemaFile) throws XmlRequestException, XsdException {

		Document document = null;

		xml = StringUtils.remove(xml, "<![CDATA[");
		xml = StringUtils.remove(xml, "]]>");

		try {
			document = loadXml(xml);
		} catch (Exception e) {
			LOGGER.error("validateOrdenCompra", "error validando xml", e);
			throw new XmlRequestException(e.getMessage(), xml, e);
		}

		try {
			validateUtil(document, schemaFile);
		} catch (Exception e) {
			LOGGER.error("validateOrdenCompra", "validateOrdenCompra", e);
			throw new XsdException(e.getMessage(), xml, e);

		}

	}

	private void validateUtil(Document document, String schemaFile) throws SAXException, IOException {
		SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
		Schema schema = factory.newSchema(new StreamSource(getInputStream(schemaFile)));

		Validator validator = schema.newValidator();
		validator.validate(new DOMSource(document));

	}

	private DocumentBuilder createDocumentBuilder() throws ParserConfigurationException {
		DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
		builderFactory.setNamespaceAware(true);
		return builderFactory.newDocumentBuilder();
	}

	private Document loadXml(String xmlToValidate) throws Exception {
		DocumentBuilder builder = createDocumentBuilder();
		return builder.parse(new InputSource(new StringReader(xmlToValidate)));
	}

	private InputStream getInputStream(String filename) {
		return getClass().getClassLoader().getResourceAsStream(filename);
	}

	@Override
	public void validateOrdenCompra(String xml) throws XmlRequestException, XsdException {

		validate(xml, "schemas/po_upload_mq.xsd");
	}

	@Override
	public void validateMovimiento(String xml) throws XmlRequestException, XsdException {
		validate(xml, "schemas/mov_upload_mq.xsd");

	}

	@Override
	public void validateCompraPago(String xml) throws XmlRequestException, XsdException {
		validate(xml, "schemas/fac_upload_mq.xsd");

	}

	@Override
	public void validateAnularOrdenCompra(String xml) throws XmlRequestException, XsdException {
		
		if (StringUtils.isBlank(xml)) {
			throw new XmlRequestException("POCANCEL is null", xml);
		}
		
		validate(xml, "schemas/po_cancel_oc_mq.xsd");
		
		POCANCEL pocancel = documentXsdService.unmarshalAnularOrdenRequest(xml);
		
		
		
		if (StringUtils.isBlank(pocancel.getCompradorOrgID()) || 
				StringUtils.isBlank(pocancel.getCompradorUsuarioID()) ||
				StringUtils.isBlank(pocancel.getOrden().getNumeroOrden()) ||
				StringUtils.isBlank(pocancel.getOrden().getAccion()))  {
			
			throw new XmlRequestException("Document is invalid", xml);
			
		}
		

	}

	@Override
	public void validateAnularComprobatePago(String xml) throws XmlRequestException, XsdException {
		validate(xml, "schemas/fac_cambio_estado_mq.xsd");
		
	}

	@Override
	public void validatePagoPublicacion(String xml) throws XmlRequestException, XsdException {
		validate(xml, "schemas/fac_acttesor_mq.xsd");
		
	}

}
