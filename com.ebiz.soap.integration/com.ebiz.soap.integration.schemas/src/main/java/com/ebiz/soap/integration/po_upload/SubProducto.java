//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.11 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2017.12.02 at 08:28:18 PM PET 
//


package com.ebiz.soap.integration.po_upload;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{}CodigoSubProducto"/&gt;
 *         &lt;element ref="{}FechaEntregaSubProducto" minOccurs="0"/&gt;
 *         &lt;element ref="{}PrecioSubProducto"/&gt;
 *         &lt;element ref="{}ImpuestoSubProducto"/&gt;
 *         &lt;element ref="{}CantidadSubProducto"/&gt;
 *         &lt;element ref="{}FactorDecimalSubProducto"/&gt;
 *         &lt;element ref="{}PrecioUnitarioSubProducto" minOccurs="0"/&gt;
 *         &lt;element ref="{}UnidadSubProducto"/&gt;
 *         &lt;element ref="{}DescripcionSubProducto" minOccurs="0"/&gt;
 *         &lt;element ref="{}DescripcionDetalladaSubProducto" minOccurs="0"/&gt;
 *         &lt;element ref="{}PosicionSubProducto" minOccurs="0"/&gt;
 *         &lt;element ref="{}AtributoSubProducto" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "codigoSubProducto",
    "fechaEntregaSubProducto",
    "precioSubProducto",
    "impuestoSubProducto",
    "cantidadSubProducto",
    "factorDecimalSubProducto",
    "precioUnitarioSubProducto",
    "unidadSubProducto",
    "descripcionSubProducto",
    "descripcionDetalladaSubProducto",
    "posicionSubProducto",
    "atributoSubProducto"
})
@XmlRootElement(name = "SubProducto")
public class SubProducto {

    @XmlElement(name = "CodigoSubProducto", required = true)
    protected String codigoSubProducto;
    @XmlElement(name = "FechaEntregaSubProducto")
    protected String fechaEntregaSubProducto;
    @XmlElement(name = "PrecioSubProducto", required = true)
    protected String precioSubProducto;
    @XmlElement(name = "ImpuestoSubProducto", required = true)
    protected String impuestoSubProducto;
    @XmlElement(name = "CantidadSubProducto", required = true)
    protected String cantidadSubProducto;
    @XmlElement(name = "FactorDecimalSubProducto", required = true)
    protected String factorDecimalSubProducto;
    @XmlElement(name = "PrecioUnitarioSubProducto")
    protected String precioUnitarioSubProducto;
    @XmlElement(name = "UnidadSubProducto", required = true)
    protected String unidadSubProducto;
    @XmlElement(name = "DescripcionSubProducto")
    protected String descripcionSubProducto;
    @XmlElement(name = "DescripcionDetalladaSubProducto")
    protected String descripcionDetalladaSubProducto;
    @XmlElement(name = "PosicionSubProducto")
    protected String posicionSubProducto;
    @XmlElement(name = "AtributoSubProducto")
    protected List<AtributoSubProducto> atributoSubProducto;

    /**
     * Gets the value of the codigoSubProducto property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoSubProducto() {
        return codigoSubProducto;
    }

    /**
     * Sets the value of the codigoSubProducto property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoSubProducto(String value) {
        this.codigoSubProducto = value;
    }

    /**
     * Gets the value of the fechaEntregaSubProducto property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechaEntregaSubProducto() {
        return fechaEntregaSubProducto;
    }

    /**
     * Sets the value of the fechaEntregaSubProducto property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaEntregaSubProducto(String value) {
        this.fechaEntregaSubProducto = value;
    }

    /**
     * Gets the value of the precioSubProducto property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrecioSubProducto() {
        return precioSubProducto;
    }

    /**
     * Sets the value of the precioSubProducto property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrecioSubProducto(String value) {
        this.precioSubProducto = value;
    }

    /**
     * Gets the value of the impuestoSubProducto property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getImpuestoSubProducto() {
        return impuestoSubProducto;
    }

    /**
     * Sets the value of the impuestoSubProducto property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setImpuestoSubProducto(String value) {
        this.impuestoSubProducto = value;
    }

    /**
     * Gets the value of the cantidadSubProducto property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCantidadSubProducto() {
        return cantidadSubProducto;
    }

    /**
     * Sets the value of the cantidadSubProducto property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCantidadSubProducto(String value) {
        this.cantidadSubProducto = value;
    }

    /**
     * Gets the value of the factorDecimalSubProducto property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFactorDecimalSubProducto() {
        return factorDecimalSubProducto;
    }

    /**
     * Sets the value of the factorDecimalSubProducto property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFactorDecimalSubProducto(String value) {
        this.factorDecimalSubProducto = value;
    }

    /**
     * Gets the value of the precioUnitarioSubProducto property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrecioUnitarioSubProducto() {
        return precioUnitarioSubProducto;
    }

    /**
     * Sets the value of the precioUnitarioSubProducto property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrecioUnitarioSubProducto(String value) {
        this.precioUnitarioSubProducto = value;
    }

    /**
     * Gets the value of the unidadSubProducto property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUnidadSubProducto() {
        return unidadSubProducto;
    }

    /**
     * Sets the value of the unidadSubProducto property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUnidadSubProducto(String value) {
        this.unidadSubProducto = value;
    }

    /**
     * Gets the value of the descripcionSubProducto property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescripcionSubProducto() {
        return descripcionSubProducto;
    }

    /**
     * Sets the value of the descripcionSubProducto property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescripcionSubProducto(String value) {
        this.descripcionSubProducto = value;
    }

    /**
     * Gets the value of the descripcionDetalladaSubProducto property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescripcionDetalladaSubProducto() {
        return descripcionDetalladaSubProducto;
    }

    /**
     * Sets the value of the descripcionDetalladaSubProducto property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescripcionDetalladaSubProducto(String value) {
        this.descripcionDetalladaSubProducto = value;
    }

    /**
     * Gets the value of the posicionSubProducto property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPosicionSubProducto() {
        return posicionSubProducto;
    }

    /**
     * Sets the value of the posicionSubProducto property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPosicionSubProducto(String value) {
        this.posicionSubProducto = value;
    }

    /**
     * Gets the value of the atributoSubProducto property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the atributoSubProducto property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAtributoSubProducto().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AtributoSubProducto }
     * 
     * 
     */
    public List<AtributoSubProducto> getAtributoSubProducto() {
        if (atributoSubProducto == null) {
            atributoSubProducto = new ArrayList<AtributoSubProducto>();
        }
        return this.atributoSubProducto;
    }

}
