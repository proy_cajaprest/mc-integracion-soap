package com.ebiz.soap.integration.schemas;

import com.ebiz.soap.integration.schemas.exception.XmlRequestException;
import com.ebiz.soap.integration.schemas.exception.XsdException;

public interface ValidateService {

	void validateOrdenCompra(String xml) throws XmlRequestException,XsdException;
	void validateMovimiento(String xml)  throws XmlRequestException,XsdException;
	void validateCompraPago(String xml)  throws XmlRequestException,XsdException;
	void validateAnularOrdenCompra(String xml)  throws XmlRequestException,XsdException;
	void validateAnularComprobatePago(String xml)  throws XmlRequestException,XsdException;
	void validatePagoPublicacion(String xml)  throws XmlRequestException,XsdException;
	
	
	
}
