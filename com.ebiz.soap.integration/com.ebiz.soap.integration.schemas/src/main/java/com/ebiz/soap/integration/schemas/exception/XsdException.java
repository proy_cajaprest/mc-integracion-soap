package com.ebiz.soap.integration.schemas.exception;

public class XsdException extends Exception {
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public XsdException(String message, String request, Throwable cause) {
		super(message, cause);
		this.request = request;
	}

	public XsdException(String message, Throwable cause) {
		super(message, cause);
		this.request = null;
	}

	private final String request;

	public String getRequest() {
		return request;
	}


}
