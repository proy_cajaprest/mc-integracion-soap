//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.11 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2017.12.02 at 08:28:18 PM PET 
//


package com.ebiz.soap.integration.po_upload;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{}NumeroRfq" minOccurs="0"/&gt;
 *         &lt;element ref="{}NombreRfq" minOccurs="0"/&gt;
 *         &lt;element ref="{}NumeroCotizacion" minOccurs="0"/&gt;
 *         &lt;element ref="{}VersionCotizacion" minOccurs="0"/&gt;
 *         &lt;element ref="{}Producto" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "numeroRfq",
    "nombreRfq",
    "numeroCotizacion",
    "versionCotizacion",
    "producto"
})
@XmlRootElement(name = "Cotizacion")
public class Cotizacion {

    @XmlElement(name = "NumeroRfq")
    protected String numeroRfq;
    @XmlElement(name = "NombreRfq")
    protected String nombreRfq;
    @XmlElement(name = "NumeroCotizacion")
    protected String numeroCotizacion;
    @XmlElement(name = "VersionCotizacion")
    protected String versionCotizacion;
    @XmlElement(name = "Producto", required = true)
    protected List<Producto> producto;

    /**
     * Gets the value of the numeroRfq property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumeroRfq() {
        return numeroRfq;
    }

    /**
     * Sets the value of the numeroRfq property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumeroRfq(String value) {
        this.numeroRfq = value;
    }

    /**
     * Gets the value of the nombreRfq property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNombreRfq() {
        return nombreRfq;
    }

    /**
     * Sets the value of the nombreRfq property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNombreRfq(String value) {
        this.nombreRfq = value;
    }

    /**
     * Gets the value of the numeroCotizacion property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumeroCotizacion() {
        return numeroCotizacion;
    }

    /**
     * Sets the value of the numeroCotizacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumeroCotizacion(String value) {
        this.numeroCotizacion = value;
    }

    /**
     * Gets the value of the versionCotizacion property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVersionCotizacion() {
        return versionCotizacion;
    }

    /**
     * Sets the value of the versionCotizacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVersionCotizacion(String value) {
        this.versionCotizacion = value;
    }

    /**
     * Gets the value of the producto property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the producto property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getProducto().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Producto }
     * 
     * 
     */
    public List<Producto> getProducto() {
        if (producto == null) {
            producto = new ArrayList<Producto>();
        }
        return this.producto;
    }

}
