
package com.ebiz.soap.integration.json.model;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "id_doc",
    "num_doc",
    "data"
})
public class Payload {

    @JsonProperty("id_doc")
    private String idDoc;
    @JsonProperty("num_doc")
    private String numDoc;
    @JsonProperty("doc")
    private String doc;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("id_doc")
    public String getIdDoc() {
        return idDoc;
    }

    @JsonProperty("id_doc")
    public void setIdDoc(String idDoc) {
        this.idDoc = idDoc;
    }

    @JsonProperty("num_doc")
    public String getNumDoc() {
        return numDoc;
    }

    @JsonProperty("num_doc")
    public void setNumDoc(String numDoc) {
        this.numDoc = numDoc;
    }

    @JsonProperty("doc")
    public String getDoc() {
        return doc;
    }

    @JsonProperty("doc")
    public void setDoc(String doc) {
        this.doc = doc;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
