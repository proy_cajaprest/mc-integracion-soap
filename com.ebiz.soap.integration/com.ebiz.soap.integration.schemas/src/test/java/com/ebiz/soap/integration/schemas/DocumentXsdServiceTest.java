package com.ebiz.soap.integration.schemas;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.ebiz.soap.integration.fac_upload.FACUPLOADMQ;
import com.ebiz.soap.integration.mov_upload.MOVUPLOADMQ;
import com.ebiz.soap.integration.po_upload.AtributoSubProducto;
import com.ebiz.soap.integration.po_upload.Cotizacion;
import com.ebiz.soap.integration.po_upload.Orden;
import com.ebiz.soap.integration.po_upload.POUPLOADMQ;
import com.ebiz.soap.integration.po_upload.Producto;
import com.ebiz.soap.integration.po_upload.SubProducto;
import com.ebiz.soap.integration.schemas.exception.XmlRequestException;
import com.ebiz.soap.integration.schemas.impl.DocumentXsdServiceImpl;



@RunWith(SpringRunner.class)
@ActiveProfiles("Default")
public class DocumentXsdServiceTest {
	
	@Autowired
	private DocumentXsdService documentXsdService;
	
	
	@Test
	public void sanityTest() {
		assertNotNull(documentXsdService);
		
	}
	
	
	@Test
	public void whenUnmarshallXmlToMovimientoThenOk() throws XmlRequestException {
		String xml = "<MOVUPLOADMQ><RucCliente>PE20101045995</RucCliente><RazonSocialCliente>Inversiones Centenario S.A.A.</RazonSocialCliente><MovimientoMaterial><NumeroGuia>XXX</NumeroGuia><RucProveedor>10028230341</RucProveedor><RazonSocialProveedor>LOZADA FLORIANO MERCEDES ANGELICA</RazonSocialProveedor><DireccionProveedor>AV. BOLOGNESI (TERCER PISO - OF 217 PIURA PIURA</DireccionProveedor><DocumentoMaterial>5000039162</DocumentoMaterial><EjercicioDocMaterial>2017</EjercicioDocMaterial><PosicionDocMaterial>0001</PosicionDocMaterial><FechaEmision>2017-10-23T00:00:00-05:00</FechaEmision><FechaContabilizacion>2017-10-23T00:00:00-05:00</FechaContabilizacion><FechaRegistro>2017-10-23T00:00:00-05:00</FechaRegistro><FechaEnvio>2017-10-23T00:00:00-05:00</FechaEnvio><TipoGuia>S</TipoGuia><ObservacionGuia></ObservacionGuia><CodigoSociedad>0010</CodigoSociedad><ClaseMovimiento>101</ClaseMovimiento><DescripcionMovimiento></DescripcionMovimiento><SignoOperacion>+</SignoOperacion><NumeroOC>4590011792</NumeroOC><NumeroItemOC>45900</NumeroItemOC><NumeroParte></NumeroParte><Descripcion>SRV RECARGA EXTINTOR DE CO2</Descripcion><CantidadMovimiento>0.0000</CantidadMovimiento><UnidadMedida>SRV</UnidadMedida><Moneda>PEN</Moneda><CantidadPedidoItemOC>1.0000</CantidadPedidoItemOC><UnidadMedidaItemOC>SRV</UnidadMedidaItemOC><CantidadMovimientoItemOC>4.0000</CantidadMovimientoItemOC><PrecioItemOC>2000.0000</PrecioItemOC><CantidadBaseItemOC></CantidadBaseItemOC><SubTotal>2000.0000</SubTotal><SubTotalItemOC></SubTotalItemOC><FechaEntregaItemOC>2017-10-23</FechaEntregaItemOC><LlaveERP>5000039162</LlaveERP></MovimientoMaterial></MOVUPLOADMQ>";
		
		
		MOVUPLOADMQ poUploadMq = documentXsdService.unmarshalMovimientoRequest(xml);
		
		assertEquals(poUploadMq.getRucCliente(), "PE20101045995");
	}
	
	@Test
	public void whenUnmarshallXmlToComprobantePagoThenOk() throws XmlRequestException {
		String xml = "<FACUPLOADMQ><RucProveedor>PE10028230341</RucProveedor><RazonSocialProveedor>LOZADAFLORIANOMERCEDESANGELICA</RazonSocialProveedor><Factura><NumeroFactura>5105622917</NumeroFactura><NumeroGuia>XXX</NumeroGuia><NumeroOC>4590011795</NumeroOC><RucCliente>PE20101045995</RucCliente><FechaCreacion>2017-10-24T00:00:00-05:00</FechaCreacion><FechaEmision>2017-10-24T00:00:00-05:00</FechaEmision><PeriodoPlazoPago>45</PeriodoPlazoPago><Moneda>PEN</Moneda><SubTotal>5333.00</SubTotal><Impuesto1>6615.00</Impuesto1><Total>11948.00</Total><TipoComprobante>01</TipoComprobante><FechaVencimiento>2017-12-08T00:00:00-05:00</FechaVencimiento><FechaRecepcion>2017-10-24T00:00:00-05:00</FechaRecepcion><FormaPago></FormaPago><DocumentoERP>5105622917</DocumentoERP><ItemFactura><NumeroItemOC>00010</NumeroItemOC><DescripcionItem></DescripcionItem><CantidadItem>6.00</CantidadItem><PrecioUnitario>5300.00</PrecioUnitario><PrecioTotal>31800.00</PrecioTotal><NumeroOcItem>4590011795</NumeroOcItem></ItemFactura><ItemFactura><NumeroItemOC>00020</NumeroItemOC><DescripcionItem>CONCRETO T/V 140KG/CM2 SLUMP 2-4 (H67)</DescripcionItem><CantidadItem>150.00</CantidadItem><PrecioUnitario>33.00</PrecioUnitario><PrecioTotal>4950.00</PrecioTotal><NumeroOcItem>4590011795</NumeroOcItem></ItemFactura></Factura></FACUPLOADMQ>";
		
		FACUPLOADMQ poUploadMq = documentXsdService.unmarshalComprobantePagoRequest(xml);
		
		assertEquals(poUploadMq.getRucProveedor(), "PE10028230341");
	}
	
	
	private POUPLOADMQ getDocument() {
		
		
		POUPLOADMQ pouploadmq = new POUPLOADMQ();
		pouploadmq.setUsuario("usuario");
		pouploadmq.setClave("clave");
		pouploadmq.setCompradorOrgID("1");
		pouploadmq.setCompradorUsuarioID("2");
		
		
		Orden orden = new Orden();
		orden.setNumeroOrden("0001");
		orden.setFecha("2017-01-01");
		orden.setMonedaOrden("01");
		orden.setOrgIDVendedor("1");
		orden.setIDVendedor("2");
		orden.setEmitirA("22");
		orden.setCobrarA("22");
		orden.setValorVenta("112");
		orden.setValorVentaNeto("112");
		orden.setMontoAPagar("100");
		
		Cotizacion cotizacion = new Cotizacion();
		cotizacion.setNumeroCotizacion("001");
		cotizacion.setNombreRfq("001");
		cotizacion.setNumeroRfq("22");
		cotizacion.setVersionCotizacion("22");
			
		
		Producto p1 = new Producto();
		p1.setCodigoProducto("001");
		p1.setPrecioProducto("1");
		p1.setImpuestoProducto("2");
		p1.setCantidadProducto("3");
		p1.setUnidadProducto("4");
		
		SubProducto sp1 = new SubProducto();
		sp1.setCodigoSubProducto("001");
		sp1.setPrecioSubProducto("2");
		sp1.setImpuestoSubProducto("3");
		sp1.setCantidadSubProducto("4");
		sp1.setFactorDecimalSubProducto("3.1");
		sp1.setUnidadSubProducto("22");
		
		AtributoSubProducto atri1 = new AtributoSubProducto();
		atri1.setNombre("Nombrw1");
		
		sp1.getAtributoSubProducto().add(atri1);
		
		
		p1.getSubProducto().add(sp1);
		
		cotizacion.getProducto().add(p1);
		
		
		
		orden.setCotizacion(cotizacion);
		
		pouploadmq.getOrden().add(orden);
		
		return pouploadmq;
	}
	
	@Test
	public void whenMarshallOrdenCompraToXmlThenOk() throws XmlRequestException {
	
		String xmlGenerated = documentXsdService.marshalDocumentoRequest(getDocument()); 
		assertNotNull(xmlGenerated);
	}
	@Test
	public void whenUnmarshallXmlToOrdenCompraThenOk() throws XmlRequestException {
		
		String xml = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n" + 
				"<POUPLOADMQ>\n" + 
				"    <Usuario>usuario</Usuario>\n" + 
				"    <Clave>clave</Clave>\n" + 
				"    <CompradorOrgID>1</CompradorOrgID>\n" + 
				"    <CompradorUsuarioID>2</CompradorUsuarioID>\n" + 
				"    <Orden>\n" + 
				"        <NumeroOrden>0001</NumeroOrden>\n" + 
				"        <Fecha>2017-01-01</Fecha>\n" + 
				"        <MonedaOrden>01</MonedaOrden>\n" + 
				"        <OrgIDVendedor>1</OrgIDVendedor>\n" + 
				"        <IDVendedor>2</IDVendedor>\n" + 
				"        <EmitirA>22</EmitirA>\n" + 
				"        <CobrarA>22</CobrarA>\n" + 
				"        <ValorVenta>112</ValorVenta>\n" + 
				"        <ValorVentaNeto>112</ValorVentaNeto>\n" + 
				"        <MontoAPagar>100</MontoAPagar>\n" + 
				"        <Cotizacion>\n" + 
				"            <NumeroRfq>22</NumeroRfq>\n" + 
				"            <NombreRfq>001</NombreRfq>\n" + 
				"            <NumeroCotizacion>001</NumeroCotizacion>\n" + 
				"            <VersionCotizacion>22</VersionCotizacion>\n" + 
				"            <Producto>\n" + 
				"                <CodigoProducto>001</CodigoProducto>\n" + 
				"                <PrecioProducto>1</PrecioProducto>\n" + 
				"                <ImpuestoProducto>2</ImpuestoProducto>\n" + 
				"                <CantidadProducto>3</CantidadProducto>\n" + 
				"                <UnidadProducto>4</UnidadProducto>\n" + 
				"<AtributoProducto Nombre=\"Centro\" Modificable=\"N\" Obligatorio=\"S\">\n" + 
				"					<OperadorAtributoProducto>=</OperadorAtributoProducto>\n" + 
				"					<ValorAtributoProducto>&lt;![CDATA[6801 Av. Victor Andrés Belaunde 147]]&gt;</ValorAtributoProducto>\n" + 
				"					<UnidadAtributoProducto>-</UnidadAtributoProducto>\n" + 
				"				</AtributoProducto>" +
				"                <SubProducto>\n" + 
				"                    <CodigoSubProducto>001</CodigoSubProducto>\n" + 
				"                    <PrecioSubProducto>2</PrecioSubProducto>\n" + 
				"                    <ImpuestoSubProducto>3</ImpuestoSubProducto>\n" + 
				"                    <CantidadSubProducto>4</CantidadSubProducto>\n" + 
				"                    <FactorDecimalSubProducto>3.1</FactorDecimalSubProducto>\n" + 
				"                    <UnidadSubProducto>22</UnidadSubProducto>\n" + 
				"                    <AtributoSubProducto Nombre=\"Nombrw1\"/>\n" + 
				"                </SubProducto>\n" + 
				"            </Producto>\n" + 
				"        </Cotizacion>\n" + 
				"    </Orden>\n" + 
				"</POUPLOADMQ>";

		POUPLOADMQ poUploadMq = documentXsdService.unmarshalOrdenCompraRequest(xml);
		//poUploadMq.getOrden().get(0).getCotizacion().getProducto().get(0).getAtributoProducto().get(0).getOperadorAtributoProductoAndValorAtributoProductoAndUnidadAtributoProducto()
		assertEquals(poUploadMq.getUsuario(), "usuario");
	}
	
	
	
	@Profile("Default")
	@Configuration
	static public class ConfigTest001 {

		@Bean
		@Primary
		public DocumentXsdService accountService() {
			return new DocumentXsdServiceImpl();
		}
		
		
	}
	

}
