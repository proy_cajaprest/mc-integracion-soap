package com.ebiz.soap.integration.schemas;

import static org.junit.Assert.*;

import java.io.StringReader;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.junit.Test;

import com.ebiz.soap.integration.po_upload.Contactos;
import com.ebiz.soap.integration.po_upload.OperadorLogistico;


public class UploadTest {

	
	
	@Test
	public void operadorXmlToObjectTest() throws JAXBException {
		
		JAXBContext jaxbContext = JAXBContext.newInstance(OperadorLogistico.class);
		Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
		
		StringReader reader = new StringReader("<OperadorLogistico>" +
														"<NombreOperador>Operador 1</NombreOperador>" +
														"<MailOperador>test@gmail.com</MailOperador>" +
														"<OrgIDOperador>3</OrgIDOperador>" +
														"</OperadorLogistico>");
		
		OperadorLogistico operadorLogistico = (OperadorLogistico) unmarshaller.unmarshal(reader);
		
		assertEquals(operadorLogistico.getNombreOperador(), "Operador 1");
		assertEquals(operadorLogistico.getMailOperador(), "test@gmail.com");
		assertEquals(operadorLogistico.getOrgIDOperador(), "3");
		
		
	}
	
	@Test
	public void contactoXmlToObjectTest() throws JAXBException {
		
		JAXBContext jaxbContext = JAXBContext.newInstance(Contactos.class);
		Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
	
		StringReader reader = new StringReader("<Contactos>" +
														"<NombreContactoVendedor>Vendedor 1</NombreContactoVendedor>" +
														"<EmailContactoVendedor>testContactoVendedor@gmail.com</EmailContactoVendedor>" +
														"<OtrosDatosVendededor>otrosDatosVendededor</OtrosDatosVendededor>" +
														"<NombreContactoComprador>nombreContactoComprador</NombreContactoComprador>" +
														"<EmailContactoComprador>testContactoComprador@gmail.com</EmailContactoComprador>" +
														"<OtrosDatosComprador>Otros datos</OtrosDatosComprador>" +
														"</Contactos>");
		
		Contactos contactos = (Contactos) unmarshaller.unmarshal(reader);
		
		assertEquals(contactos.getNombreContactoVendedor(), "Vendedor 1");
		assertEquals(contactos.getEmailContactoVendedor(), "testContactoVendedor@gmail.com");
		assertEquals(contactos.getOtrosDatosVendededor(), "otrosDatosVendededor");
		assertEquals(contactos.getNombreContactoComprador(), "nombreContactoComprador");
		assertEquals(contactos.getEmailContactoComprador(), "testContactoComprador@gmail.com");
		assertEquals(contactos.getOtrosDatosComprador(), "Otros datos");
		
	}
	
}
