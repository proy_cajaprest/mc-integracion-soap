package com.ebiz.soap.integration.schemas;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.ebiz.soap.integration.json.DocumentoJsonService;
import com.ebiz.soap.integration.json.exception.DocumentJsonException;
import com.ebiz.soap.integration.json.exception.ParseJsonException;
import com.ebiz.soap.integration.json.impl.DocumentoJsonServiceImpl;
import com.ebiz.soap.integration.json.model.DocumentJson;
import com.fasterxml.jackson.databind.MapperFeature;

@RunWith(SpringRunner.class)
@ActiveProfiles("Default")
public class DocumentoJsonServiceTest {

	@Autowired
	private DocumentoJsonService documentoJsonService;

	@Test
	public void sanityTest() {
		assertNotNull(documentoJsonService);

	}
	
	@Rule
	public ExpectedException thrown = ExpectedException.none();
	
	@Test
	@Ignore
	public void whenParseBson2PayLoadDocumentoThrowDocumentJsonException() throws DocumentJsonException,ParseJsonException {
	    thrown.expect(ParseJsonException.class);
	    
	    documentoJsonService.unmarshalDocumentRequest("{"
				+ "\"_id\" : ObjectId(\"59f8d8b0ac6a960007296813\"),"
				+ "\"_class\" : \"org.bson.Document\","
				+ "\"event_id\" : \"b6e7d800-afda-4801-bb57-696a8b01df5b\","
				+ "\"event_name\" : \"comprobante-creado\","
				+ "\"data\" : {"
				+ "\"meta\" : {"
				+ "\"created_at\" : \"2017-10-31 15:10:02\","
				+ "\"source\" : \"ms-comprobante-gestionar\","
				+ "\"target\" : \"t_respuesta\","
				+ "\"http_verb\" : \"POST\","
				+ "\"org_id\" : \"7a1f3ea5-ee36-4b60-95a3-c25516704e2a\","
				+ "\"user_id\" : \"f8d47b40-def9-4518-b3f8-3b14b3fa8cf0\","
				+ "\"session_id\" : \"proveedor1\","
				+ "\"tipo_empresa\" : \"P\","
				+ "\"origen_datos\" : \"PEB2M\","
				+ "\"resource\" : \"comprobante\""
				+ "},"
				+ "\"payload\" : {"
				+ "\"FACUPLOADMQ\" : {"
				+ "\"RucProveedor\" : \"PE20193843817\","
				+ "\"RazonSocialProveedor\" : \"ACADEMIA DEPORTIVA CANTOLAO\","
				+ "\"DireccionProveedor\" : \"Juan Chavez 1312\""
				+ "}"
				+ "}"
				//+ "}" Forzamos el error
				+ "}");

	}
	
	@Test
	@Ignore
	public void whenParseBson2PayLoadDocumentoThenOk() throws DocumentJsonException,ParseJsonException {
		
		DocumentJson documentJson = documentoJsonService.unmarshalDocumentRequest("{\n" + 
				"  \"event_id\" : \"9955b49c-e3bd-44ed-86e2-b6ab9d241220\",\n" + 
				"  \"event_name\" : \"guia-creado\",\n" + 
				"  \"data\" : {\n" + 
				"    \"meta\" : {\n" + 
				"      \"created_at\" : \"2017-12-01 03:21:04\",\n" + 
				"      \"source\" : \"ms-guia-crear\",\n" + 
				"      \"target\" : \"t_respuesta_guia\",\n" + 
				"      \"http_verb\" : \"POST\",\n" + 
				"      \"org_id\" : \"b62d6238-4300-49e8-b252-b1e20a0b73ff\",\n" + 
				"      \"user_id\" : \"09a0c8b9-4f44-4cd9-946a-a46adb710235\",\n" + 
				"      \"session_id\" : \"304650b6-97f3-4985-8996-2bf7402c8319\",\n" + 
				"      \"tipo_empresa\" : \"P\",\n" + 
				"      \"origen_datos\" : \"PEB2M\",\n" + 
				"      \"resource\" : \"guia\"\n" + 
				"    },\n" + 
				"    \"payload\" : {\n" + 
				"      \"id_doc\" : \"a2de6160-def3-4f50-8a10-a09147e64fab\",\n" + 
				"      \"num_doc\" : \"7531-00000030\",\n" + 
				"      \"doc\" : \"{\\n  \\\"event_id\\\" : \\\"20c07c33-a121-4e17-bf97-752074c93353\\\",\\n  \\\"event_name\\\" : \\\"creacion-guia-solicitado\\\",\\n  \\\"data\\\" : {\\n    \\\"meta\\\" : {\\n      \\\"created_at\\\" : \\\"2017-12-01 03:21:03\\\",\\n      \\\"source\\\" : \\\"ms-productor\\\",\\n      \\\"target\\\" : \\\"t_crear_guia\\\",\\n      \\\"http_verb\\\" : \\\"POST\\\",\\n      \\\"org_id\\\" : \\\"b62d6238-4300-49e8-b252-b1e20a0b73ff\\\",\\n      \\\"user_id\\\" : \\\"09a0c8b9-4f44-4cd9-946a-a46adb710235\\\",\\n      \\\"session_id\\\" : \\\"304650b6-97f3-4985-8996-2bf7402c8319\\\",\\n      \\\"tipo_empresa\\\" : \\\"P\\\",\\n      \\\"origen_datos\\\" : \\\"PEB2M\\\",\\n      \\\"resource\\\" : \\\"guia\\\"\\n    },\\n    \\\"payload\\\" : {\\n      \\\"GDUPLOADMQ\\\" : {\\n        \\\"RucProveedor\\\" : \\\"PE20254053822\\\",\\n        \\\"RazonSocialProveedor\\\" : \\\"PRODUCTOS DE ACERO CASSADO S.A.\\\",\\n        \\\"GuiaDespacho\\\" : [ {\\n          \\\"IdTablaPeso\\\" : \\\"10000\\\",\\n          \\\"IdRegistroPeso\\\" : \\\"0000027\\\",\\n          \\\"IdTablaVolumen\\\" : \\\"10000\\\",\\n          \\\"IdRegistroVolumen\\\" : \\\"0000015\\\",\\n          \\\"IsoPeso\\\" : \\\"lb\\\",\\n          \\\"IsoVolumen\\\" : \\\"ft3\\\",\\n          \\\"NumeroGuia\\\" : \\\"7531-00000030\\\",\\n          \\\"RucCliente\\\" : \\\"PE20101164901\\\",\\n          \\\"RazonSocialCliente\\\" : \\\"WONG\\\",\\n          \\\"FechaEmision\\\" : \\\"2017-09-22\\\",\\n          \\\"FechaInicioTraslado\\\" : \\\"2017-09-24\\\",\\n          \\\"FechaProbArribo\\\" : \\\"2017-09-27\\\",\\n          \\\"IdTablaMotivoGuia\\\" : \\\"10010\\\",\\n          \\\"IdRegistroMotivoGuia\\\" : \\\"0000001\\\",\\n          \\\"MotivoGuia\\\" : \\\"Transporte\\\",\\n          \\\"Observaciones\\\" : \\\"Obs Guia\\\",\\n          \\\"IdTablaTipDocTransportista\\\" : \\\"10015\\\",\\n          \\\"IdRegistroTipDocTransportista\\\" : \\\"06\\\",\\n          \\\"TipDocTransportista\\\" : \\\"ruc\\\",\\n          \\\"NumDocTransportista\\\" : \\\"PE20700673699\\\",\\n          \\\"RazonSocialTransportista\\\" : \\\"Sr. transportista\\\",\\n          \\\"PlacaoNave\\\" : \\\"E7A712\\\",\\n          \\\"DirTransportista\\\" : \\\"Direc transportista\\\",\\n          \\\"RegistroMTC\\\" : \\\"regmtc\\\",\\n          \\\"IdTablaTipoTransporte\\\" : \\\"10011\\\",\\n          \\\"IdRegistroTipoTransporte\\\" : \\\"0000001\\\",\\n          \\\"TipoTransporte\\\" : \\\"Terrestre\\\",\\n          \\\"PuntoPartida\\\" : \\\"PuntoPartida\\\",\\n          \\\"PuntoLlegada\\\" : \\\"PuntoLlegada\\\",\\n          \\\"AlmacenDestino\\\" : \\\"AlmacenDestino\\\",\\n          \\\"TotalBultos\\\" : 20,\\n          \\\"TotalVolumen\\\" : 50.45,\\n          \\\"TotalPesoBruto\\\" : 20.45,\\n          \\\"Tara\\\" : 10.4,\\n          \\\"TotalPesoNeto\\\" : 13.5,\\n          \\\"IdOrganizacionCreacion\\\" : \\\"10000000-0000-0000-0000-000000000042\\\",\\n          \\\"IdUsuarioCreacion\\\" : \\\"00000000-0000-0000-0000-000000000023\\\",\\n          \\\"IdOrganizacionCompradora\\\" : \\\"58e68c11-a73c-4027-b1dd-4e511ed22222\\\",\\n          \\\"IdOrganizacionProveedora\\\" : \\\"b62d6238-4300-49e8-b252-b1e20a0b73ff\\\",\\n          \\\"TipoGuia\\\" : \\\"M\\\",\\n          \\\"ItemGuia\\\" : [ {\\n            \\\"IdOc\\\" : \\\"89d92c35-b17f-496e-a9b5-651355251958\\\",\\n            \\\"NumeroOrden\\\" : \\\"1111014555\\\",\\n            \\\"NumeroParte\\\" : \\\"CODPROD1\\\",\\n            \\\"DescripcionItem\\\" : \\\"Es una descripcion item\\\",\\n            \\\"PesoNetoItem\\\" : 50.6,\\n            \\\"IdTablaunidadMedida\\\" : \\\"10000\\\",\\n            \\\"IdRegistroUnidadMedida\\\" : \\\"0000043\\\",\\n            \\\"UnidadMedidaItem\\\" : \\\"Unidades\\\",\\n            \\\"IdProductoxOc\\\" : \\\"02dcc28a-22e0-4e1a-944d-778e11209c47\\\",\\n            \\\"NumeroItem\\\" : \\\"1\\\",\\n            \\\"NumeroItemOC\\\" : \\\"001\\\",\\n            \\\"IdRegistroUnidadPeso\\\" : \\\"0000022\\\",\\n            \\\"UnidadPeso\\\" : \\\"Kilogram\\\",\\n            \\\"CantidadDespachada\\\" : 10,\\n            \\\"CantidadPedido\\\" : 15,\\n            \\\"Cantidadaf\\\" : 2,\\n            \\\"Destino\\\" : \\\"1100\\\"\\n          }, {\\n            \\\"IdOc\\\" : \\\"89d92c35-b17f-496e-a9b5-651355251958\\\",\\n            \\\"NumeroOrden\\\" : \\\"1111014555\\\",\\n            \\\"NumeroParte\\\" : \\\"CODPROD2\\\",\\n            \\\"DescripcionItem\\\" : \\\"Es una descripcion item\\\",\\n            \\\"PesoNetoItem\\\" : 50.6,\\n            \\\"IdTablaunidadMedida\\\" : \\\"10000\\\",\\n            \\\"IdRegistroUnidadMedida\\\" : \\\"0000043\\\",\\n            \\\"UnidadMedidaItem\\\" : \\\"Unidades\\\",\\n            \\\"IdProductoxOc\\\" : \\\"60e9cb4b-ed5d-49a4-8062-0dbe04512bb0\\\",\\n            \\\"NumeroItem\\\" : \\\"2\\\",\\n            \\\"NumeroItemOC\\\" : \\\"002\\\",\\n            \\\"IdRegistroUnidadPeso\\\" : \\\"0000022\\\",\\n            \\\"UnidadPeso\\\" : \\\"Kilogram\\\",\\n            \\\"CantidadDespachada\\\" : 15,\\n            \\\"CantidadPedido\\\" : 15,\\n            \\\"Cantidadaf\\\" : 0,\\n            \\\"Destino\\\" : \\\"1100\\\"\\n          } ]\\n        } ]\\n      }\\n    }\\n  }\\n}\"\n" + 
				"      }\n" + 
				"    }\n" + 
				"  }\n" + 
				"}");
	
		assertEquals(documentJson.getData().getPayload().getDoc(),"");
		//assertEquals(documentJson.getData().getPayload().toString(), "{\"FACUPLOADMQ\":{\"RucProveedor\":\"PE20193843817\",\"RazonSocialProveedor\":\"ACADEMIA DEPORTIVA CANTOLAO\",\"DireccionProveedor\":\"Juan Chavez 1312\"}}");
		
	}
	@Test
	public void whenParseBson2DocumentoJsonThenOk() throws DocumentJsonException,ParseJsonException {
		DocumentJson documentJson = documentoJsonService.unmarshalDocumentRequest("{\n" + 
				"  \"event_id\" : \"9955b49c-e3bd-44ed-86e2-b6ab9d241220\",\n" + 
				"  \"event_name\" : \"guia-creado\",\n" + 
				"  \"data\" : {\n" + 
				"    \"meta\" : {\n" + 
				"      \"created_at\" : \"2017-12-01 03:21:04\",\n" + 
				"      \"source\" : \"ms-guia-crear\",\n" + 
				"      \"target\" : \"t_respuesta_guia\",\n" + 
				"      \"http_verb\" : \"POST\",\n" + 
				"      \"org_id\" : \"b62d6238-4300-49e8-b252-b1e20a0b73ff\",\n" + 
				"      \"user_id\" : \"09a0c8b9-4f44-4cd9-946a-a46adb710235\",\n" + 
				"      \"session_id\" : \"304650b6-97f3-4985-8996-2bf7402c8319\",\n" + 
				"      \"tipo_empresa\" : \"P\",\n" + 
				"      \"origen_datos\" : \"PEB2M\",\n" + 
				"      \"resource\" : \"guia\"\n" + 
				"    },\n" + 
				"    \"payload\" : {\n" + 
				"      \"id_doc\" : \"a2de6160-def3-4f50-8a10-a09147e64fab\",\n" + 
				"      \"num_doc\" : \"7531-00000030\",\n" + 
				"      \"doc\" : \"{\\n  \\\"event_id\\\" : \\\"20c07c33-a121-4e17-bf97-752074c93353\\\",\\n  \\\"event_name\\\" : \\\"creacion-guia-solicitado\\\",\\n  \\\"data\\\" : {\\n    \\\"meta\\\" : {\\n      \\\"created_at\\\" : \\\"2017-12-01 03:21:03\\\",\\n      \\\"source\\\" : \\\"ms-productor\\\",\\n      \\\"target\\\" : \\\"t_crear_guia\\\",\\n      \\\"http_verb\\\" : \\\"POST\\\",\\n      \\\"org_id\\\" : \\\"b62d6238-4300-49e8-b252-b1e20a0b73ff\\\",\\n      \\\"user_id\\\" : \\\"09a0c8b9-4f44-4cd9-946a-a46adb710235\\\",\\n      \\\"session_id\\\" : \\\"304650b6-97f3-4985-8996-2bf7402c8319\\\",\\n      \\\"tipo_empresa\\\" : \\\"P\\\",\\n      \\\"origen_datos\\\" : \\\"PEB2M\\\",\\n      \\\"resource\\\" : \\\"guia\\\"\\n    },\\n    \\\"payload\\\" : {\\n      \\\"GDUPLOADMQ\\\" : {\\n        \\\"RucProveedor\\\" : \\\"PE20254053822\\\",\\n        \\\"RazonSocialProveedor\\\" : \\\"PRODUCTOS DE ACERO CASSADO S.A.\\\",\\n        \\\"GuiaDespacho\\\" : [ {\\n          \\\"IdTablaPeso\\\" : \\\"10000\\\",\\n          \\\"IdRegistroPeso\\\" : \\\"0000027\\\",\\n          \\\"IdTablaVolumen\\\" : \\\"10000\\\",\\n          \\\"IdRegistroVolumen\\\" : \\\"0000015\\\",\\n          \\\"IsoPeso\\\" : \\\"lb\\\",\\n          \\\"IsoVolumen\\\" : \\\"ft3\\\",\\n          \\\"NumeroGuia\\\" : \\\"7531-00000030\\\",\\n          \\\"RucCliente\\\" : \\\"PE20101164901\\\",\\n          \\\"RazonSocialCliente\\\" : \\\"WONG\\\",\\n          \\\"FechaEmision\\\" : \\\"2017-09-22\\\",\\n          \\\"FechaInicioTraslado\\\" : \\\"2017-09-24\\\",\\n          \\\"FechaProbArribo\\\" : \\\"2017-09-27\\\",\\n          \\\"IdTablaMotivoGuia\\\" : \\\"10010\\\",\\n          \\\"IdRegistroMotivoGuia\\\" : \\\"0000001\\\",\\n          \\\"MotivoGuia\\\" : \\\"Transporte\\\",\\n          \\\"Observaciones\\\" : \\\"Obs Guia\\\",\\n          \\\"IdTablaTipDocTransportista\\\" : \\\"10015\\\",\\n          \\\"IdRegistroTipDocTransportista\\\" : \\\"06\\\",\\n          \\\"TipDocTransportista\\\" : \\\"ruc\\\",\\n          \\\"NumDocTransportista\\\" : \\\"PE20700673699\\\",\\n          \\\"RazonSocialTransportista\\\" : \\\"Sr. transportista\\\",\\n          \\\"PlacaoNave\\\" : \\\"E7A712\\\",\\n          \\\"DirTransportista\\\" : \\\"Direc transportista\\\",\\n          \\\"RegistroMTC\\\" : \\\"regmtc\\\",\\n          \\\"IdTablaTipoTransporte\\\" : \\\"10011\\\",\\n          \\\"IdRegistroTipoTransporte\\\" : \\\"0000001\\\",\\n          \\\"TipoTransporte\\\" : \\\"Terrestre\\\",\\n          \\\"PuntoPartida\\\" : \\\"PuntoPartida\\\",\\n          \\\"PuntoLlegada\\\" : \\\"PuntoLlegada\\\",\\n          \\\"AlmacenDestino\\\" : \\\"AlmacenDestino\\\",\\n          \\\"TotalBultos\\\" : 20,\\n          \\\"TotalVolumen\\\" : 50.45,\\n          \\\"TotalPesoBruto\\\" : 20.45,\\n          \\\"Tara\\\" : 10.4,\\n          \\\"TotalPesoNeto\\\" : 13.5,\\n          \\\"IdOrganizacionCreacion\\\" : \\\"10000000-0000-0000-0000-000000000042\\\",\\n          \\\"IdUsuarioCreacion\\\" : \\\"00000000-0000-0000-0000-000000000023\\\",\\n          \\\"IdOrganizacionCompradora\\\" : \\\"58e68c11-a73c-4027-b1dd-4e511ed22222\\\",\\n          \\\"IdOrganizacionProveedora\\\" : \\\"b62d6238-4300-49e8-b252-b1e20a0b73ff\\\",\\n          \\\"TipoGuia\\\" : \\\"M\\\",\\n          \\\"ItemGuia\\\" : [ {\\n            \\\"IdOc\\\" : \\\"89d92c35-b17f-496e-a9b5-651355251958\\\",\\n            \\\"NumeroOrden\\\" : \\\"1111014555\\\",\\n            \\\"NumeroParte\\\" : \\\"CODPROD1\\\",\\n            \\\"DescripcionItem\\\" : \\\"Es una descripcion item\\\",\\n            \\\"PesoNetoItem\\\" : 50.6,\\n            \\\"IdTablaunidadMedida\\\" : \\\"10000\\\",\\n            \\\"IdRegistroUnidadMedida\\\" : \\\"0000043\\\",\\n            \\\"UnidadMedidaItem\\\" : \\\"Unidades\\\",\\n            \\\"IdProductoxOc\\\" : \\\"02dcc28a-22e0-4e1a-944d-778e11209c47\\\",\\n            \\\"NumeroItem\\\" : \\\"1\\\",\\n            \\\"NumeroItemOC\\\" : \\\"001\\\",\\n            \\\"IdRegistroUnidadPeso\\\" : \\\"0000022\\\",\\n            \\\"UnidadPeso\\\" : \\\"Kilogram\\\",\\n            \\\"CantidadDespachada\\\" : 10,\\n            \\\"CantidadPedido\\\" : 15,\\n            \\\"Cantidadaf\\\" : 2,\\n            \\\"Destino\\\" : \\\"1100\\\"\\n          }, {\\n            \\\"IdOc\\\" : \\\"89d92c35-b17f-496e-a9b5-651355251958\\\",\\n            \\\"NumeroOrden\\\" : \\\"1111014555\\\",\\n            \\\"NumeroParte\\\" : \\\"CODPROD2\\\",\\n            \\\"DescripcionItem\\\" : \\\"Es una descripcion item\\\",\\n            \\\"PesoNetoItem\\\" : 50.6,\\n            \\\"IdTablaunidadMedida\\\" : \\\"10000\\\",\\n            \\\"IdRegistroUnidadMedida\\\" : \\\"0000043\\\",\\n            \\\"UnidadMedidaItem\\\" : \\\"Unidades\\\",\\n            \\\"IdProductoxOc\\\" : \\\"60e9cb4b-ed5d-49a4-8062-0dbe04512bb0\\\",\\n            \\\"NumeroItem\\\" : \\\"2\\\",\\n            \\\"NumeroItemOC\\\" : \\\"002\\\",\\n            \\\"IdRegistroUnidadPeso\\\" : \\\"0000022\\\",\\n            \\\"UnidadPeso\\\" : \\\"Kilogram\\\",\\n            \\\"CantidadDespachada\\\" : 15,\\n            \\\"CantidadPedido\\\" : 15,\\n            \\\"Cantidadaf\\\" : 0,\\n            \\\"Destino\\\" : \\\"1100\\\"\\n          } ]\\n        } ]\\n      }\\n    }\\n  }\\n}\"\n" + 
				"      }\n" + 
				"    }\n" + 
				"  }\n" + 
				"}");
		
		//assertEquals(documentJson.getId().getOid(), "59f8d8b0ac6a960007296813");
		//assertEquals(documentJson.getClassName(), "org.bson.Document");
		assertEquals(documentJson.getEventId(), "9955b49c-e3bd-44ed-86e2-b6ab9d241220");
		assertEquals(documentJson.getEventName(), "guia-creado");
		assertEquals(documentJson.getData().getMeta().getCreatedAt(), "2017-12-01 03:21:04");
		assertEquals(documentJson.getData().getMeta().getSource(), "ms-guia-crear");
		assertEquals(documentJson.getData().getMeta().getTarget(), "t_respuesta_guia");
		assertEquals(documentJson.getData().getMeta().getHttpVerb(), "POST");
		assertEquals(documentJson.getData().getMeta().getOrgId(), "b62d6238-4300-49e8-b252-b1e20a0b73ff");
		assertEquals(documentJson.getData().getMeta().getUserId(), "09a0c8b9-4f44-4cd9-946a-a46adb710235");
		assertEquals(documentJson.getData().getMeta().getSessionId(), "304650b6-97f3-4985-8996-2bf7402c8319");
		assertEquals(documentJson.getData().getMeta().getTipoEmpresa(), "P");
		assertEquals(documentJson.getData().getMeta().getOrigenDatos(), "PEB2M");
		assertEquals(documentJson.getData().getMeta().getResource(), "guia");
		//assertEquals(documentJson.getData().getPayload().toString(), "{\"id_doc\":\"0639fb2b-db1f-4eb8-b3d0-3e7e0d030e44\",\"num_doc\":\"001-0000122\"}");
	}

	@Profile("Default")
	@Configuration
	static public class ConfigTest001 {

		@Bean
		@Primary
		public DocumentoJsonService accountService() {
			return new DocumentoJsonServiceImpl();
		}

		@Bean
		MappingJackson2HttpMessageConverter converter() {
			MappingJackson2HttpMessageConverter messageConverter = new MappingJackson2HttpMessageConverter();
			
			
			messageConverter.getObjectMapper().configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);

			return messageConverter;

		}

	}

}
