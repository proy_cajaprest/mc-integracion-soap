package com.ebiz.soap.integration.schemas;

import static org.junit.Assert.assertNotNull;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;

import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.ebiz.soap.integration.schemas.exception.XmlRequestException;
import com.ebiz.soap.integration.schemas.exception.XsdException;
import com.ebiz.soap.integration.schemas.impl.DocumentXsdServiceImpl;
import com.ebiz.soap.integration.schemas.impl.ValidateServiceImpl;

@RunWith(SpringRunner.class)
@ActiveProfiles("Default")
public class ValidateServiceTest {
	
	@Autowired
	private ValidateService validateService;
	
	@Rule
	public ExpectedException thrown = ExpectedException.none();
	@Test
	public void sanityTest() {
		assertNotNull(validateService);

	}
	
	@Test
	public void validate1() throws XmlRequestException, XsdException {
		validateService.validateOrdenCompra("<POUPLOADMQ><CompradorOrgID>PE20101045995</CompradorOrgID><CompradorUsuarioID>SEIDOR_SP1C</CompradorUsuarioID>\n" + 
				" <Orden><NumeroOrden>4590022221</NumeroOrden><Fecha>2017-1020T00:00:00-05:00</Fecha><TipoOrden>M</TipoOrden>\n" + 
				"     <MonedaOrden>PEN</MonedaOrden><OrgIDVendedor>0001006970</OrgIDVendedor>\n" + 
				"     <IDVendedor></IDVendedor><NombreVendedor>COSAPI DATA S A</NombreVendedor>\n" + 
				"     <NITVendedor>PE20100083362</NITVendedor>\n" + 
				"     <EnviarComprobanteA>a</EnviarComprobanteA>\n" + 
				"     <EmitirA>b</EmitirA>\n" + 
				"     <CobrarA></CobrarA><CondicionesPago>Dentro de 45 Días</CondicionesPago>\n" + 
				"     <FechaIniContrato>0000-00-00T00:00:00-05:00</FechaIniContrato>\n" + 
				"     <FechaFinContrato>0000-00-00T00:00:00-</FechaFinContrato>\n" + 
				"     <GrupoCompra>ss</GrupoCompra>\n" + 
				"     <AtencionA>ss</AtencionA>\n" + 
				"     <CreadoPor>SEIDOR_SP1C</CreadoPor><ValorVenta>20300.0000</ValorVenta>\n" + 
				"     <Terminos>&lt;![CDATA[%CMP%%POS%C%TIT%UTILIDADES(5.0000%)%TAB%0.0000%ITM%%TIT%GASTOS GENERALES(4.0700%)%TAB%0.0000%END%]]&gt;</Terminos><ValorVentaNeto>20300.0000</ValorVentaNeto><Impuestos>3654.0000</Impuestos><PorcentajeImpuestos></PorcentajeImpuestos><MontoAPagar>23954.0000</MontoAPagar><AprobadoPor>&lt;![CDATA[%CMP%%POS%C%TIT%Aprobado por:%ITM%Aprobado Nivel1: GMENDEZG%END%]]&gt;</AprobadoPor>\n" + 
				"     <Narrativa>ssqwewqe sdf</Narrativa><Cotizacion><Producto><CodigoProducto>01</CodigoProducto><TipoProducto>M</TipoProducto><FechaEntregaProducto>2017-03-23</FechaEntregaProducto><PrecioProducto>5300.0000</PrecioProducto><ImpuestoProducto>954.0000</ImpuestoProducto><CantidadProducto>1.0000</CantidadProducto><PrecioUnitarioProducto>5300.0000</PrecioUnitarioProducto><UnidadProducto>SRV</UnidadProducto><DescripcionProducto>Recarga de Extintor OAC Piura</DescripcionProducto><DescripcionDetalladaProducto></DescripcionDetalladaProducto><PosicionProducto>00010</PosicionProducto><AtributoProducto Nombre=\"Centro\" Modificable=\"N\" Obligatorio=\"S\"><OperadorAtributoProducto>=</OperadorAtributoProducto><ValorAtributoProducto>![CDATA[6800 Av. Victor Andrés Belaunde 147]]</ValorAtributoProducto><UnidadAtributoProducto>-</UnidadAtributoProducto></AtributoProducto><AtributoProducto Nombre=\"Solicitud de pedido\" Modificable=\"N\" Obligatorio=\"S\"><OperadorAtributoProducto>=</OperadorAtributoProducto><ValorAtributoProducto>1000013301</ValorAtributoProducto><UnidadAtributoProducto>-</UnidadAtributoProducto></AtributoProducto><SubProducto><CodigoSubProducto>000000001100000292</CodigoSubProducto><FechaEntregaSubProducto>2017-03-23</FechaEntregaSubProducto><PrecioSubProducto>5300.0000</PrecioSubProducto><ImpuestoSubProducto>954.0000</ImpuestoSubProducto><CantidadSubProducto>1.0000</CantidadSubProducto><FactorDecimalSubProducto></FactorDecimalSubProducto><PrecioUnitarioSubProducto>5300.0000</PrecioUnitarioSubProducto><UnidadSubProducto>SRV</UnidadSubProducto><DescripcionSubProducto>SRV RECARGA EXTINTOR DE CO2</DescripcionSubProducto><DescripcionDetalladaSubProducto>SRV RECARGA EXTINTOR DE CO2</DescripcionDetalladaSubProducto><PosicionSubProducto>10.10</PosicionSubProducto><AtributoSubProducto Nombre=\"Centro\" Modificable=\"N\" Obligatorio=\"S\"><OperadorAtributoSubProducto>=</OperadorAtributoSubProducto><ValorAtributoSubProducto>6800 Av. Victor Andrés Belaunde 147</ValorAtributoSubProducto><UnidadAtributoSubProducto>-</UnidadAtributoSubProducto></AtributoSubProducto><AtributoSubProducto Nombre=\"Solicitud de pedido\" Modificable=\"N\" Obligatorio=\"S\"><OperadorAtributoSubProducto>=</OperadorAtributoSubProducto><ValorAtributoSubProducto>1000013301</ValorAtributoSubProducto><UnidadAtributoSubProducto>-</UnidadAtributoSubProducto></AtributoSubProducto></SubProducto></Producto><Producto><CodigoProducto>000000000820001158</CodigoProducto><TipoProducto>M</TipoProducto><FechaEntregaProducto>2017-03-23</FechaEntregaProducto><PrecioProducto>15000.0000</PrecioProducto><ImpuestoProducto>2700.0000</ImpuestoProducto><CantidadProducto>150.0000</CantidadProducto><PrecioUnitarioProducto>100.0000</PrecioUnitarioProducto><UnidadProducto>M3</UnidadProducto><DescripcionProducto>CONCRETO T/V 140KG/CM2 SLUMP 2-4 (H67)</DescripcionProducto><DescripcionDetalladaProducto></DescripcionDetalladaProducto><PosicionProducto>00020</PosicionProducto><AtributoProducto Nombre=\"Centro\" Modificable=\"N\" Obligatorio=\"S\"><OperadorAtributoProducto>=</OperadorAtributoProducto><ValorAtributoProducto>![CDATA[6800 Av. Victor Andrés Belaunde 147]]</ValorAtributoProducto><UnidadAtributoProducto>-</UnidadAtributoProducto></AtributoProducto><AtributoProducto Nombre=\"Solicitud de pedido\" Modificable=\"N\" Obligatorio=\"S\"><OperadorAtributoProducto>=</OperadorAtributoProducto><ValorAtributoProducto>1000013301</ValorAtributoProducto><UnidadAtributoProducto>-</UnidadAtributoProducto></AtributoProducto><SubProducto><CodigoSubProducto></CodigoSubProducto><FechaEntregaSubProducto></FechaEntregaSubProducto><PrecioSubProducto></PrecioSubProducto><ImpuestoSubProducto></ImpuestoSubProducto><CantidadSubProducto></CantidadSubProducto><FactorDecimalSubProducto></FactorDecimalSubProducto><PrecioUnitarioSubProducto></PrecioUnitarioSubProducto><UnidadSubProducto></UnidadSubProducto><DescripcionSubProducto></DescripcionSubProducto><DescripcionDetalladaSubProducto></DescripcionDetalladaSubProducto><PosicionSubProducto></PosicionSubProducto><AtributoSubProducto Nombre=\"Centro\" Modificable=\"N\" Obligatorio=\"S\"><OperadorAtributoSubProducto>=</OperadorAtributoSubProducto><ValorAtributoSubProducto></ValorAtributoSubProducto><UnidadAtributoSubProducto>-</UnidadAtributoSubProducto></AtributoSubProducto><AtributoSubProducto Nombre=\"Solicitud de pedido\" Modificable=\"N\" Obligatorio=\"S\"><OperadorAtributoSubProducto>=</OperadorAtributoSubProducto><ValorAtributoSubProducto></ValorAtributoSubProducto><UnidadAtributoSubProducto>-</UnidadAtributoSubProducto></AtributoSubProducto></SubProducto></Producto></Cotizacion></Orden></POUPLOADMQ>");

	}
	
	@Test
	public void validate2() throws XmlRequestException, XsdException {
		
		thrown.expect(XsdException.class);
		
		validateService.validateOrdenCompra("<![CDATA[<POUPLOADMQ><CompradorOrgID>PE20101045995</CompradorOrgID><CompradorUsuarioID>SEIDOR_SP1C</CompradorUsuarioID>\n" + 
				" <Orden><Fecha>2017-1020T00:00:00-05:00</Fecha><TipoOrden>M</TipoOrden>\n" + 
				"     <MonedaOrden>PEN</MonedaOrden><OrgIDVendedor>0001006970</OrgIDVendedor>\n" + 
				"     <IDVendedor></IDVendedor><NombreVendedor>COSAPI DATA S A</NombreVendedor>\n" + 
				"     <NITVendedor>PE20100083362</NITVendedor>\n" + 
				"     <EnviarComprobanteA>a</EnviarComprobanteA>\n" + 
				"     <EmitirA>b</EmitirA>\n" + 
				"     <CobrarA></CobrarA><CondicionesPago>Dentro de 45 Días</CondicionesPago>\n" + 
				"     <FechaIniContrato>0000-00-00T00:00:00-05:00</FechaIniContrato>\n" + 
				"     <FechaFinContrato>0000-00-00T00:00:00-</FechaFinContrato>\n" + 
				"     <GrupoCompra>ss</GrupoCompra>\n" + 
				"     <AtencionA>ss</AtencionA>\n" + 
				"     <CreadoPor>SEIDOR_SP1C</CreadoPor><ValorVenta>20300.0000</ValorVenta>\n" + 
				"     <Terminos>&lt;![CDATA[%CMP%%POS%C%TIT%UTILIDADES(5.0000%)%TAB%0.0000%ITM%%TIT%GASTOS GENERALES(4.0700%)%TAB%0.0000%END%]]&gt;</Terminos><ValorVentaNeto>20300.0000</ValorVentaNeto><Impuestos>3654.0000</Impuestos><PorcentajeImpuestos></PorcentajeImpuestos><MontoAPagar>23954.0000</MontoAPagar><AprobadoPor>&lt;![CDATA[%CMP%%POS%C%TIT%Aprobado por:%ITM%Aprobado Nivel1: GMENDEZG%END%]]&gt;</AprobadoPor>\n" + 
				"     <Narrativa>ssqwewqe sdf</Narrativa><Cotizacion><Producto><CodigoProducto></CodigoProducto><FechaEntregaProducto>2017-03-23</FechaEntregaProducto><PrecioProducto>5300.0000</PrecioProducto><ImpuestoProducto>954.0000</ImpuestoProducto><CantidadProducto>1.0000</CantidadProducto><PrecioUnitarioProducto>5300.0000</PrecioUnitarioProducto><UnidadProducto>SRV</UnidadProducto><DescripcionProducto>Recarga de Extintor OAC Piura</DescripcionProducto><DescripcionDetalladaProducto></DescripcionDetalladaProducto><PosicionProducto>00010</PosicionProducto><AtributoProducto Nombre=\"Centro\" Modificable=\"N\" Obligatorio=\"S\"><OperadorAtributoProducto>=</OperadorAtributoProducto><ValorAtributoProducto>![CDATA[6800 Av. Victor Andrés Belaunde 147]]</ValorAtributoProducto><UnidadAtributoProducto>-</UnidadAtributoProducto></AtributoProducto><AtributoProducto Nombre=\"Solicitud de pedido\" Modificable=\"N\" Obligatorio=\"S\"><OperadorAtributoProducto>=</OperadorAtributoProducto><ValorAtributoProducto>1000013301</ValorAtributoProducto><UnidadAtributoProducto>-</UnidadAtributoProducto></AtributoProducto><SubProducto><CodigoSubProducto>000000001100000292</CodigoSubProducto><FechaEntregaSubProducto>2017-03-23</FechaEntregaSubProducto><PrecioSubProducto>5300.0000</PrecioSubProducto><ImpuestoSubProducto>954.0000</ImpuestoSubProducto><CantidadSubProducto>1.0000</CantidadSubProducto><FactorDecimalSubProducto></FactorDecimalSubProducto><PrecioUnitarioSubProducto>5300.0000</PrecioUnitarioSubProducto><UnidadSubProducto>SRV</UnidadSubProducto><DescripcionSubProducto>SRV RECARGA EXTINTOR DE CO2</DescripcionSubProducto><DescripcionDetalladaSubProducto>SRV RECARGA EXTINTOR DE CO2</DescripcionDetalladaSubProducto><PosicionSubProducto>10.10</PosicionSubProducto><AtributoSubProducto Nombre=\"Centro\" Modificable=\"N\" Obligatorio=\"S\"><OperadorAtributoSubProducto>=</OperadorAtributoSubProducto><ValorAtributoSubProducto>6800 Av. Victor Andrés Belaunde 147</ValorAtributoSubProducto><UnidadAtributoSubProducto>-</UnidadAtributoSubProducto></AtributoSubProducto><AtributoSubProducto Nombre=\"Solicitud de pedido\" Modificable=\"N\" Obligatorio=\"S\"><OperadorAtributoSubProducto>=</OperadorAtributoSubProducto><ValorAtributoSubProducto>1000013301</ValorAtributoSubProducto><UnidadAtributoSubProducto>-</UnidadAtributoSubProducto></AtributoSubProducto></SubProducto></Producto><Producto><CodigoProducto>000000000820001158</CodigoProducto><FechaEntregaProducto>2017-03-23</FechaEntregaProducto><PrecioProducto>15000.0000</PrecioProducto><ImpuestoProducto>2700.0000</ImpuestoProducto><CantidadProducto>150.0000</CantidadProducto><PrecioUnitarioProducto>100.0000</PrecioUnitarioProducto><UnidadProducto>M3</UnidadProducto><DescripcionProducto>CONCRETO T/V 140KG/CM2 SLUMP 2-4 (H67)</DescripcionProducto><DescripcionDetalladaProducto></DescripcionDetalladaProducto><PosicionProducto>00020</PosicionProducto><AtributoProducto Nombre=\"Centro\" Modificable=\"N\" Obligatorio=\"S\"><OperadorAtributoProducto>=</OperadorAtributoProducto><ValorAtributoProducto>![CDATA[6800 Av. Victor Andrés Belaunde 147]]</ValorAtributoProducto><UnidadAtributoProducto>-</UnidadAtributoProducto></AtributoProducto><AtributoProducto Nombre=\"Solicitud de pedido\" Modificable=\"N\" Obligatorio=\"S\"><OperadorAtributoProducto>=</OperadorAtributoProducto><ValorAtributoProducto>1000013301</ValorAtributoProducto><UnidadAtributoProducto>-</UnidadAtributoProducto></AtributoProducto><SubProducto><CodigoSubProducto></CodigoSubProducto><FechaEntregaSubProducto></FechaEntregaSubProducto><PrecioSubProducto></PrecioSubProducto><ImpuestoSubProducto></ImpuestoSubProducto><CantidadSubProducto></CantidadSubProducto><FactorDecimalSubProducto></FactorDecimalSubProducto><PrecioUnitarioSubProducto></PrecioUnitarioSubProducto><UnidadSubProducto></UnidadSubProducto><DescripcionSubProducto></DescripcionSubProducto><DescripcionDetalladaSubProducto></DescripcionDetalladaSubProducto><PosicionSubProducto></PosicionSubProducto><AtributoSubProducto Nombre=\"Centro\" Modificable=\"N\" Obligatorio=\"S\"><OperadorAtributoSubProducto>=</OperadorAtributoSubProducto><ValorAtributoSubProducto></ValorAtributoSubProducto><UnidadAtributoSubProducto>-</UnidadAtributoSubProducto></AtributoSubProducto><AtributoSubProducto Nombre=\"Solicitud de pedido\" Modificable=\"N\" Obligatorio=\"S\"><OperadorAtributoSubProducto>=</OperadorAtributoSubProducto><ValorAtributoSubProducto></ValorAtributoSubProducto><UnidadAtributoSubProducto>-</UnidadAtributoSubProducto></AtributoSubProducto></SubProducto></Producto></Cotizacion></Orden></POUPLOADMQ>]]>");

	}
	

	
	@Test
	public void validate3() throws XmlRequestException, XsdException {
		
		thrown.expect(XsdException.class);
		   
		validateService.validateOrdenCompra("<POUPLOADMQ><CompradorOrgID>PE20101045995</CompradorOrgID><CompradorUsuarioID>SEIDOR_SP1C</CompradorUsuarioID>\n" + 
				" <Orden><Fecha>2017-1020T00:00:00-05:00</Fecha><TipoOrden>M</TipoOrden>\n" + 
				"     <MonedaOrden>PEN</MonedaOrden><OrgIDVendedor>0001006970</OrgIDVendedor>\n" + 
				"     <IDVendedor></IDVendedor><NombreVendedor>COSAPI DATA S A</NombreVendedor>\n" + 
				"     <NITVendedor>PE20100083362</NITVendedor>\n" + 
				"     <EnviarComprobanteA>a</EnviarComprobanteA>\n" + 
				"     <EmitirA>b</EmitirA>\n" + 
				"     <CobrarA></CobrarA><CondicionesPago>Dentro de 45 Días</CondicionesPago>\n" + 
				"     <FechaIniContrato>0000-00-00T00:00:00-05:00</FechaIniContrato>\n" + 
				"     <FechaFinContrato>0000-00-00T00:00:00-</FechaFinContrato>\n" + 
				"     <GrupoCompra>ss</GrupoCompra>\n" + 
				"     <AtencionA>ss</AtencionA>\n" + 
				"     <CreadoPor>SEIDOR_SP1C</CreadoPor><ValorVenta>20300.0000</ValorVenta>\n" + 
				"     <Terminos>&lt;![CDATA[%CMP%%POS%C%TIT%UTILIDADES(5.0000%)%TAB%0.0000%ITM%%TIT%GASTOS GENERALES(4.0700%)%TAB%0.0000%END%]]&gt;</Terminos><ValorVentaNeto>20300.0000</ValorVentaNeto><Impuestos>3654.0000</Impuestos><PorcentajeImpuestos></PorcentajeImpuestos><MontoAPagar>23954.0000</MontoAPagar><AprobadoPor>&lt;![CDATA[%CMP%%POS%C%TIT%Aprobado por:%ITM%Aprobado Nivel1: GMENDEZG%END%]]&gt;</AprobadoPor>\n" + 
				"     <Narrativa>ssqwewqe sdf</Narrativa><Cotizacion><Producto><CodigoProducto></CodigoProducto><FechaEntregaProducto>2017-03-23</FechaEntregaProducto><PrecioProducto>5300.0000</PrecioProducto><ImpuestoProducto>954.0000</ImpuestoProducto><CantidadProducto>1.0000</CantidadProducto><PrecioUnitarioProducto>5300.0000</PrecioUnitarioProducto><UnidadProducto>SRV</UnidadProducto><DescripcionProducto>Recarga de Extintor OAC Piura</DescripcionProducto><DescripcionDetalladaProducto></DescripcionDetalladaProducto><PosicionProducto>00010</PosicionProducto><AtributoProducto Nombre=\"Centro\" Modificable=\"N\" Obligatorio=\"S\"><OperadorAtributoProducto>=</OperadorAtributoProducto><ValorAtributoProducto>![CDATA[6800 Av. Victor Andrés Belaunde 147]]</ValorAtributoProducto><UnidadAtributoProducto>-</UnidadAtributoProducto></AtributoProducto><AtributoProducto Nombre=\"Solicitud de pedido\" Modificable=\"N\" Obligatorio=\"S\"><OperadorAtributoProducto>=</OperadorAtributoProducto><ValorAtributoProducto>1000013301</ValorAtributoProducto><UnidadAtributoProducto>-</UnidadAtributoProducto></AtributoProducto><SubProducto><CodigoSubProducto>000000001100000292</CodigoSubProducto><FechaEntregaSubProducto>2017-03-23</FechaEntregaSubProducto><PrecioSubProducto>5300.0000</PrecioSubProducto><ImpuestoSubProducto>954.0000</ImpuestoSubProducto><CantidadSubProducto>1.0000</CantidadSubProducto><FactorDecimalSubProducto></FactorDecimalSubProducto><PrecioUnitarioSubProducto>5300.0000</PrecioUnitarioSubProducto><UnidadSubProducto>SRV</UnidadSubProducto><DescripcionSubProducto>SRV RECARGA EXTINTOR DE CO2</DescripcionSubProducto><DescripcionDetalladaSubProducto>SRV RECARGA EXTINTOR DE CO2</DescripcionDetalladaSubProducto><PosicionSubProducto>10.10</PosicionSubProducto><AtributoSubProducto Nombre=\"Centro\" Modificable=\"N\" Obligatorio=\"S\"><OperadorAtributoSubProducto>=</OperadorAtributoSubProducto><ValorAtributoSubProducto>6800 Av. Victor Andrés Belaunde 147</ValorAtributoSubProducto><UnidadAtributoSubProducto>-</UnidadAtributoSubProducto></AtributoSubProducto><AtributoSubProducto Nombre=\"Solicitud de pedido\" Modificable=\"N\" Obligatorio=\"S\"><OperadorAtributoSubProducto>=</OperadorAtributoSubProducto><ValorAtributoSubProducto>1000013301</ValorAtributoSubProducto><UnidadAtributoSubProducto>-</UnidadAtributoSubProducto></AtributoSubProducto></SubProducto></Producto><Producto><CodigoProducto>000000000820001158</CodigoProducto><FechaEntregaProducto>2017-03-23</FechaEntregaProducto><PrecioProducto>15000.0000</PrecioProducto><ImpuestoProducto>2700.0000</ImpuestoProducto><CantidadProducto>150.0000</CantidadProducto><PrecioUnitarioProducto>100.0000</PrecioUnitarioProducto><UnidadProducto>M3</UnidadProducto><DescripcionProducto>CONCRETO T/V 140KG/CM2 SLUMP 2-4 (H67)</DescripcionProducto><DescripcionDetalladaProducto></DescripcionDetalladaProducto><PosicionProducto>00020</PosicionProducto><AtributoProducto Nombre=\"Centro\" Modificable=\"N\" Obligatorio=\"S\"><OperadorAtributoProducto>=</OperadorAtributoProducto><ValorAtributoProducto>![CDATA[6800 Av. Victor Andrés Belaunde 147]]</ValorAtributoProducto><UnidadAtributoProducto>-</UnidadAtributoProducto></AtributoProducto><AtributoProducto Nombre=\"Solicitud de pedido\" Modificable=\"N\" Obligatorio=\"S\"><OperadorAtributoProducto>=</OperadorAtributoProducto><ValorAtributoProducto>1000013301</ValorAtributoProducto><UnidadAtributoProducto>-</UnidadAtributoProducto></AtributoProducto><SubProducto><CodigoSubProducto></CodigoSubProducto><FechaEntregaSubProducto></FechaEntregaSubProducto><PrecioSubProducto></PrecioSubProducto><ImpuestoSubProducto></ImpuestoSubProducto><CantidadSubProducto></CantidadSubProducto><FactorDecimalSubProducto></FactorDecimalSubProducto><PrecioUnitarioSubProducto></PrecioUnitarioSubProducto><UnidadSubProducto></UnidadSubProducto><DescripcionSubProducto></DescripcionSubProducto><DescripcionDetalladaSubProducto></DescripcionDetalladaSubProducto><PosicionSubProducto></PosicionSubProducto><AtributoSubProducto Nombre=\"Centro\" Modificable=\"N\" Obligatorio=\"S\"><OperadorAtributoSubProducto>=</OperadorAtributoSubProducto><ValorAtributoSubProducto></ValorAtributoSubProducto><UnidadAtributoSubProducto>-</UnidadAtributoSubProducto></AtributoSubProducto><AtributoSubProducto Nombre=\"Solicitud de pedido\" Modificable=\"N\" Obligatorio=\"S\"><OperadorAtributoSubProducto>=</OperadorAtributoSubProducto><ValorAtributoSubProducto></ValorAtributoSubProducto><UnidadAtributoSubProducto>-</UnidadAtributoSubProducto></AtributoSubProducto></SubProducto></Producto></Cotizacion></Orden></POUPLOADMQ>");

	}
	
	@Profile("Default")
	@Configuration
	static public class ConfigTest001 {

		@Bean
		@Primary
		public ValidateService accountService() {
			return new ValidateServiceImpl();
		}
		
		@Bean
		@Primary
		public DocumentXsdService documentXsdService() {
			return new DocumentXsdServiceImpl();
		}

	}

}
