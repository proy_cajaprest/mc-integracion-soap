package com.ebiz.soap.integration;


import static org.springframework.ws.test.server.RequestCreators.withPayload;
import static org.springframework.ws.test.server.ResponseMatchers.payload;

import javax.xml.transform.Source;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.ws.test.server.MockWebServiceClient;
import org.springframework.xml.transform.StringSource;

import com.ebiz.soap.integration.web.SpringWsApplication;


@RunWith(SpringRunner.class)
@SpringBootTest(classes = SpringWsApplication.class)
@ActiveProfiles("unittest")
public class RequestEndpointTest {

	@Autowired
	  private ApplicationContext applicationContext;

	  private MockWebServiceClient mockClient;

	  @Before
	  public void createClient() {
	    mockClient = MockWebServiceClient.createClient(applicationContext);
	  }
	  
	  private static final String RESPONSE_DOCUMENTO_INVALIDO = 
			  "<ns2:documentoResponse xmlns:ns2=\"http://www.ebizlatin.com/service\">" +
					  "<ns2:CodigoError>ESB101</ns2:CodigoError>" +
					  "<ns2:DataError>Documento inválido</ns2:DataError>" +
			  "</ns2:documentoResponse>";

	  
	  @Test
	  public void whenAnularOCXmlAccionNoTagIsEmptyThenErrorESB100() {
		  
		  Source requestPayload = new StringSource(
			        "		      <xsd:documentoRequest xmlns:xsd=\"http://www.ebizlatin.com/service\">\n" + 
			        "		         <xsd:orgId>1b9a0ddb-1c6a-2687-a501-2885d082eba2</xsd:orgId>\n" + 
			        "		         <xsd:ruc>PE20299982</xsd:ruc>\n" + 
			        "		         <xsd:codigoAccion>OC</xsd:codigoAccion>\n" + 
			        "		         <xsd:portal>B2M</xsd:portal>\n" + 
			        "		         <xsd:id>6695013939</xsd:id>\n" + 
			        "		         <xsd:userErp>AVINKA008</xsd:userErp>\n" + 
			        "		         <xsd:userOauth>rmarinos</xsd:userOauth>\n" + 
			        "		         <xsd:pwdOauth>cGFzczEyMw==</xsd:pwdOauth>\n" + 
			        "		         <xsd:docXml><![CDATA[<POCANCEL>"
			        + "<CompradorOrgID>1</CompradorOrgID>"
			        + "<CompradorUsuarioID></CompradorUsuarioID>"
			        + "<Orden>"
			        + "<NumeroOrden>2</NumeroOrden>"
			        //+ "<Accion>3</Accion>"
			        + "</Orden></POCANCEL>]]></xsd:docXml>\n" + 
			        "		      </xsd:documentoRequest>");

			    Source responsePayload = new StringSource("<ns2:documentoResponse xmlns:ns2=\"http://www.ebizlatin.com/service\">"
			    		+ "<ns2:orgId>1b9a0ddb-1c6a-2687-a501-2885d082eba2</ns2:orgId>"
			    		+ "<ns2:codigoAccion>OC</ns2:codigoAccion>"
			    		+ "<ns2:DocumentID>6695013939</ns2:DocumentID>"
			    		+ "<ns2:CodigoError>ESB101</ns2:CodigoError>"
			    		+ "<ns2:DataError>cvc-complex-type.2.4.b: The content of element 'Orden' is not complete. One of '{Accion}' is expected.</ns2:DataError>"
			    		+ "</ns2:documentoResponse>");
			    
			    mockClient.sendRequest(withPayload(requestPayload)).andExpect(payload(responsePayload));
		  
	  }
	  
	  @Test
	  public void whenAnularOCXmlNumeroOrdenNoTagIsEmptyThenErrorESB100() {
		  
		  Source requestPayload = new StringSource(
			        "		      <xsd:documentoRequest xmlns:xsd=\"http://www.ebizlatin.com/service\">\n" + 
			        "		         <xsd:orgId>1b9a0ddb-1c6a-2687-a501-2885d082eba2</xsd:orgId>\n" + 
			        "		         <xsd:ruc>PE20299982</xsd:ruc>\n" + 
			        "		         <xsd:codigoAccion>OC</xsd:codigoAccion>\n" + 
			        "		         <xsd:portal>B2M</xsd:portal>\n" + 
			        "		         <xsd:id>6695013939</xsd:id>\n" + 
			        "		         <xsd:userErp>AVINKA008</xsd:userErp>\n" + 
			        "		         <xsd:userOauth>rmarinos</xsd:userOauth>\n" + 
			        "		         <xsd:pwdOauth>cGFzczEyMw==</xsd:pwdOauth>\n" + 
			        "		         <xsd:docXml><![CDATA[<POCANCEL>"
			        + "<CompradorOrgID>1</CompradorOrgID>"
			        + "<CompradorUsuarioID></CompradorUsuarioID>"
			        + "<Orden>"
			        //+ "<NumeroOrden>2</NumeroOrden>"
			        + "<Accion>3</Accion>"
			        + "</Orden></POCANCEL>]]></xsd:docXml>\n" + 
			        "		      </xsd:documentoRequest>");

			    Source responsePayload = new StringSource("<ns2:documentoResponse xmlns:ns2=\"http://www.ebizlatin.com/service\">"
			    		+ "<ns2:orgId>1b9a0ddb-1c6a-2687-a501-2885d082eba2</ns2:orgId>"
			    		+ "<ns2:codigoAccion>OC</ns2:codigoAccion>"
			    		+ "<ns2:DocumentID>6695013939</ns2:DocumentID>"
			    		+ "<ns2:CodigoError>ESB101</ns2:CodigoError>"
			    		+ "<ns2:DataError>cvc-complex-type.2.4.a: Invalid content was found starting with element 'Accion'. One of '{NumeroOrden}' is expected.</ns2:DataError>"
			    		+ "</ns2:documentoResponse>");
			    
			    mockClient.sendRequest(withPayload(requestPayload)).andExpect(payload(responsePayload));
		  
	  }
	  
	  @Test
	  public void whenAnularOCXmlCompradorUsuarioIDNoTagIsEmptyThenErrorESB100() {
		  
		  Source requestPayload = new StringSource(
			        "		      <xsd:documentoRequest xmlns:xsd=\"http://www.ebizlatin.com/service\">\n" + 
			        "		         <xsd:orgId>1b9a0ddb-1c6a-2687-a501-2885d082eba2</xsd:orgId>\n" + 
			        "		         <xsd:ruc>PE20299982</xsd:ruc>\n" + 
			        "		         <xsd:codigoAccion>OC</xsd:codigoAccion>\n" + 
			        "		         <xsd:portal>B2M</xsd:portal>\n" + 
			        "		         <xsd:id>6695013939</xsd:id>\n" + 
			        "		         <xsd:userErp>AVINKA008</xsd:userErp>\n" + 
			        "		         <xsd:userOauth>rmarinos</xsd:userOauth>\n" + 
			        "		         <xsd:pwdOauth>cGFzczEyMw==</xsd:pwdOauth>\n" + 
			        "		         <xsd:docXml><![CDATA[<POCANCEL>"
			        + "<CompradorOrgID>1</CompradorOrgID>"
			        //+ "<CompradorUsuarioID></CompradorUsuarioID>"
			        + "<Orden>"
			        + "<NumeroOrden>2</NumeroOrden>"
			        + "<Accion>3</Accion>"
			        + "</Orden></POCANCEL>]]></xsd:docXml>\n" + 
			        "		      </xsd:documentoRequest>");

			    Source responsePayload = new StringSource("<ns2:documentoResponse xmlns:ns2=\"http://www.ebizlatin.com/service\">"
			    		+ "<ns2:orgId>1b9a0ddb-1c6a-2687-a501-2885d082eba2</ns2:orgId>"
			    		+ "<ns2:codigoAccion>OC</ns2:codigoAccion>"
			    		+ "<ns2:DocumentID>6695013939</ns2:DocumentID>"
			    		+ "<ns2:CodigoError>ESB101</ns2:CodigoError>"
			    		+ "<ns2:DataError>cvc-complex-type.2.4.a: Invalid content was found starting with element 'Orden'. One of '{CompradorUsuarioID}' is expected.</ns2:DataError>"
			    		+ "</ns2:documentoResponse>");
			    
			    mockClient.sendRequest(withPayload(requestPayload)).andExpect(payload(responsePayload));
		  
	  }
	  
	  @Test
	  public void whenAnularOCXmlCompradorOrgIDNoTagIsEmptyThenErrorESB100() {
		  
		  Source requestPayload = new StringSource(
			        "		      <xsd:documentoRequest xmlns:xsd=\"http://www.ebizlatin.com/service\">\n" + 
			        "		         <xsd:orgId>1b9a0ddb-1c6a-2687-a501-2885d082eba2</xsd:orgId>\n" + 
			        "		         <xsd:ruc>PE20299982</xsd:ruc>\n" + 
			        "		         <xsd:codigoAccion>OC</xsd:codigoAccion>\n" + 
			        "		         <xsd:portal>B2M</xsd:portal>\n" + 
			        "		         <xsd:id>6695013939</xsd:id>\n" + 
			        "		         <xsd:userErp>AVINKA008</xsd:userErp>\n" + 
			        "		         <xsd:userOauth>rmarinos</xsd:userOauth>\n" + 
			        "		         <xsd:pwdOauth>cGFzczEyMw==</xsd:pwdOauth>\n" + 
			        "		         <xsd:docXml><![CDATA[<POCANCEL>"
			        //+ "<CompradorOrgID>1</CompradorOrgID>"
			        + "<CompradorUsuarioID></CompradorUsuarioID>"
			        + "<Orden>"
			        + "<NumeroOrden>2</NumeroOrden>"
			        + "<Accion>3</Accion>"
			        + "</Orden></POCANCEL>]]></xsd:docXml>\n" + 
			        "		      </xsd:documentoRequest>");

			    Source responsePayload = new StringSource("<ns2:documentoResponse xmlns:ns2=\"http://www.ebizlatin.com/service\">"
			    		+ "<ns2:orgId>1b9a0ddb-1c6a-2687-a501-2885d082eba2</ns2:orgId>"
			    		+ "<ns2:codigoAccion>OC</ns2:codigoAccion>"
			    		+ "<ns2:DocumentID>6695013939</ns2:DocumentID>"
			    		+ "<ns2:CodigoError>ESB101</ns2:CodigoError>"
			    		+ "<ns2:DataError>cvc-complex-type.2.4.a: Invalid content was found starting with element 'CompradorUsuarioID'. One of '{CompradorOrgID}' is expected.</ns2:DataError></ns2:documentoResponse>");
			    
			    mockClient.sendRequest(withPayload(requestPayload)).andExpect(payload(responsePayload));
		  
	  }
	  
	  @Test
	  public void whenAnularOCXmlPOCANCELIsNullIsEmptyThenErrorESB100() {
		  
		  Source requestPayload = new StringSource(
			        "		      <xsd:documentoRequest xmlns:xsd=\"http://www.ebizlatin.com/service\">\n" + 
			        "		         <xsd:orgId>1b9a0ddb-1c6a-2687-a501-2885d082eba2</xsd:orgId>\n" + 
			        "		         <xsd:ruc>PE20299982</xsd:ruc>\n" + 
			        "		         <xsd:codigoAccion>OC</xsd:codigoAccion>\n" + 
			        "		         <xsd:portal>B2M</xsd:portal>\n" + 
			        "		         <xsd:id>6695013939</xsd:id>\n" + 
			        "		         <xsd:userErp>AVINKA008</xsd:userErp>\n" + 
			        "		         <xsd:userOauth>rmarinos</xsd:userOauth>\n" + 
			        "		         <xsd:pwdOauth>cGFzczEyMw==</xsd:pwdOauth>\n" + 
			        "		         <xsd:docXml><![CDATA[<POCANCEL></POCANCEL>]]></xsd:docXml>\n" + 
			        "		      </xsd:documentoRequest>");

			    Source responsePayload = new StringSource("<ns2:documentoResponse xmlns:ns2=\"http://www.ebizlatin.com/service\">"
			    		+ "<ns2:orgId>1b9a0ddb-1c6a-2687-a501-2885d082eba2</ns2:orgId>"
			    		+ "<ns2:codigoAccion>OC</ns2:codigoAccion>"
			    		+ "<ns2:DocumentID>6695013939</ns2:DocumentID>"
			    		+ "<ns2:CodigoError>ESB101</ns2:CodigoError>"
			    		+ "<ns2:DataError>cvc-complex-type.2.4.b: The content of element 'POCANCEL' is not complete. One of '{CompradorOrgID}' is expected.</ns2:DataError>"
			    		+ "</ns2:documentoResponse>");
			    
			    mockClient.sendRequest(withPayload(requestPayload)).andExpect(payload(responsePayload));
		  
	  }
	  
	  @Test
	  public void whenAnularOCXmlAccionIDIsNullIsEmptyThenErrorESB100() {
		  
		  Source requestPayload = new StringSource(
			        "		      <xsd:documentoRequest xmlns:xsd=\"http://www.ebizlatin.com/service\">\n" + 
			        "		         <xsd:orgId>1b9a0ddb-1c6a-2687-a501-2885d082eba2</xsd:orgId>\n" + 
			        "		         <xsd:ruc>PE20299982</xsd:ruc>\n" + 
			        "		         <xsd:codigoAccion>OC</xsd:codigoAccion>\n" + 
			        "		         <xsd:portal>B2M</xsd:portal>\n" + 
			        "		         <xsd:id>6695013939</xsd:id>\n" + 
			        "		         <xsd:userErp>AVINKA008</xsd:userErp>\n" + 
			        "		         <xsd:userOauth>rmarinos</xsd:userOauth>\n" + 
			        "		         <xsd:pwdOauth>cGFzczEyMw==</xsd:pwdOauth>\n" + 
			        "		         <xsd:docXml><![CDATA[<POCANCEL>"
			        + "<CompradorOrgID>1</CompradorOrgID>"
			        + "<CompradorUsuarioID>2</CompradorUsuarioID>"
			        + "<Orden>"
			        + "<NumeroOrden>3</NumeroOrden>"
			        + "<Accion></Accion>"
			        + "</Orden></POCANCEL>]]></xsd:docXml>\n" + 
			        "		      </xsd:documentoRequest>");

			    Source responsePayload = new StringSource("<ns2:documentoResponse xmlns:ns2=\"http://www.ebizlatin.com/service\">\n" + 
			    		"<ns2:orgId>1b9a0ddb-1c6a-2687-a501-2885d082eba2</ns2:orgId>\n" + 
			    		"<ns2:codigoAccion>OC</ns2:codigoAccion>\n" + 
			    		"<ns2:DocumentID>6695013939</ns2:DocumentID>\n" + 
			    		"<ns2:CodigoError>ESB101</ns2:CodigoError>\n" + 
			    		"<ns2:DataError>Document is invalid</ns2:DataError></ns2:documentoResponse>");
			    
			    mockClient.sendRequest(withPayload(requestPayload)).andExpect(payload(responsePayload));
		  
	  }
	  
	  @Test
	  public void whenAnularOCXmlNumeroOrdenIDIsNullIsEmptyThenErrorESB100() {
		  
		  Source requestPayload = new StringSource(
			        "		      <xsd:documentoRequest xmlns:xsd=\"http://www.ebizlatin.com/service\">\n" + 
			        "		         <xsd:orgId>1b9a0ddb-1c6a-2687-a501-2885d082eba2</xsd:orgId>\n" + 
			        "		         <xsd:ruc>PE20299982</xsd:ruc>\n" + 
			        "		         <xsd:codigoAccion>OC</xsd:codigoAccion>\n" + 
			        "		         <xsd:portal>B2M</xsd:portal>\n" + 
			        "		         <xsd:id>6695013939</xsd:id>\n" + 
			        "		         <xsd:userErp>AVINKA008</xsd:userErp>\n" + 
			        "		         <xsd:userOauth>rmarinos</xsd:userOauth>\n" + 
			        "		         <xsd:pwdOauth>cGFzczEyMw==</xsd:pwdOauth>\n" + 
			        "		         <xsd:docXml><![CDATA[<POCANCEL>"
			        + "<CompradorOrgID>1</CompradorOrgID>"
			        + "<CompradorUsuarioID>2</CompradorUsuarioID>"
			        + "<Orden>"
			        + "<NumeroOrden></NumeroOrden>"
			        + "<Accion>3</Accion>"
			        + "</Orden></POCANCEL>]]></xsd:docXml>\n" + 
			        "		      </xsd:documentoRequest>");

			    Source responsePayload = new StringSource("<ns2:documentoResponse xmlns:ns2=\"http://www.ebizlatin.com/service\">\n" + 
			    		"<ns2:orgId>1b9a0ddb-1c6a-2687-a501-2885d082eba2</ns2:orgId>\n" + 
			    		"<ns2:codigoAccion>OC</ns2:codigoAccion>\n" + 
			    		"<ns2:DocumentID>6695013939</ns2:DocumentID>\n" + 
			    		"<ns2:CodigoError>ESB101</ns2:CodigoError>\n" + 
			    		"<ns2:DataError>Document is invalid</ns2:DataError></ns2:documentoResponse>");
			    
			    mockClient.sendRequest(withPayload(requestPayload)).andExpect(payload(responsePayload));
		  
	  }
	  
	  
	  @Test
	  public void whenAnularOCXmlCompradorUsuarioIDIsNullIsEmptyThenErrorESB100() {
		  
		  Source requestPayload = new StringSource(
			        "		      <xsd:documentoRequest xmlns:xsd=\"http://www.ebizlatin.com/service\">\n" + 
			        "		         <xsd:orgId>1b9a0ddb-1c6a-2687-a501-2885d082eba2</xsd:orgId>\n" + 
			        "		         <xsd:ruc>PE20299982</xsd:ruc>\n" + 
			        "		         <xsd:codigoAccion>OC</xsd:codigoAccion>\n" + 
			        "		         <xsd:portal>B2M</xsd:portal>\n" + 
			        "		         <xsd:id>6695013939</xsd:id>\n" + 
			        "		         <xsd:userErp>AVINKA008</xsd:userErp>\n" + 
			        "		         <xsd:userOauth>rmarinos</xsd:userOauth>\n" + 
			        "		         <xsd:pwdOauth>cGFzczEyMw==</xsd:pwdOauth>\n" + 
			        "		         <xsd:docXml><![CDATA[<POCANCEL>"
			        + "<CompradorOrgID>1</CompradorOrgID>"
			        + "<CompradorUsuarioID></CompradorUsuarioID>"
			        + "<Orden>"
			        + "<NumeroOrden>2</NumeroOrden>"
			        + "<Accion>3</Accion>"
			        + "</Orden></POCANCEL>]]></xsd:docXml>\n" + 
			        "		      </xsd:documentoRequest>");

			    Source responsePayload = new StringSource("<ns2:documentoResponse xmlns:ns2=\"http://www.ebizlatin.com/service\">\n" + 
			    		"<ns2:orgId>1b9a0ddb-1c6a-2687-a501-2885d082eba2</ns2:orgId>\n" + 
			    		"<ns2:codigoAccion>OC</ns2:codigoAccion>\n" + 
			    		"<ns2:DocumentID>6695013939</ns2:DocumentID>\n" + 
			    		"<ns2:CodigoError>ESB101</ns2:CodigoError>\n" + 
			    		"<ns2:DataError>Document is invalid</ns2:DataError></ns2:documentoResponse>");
			    
			    mockClient.sendRequest(withPayload(requestPayload)).andExpect(payload(responsePayload));
		  
	  }
	  
	  @Test
	  public void whenAnularOCXmlCompradorOrgIDIsNullIsEmptyThenErrorESB100() {
		  
		  Source requestPayload = new StringSource(
			        "		      <xsd:documentoRequest xmlns:xsd=\"http://www.ebizlatin.com/service\">\n" + 
			        "		         <xsd:orgId>1b9a0ddb-1c6a-2687-a501-2885d082eba2</xsd:orgId>\n" + 
			        "		         <xsd:ruc>PE20299982</xsd:ruc>\n" + 
			        "		         <xsd:codigoAccion>OC</xsd:codigoAccion>\n" + 
			        "		         <xsd:portal>B2M</xsd:portal>\n" + 
			        "		         <xsd:id>6695013939</xsd:id>\n" + 
			        "		         <xsd:userErp>AVINKA008</xsd:userErp>\n" + 
			        "		         <xsd:userOauth>rmarinos</xsd:userOauth>\n" + 
			        "		         <xsd:pwdOauth>cGFzczEyMw==</xsd:pwdOauth>\n" + 
			        "		         <xsd:docXml><![CDATA[<POCANCEL>"
			        + "<CompradorOrgID></CompradorOrgID>"
			        + "<CompradorUsuarioID>1</CompradorUsuarioID>"
			        + "<Orden>"
			        + "<NumeroOrden>2</NumeroOrden>"
			        + "<Accion>3</Accion>"
			        + "</Orden></POCANCEL>]]></xsd:docXml>\n" + 
			        "		      </xsd:documentoRequest>");

			    Source responsePayload = new StringSource("<ns2:documentoResponse xmlns:ns2=\"http://www.ebizlatin.com/service\">\n" + 
			    		"<ns2:orgId>1b9a0ddb-1c6a-2687-a501-2885d082eba2</ns2:orgId>\n" + 
			    		"<ns2:codigoAccion>OC</ns2:codigoAccion>\n" + 
			    		"<ns2:DocumentID>6695013939</ns2:DocumentID>\n" + 
			    		"<ns2:CodigoError>ESB101</ns2:CodigoError>\n" + 
			    		"<ns2:DataError>Document is invalid</ns2:DataError></ns2:documentoResponse>");
			    
			    mockClient.sendRequest(withPayload(requestPayload)).andExpect(payload(responsePayload));
		  
	  }
	  
	  @Test
	  public void whenAnularOCXmlNullThenErrorESB100() {
		  
		  Source requestPayload = new StringSource(
			        "		      <xsd:documentoRequest xmlns:xsd=\"http://www.ebizlatin.com/service\">\n" + 
			        "		         <xsd:orgId>1b9a0ddb-1c6a-2687-a501-2885d082eba2</xsd:orgId>\n" + 
			        "		         <xsd:ruc>PE20299982</xsd:ruc>\n" + 
			        "		         <xsd:codigoAccion>OC</xsd:codigoAccion>\n" + 
			        "		         <xsd:portal>B2M</xsd:portal>\n" + 
			        "		         <xsd:id>6695013939</xsd:id>\n" + 
			        "		         <xsd:userErp>AVINKA008</xsd:userErp>\n" + 
			        "		         <xsd:userOauth>rmarinos</xsd:userOauth>\n" + 
			        "		         <xsd:pwdOauth>cGFzczEyMw==</xsd:pwdOauth>\n" + 
			        "		         <xsd:docXml></xsd:docXml>\n" + 
			        "		      </xsd:documentoRequest>");

			    Source responsePayload = new StringSource("<ns2:documentoResponse xmlns:ns2=\"http://www.ebizlatin.com/service\">"
			    		+ "<ns2:orgId>1b9a0ddb-1c6a-2687-a501-2885d082eba2</ns2:orgId>"
			    		+ "<ns2:codigoAccion>OC</ns2:codigoAccion>"
			    		+ "<ns2:DocumentID>6695013939</ns2:DocumentID>"
			    		+ "<ns2:CodigoError>ESB101</ns2:CodigoError>"
			    		+ "<ns2:DataError>POCANCEL is null</ns2:DataError>"
			    		+ "</ns2:documentoResponse>");
			    
			    mockClient.sendRequest(withPayload(requestPayload)).andExpect(payload(responsePayload));
		  
		  
	  }
	  
	  
	  @Test
	  public void whenAnularOCXmlIsEmptyThenErrorESB100() {
		  
		  Source requestPayload = new StringSource(
			        "		      <xsd:documentoRequest xmlns:xsd=\"http://www.ebizlatin.com/service\">\n" + 
			        "		         <xsd:orgId>1b9a0ddb-1c6a-2687-a501-2885d082eba2</xsd:orgId>\n" + 
			        "		         <xsd:ruc>PE20299982</xsd:ruc>\n" + 
			        "		         <xsd:codigoAccion>OC</xsd:codigoAccion>\n" + 
			        "		         <xsd:portal>B2M</xsd:portal>\n" + 
			        "		         <xsd:id>6695013939</xsd:id>\n" + 
			        "		         <xsd:userErp>AVINKA008</xsd:userErp>\n" + 
			        "		         <xsd:userOauth>rmarinos</xsd:userOauth>\n" + 
			        "		         <xsd:pwdOauth>cGFzczEyMw==</xsd:pwdOauth>\n" + 
			        "		         <xsd:docXml><![CDATA[<POCANCEL></POCANCEL>]]></xsd:docXml>\n" + 
			        "		      </xsd:documentoRequest>");

			    Source responsePayload = new StringSource("<ns2:documentoResponse xmlns:ns2=\"http://www.ebizlatin.com/service\">\n" + 
			    		"<ns2:orgId>1b9a0ddb-1c6a-2687-a501-2885d082eba2</ns2:orgId>\n" + 
			    		"<ns2:codigoAccion>OC</ns2:codigoAccion>\n" + 
			    		"<ns2:DocumentID>6695013939</ns2:DocumentID>\n" + 
			    		"<ns2:CodigoError>ESB101</ns2:CodigoError>\n" + 
			    		"<ns2:DataError>cvc-complex-type.2.4.b: The content of element 'POCANCEL' is not complete. One of '{CompradorOrgID}' is expected.</ns2:DataError>\n" + 
			    		"</ns2:documentoResponse>");
			    
			    mockClient.sendRequest(withPayload(requestPayload)).andExpect(payload(responsePayload));
		  
	  }
	  
	  @Test
	  public void whenDocumentInvalidThenErrorESB100() {

	    Source requestPayload = new StringSource(
	        "		      <xsd:documentoRequest xmlns:xsd=\"http://www.ebizlatin.com/service\">\n" +
	        	//"		         <xsd:orgIdDEmo>1b9a0ddb-1c6a-2687-a501-2885d082eba2</xsd:orgIdDEmo>\n" + 
	        //"		         <xsd:orgId>1b9a0ddb-1c6a-2687-a501-2885d082eba2</xsd:orgId>\n" + 
	        //"		         <xsd:ruc>PE20299982</xsd:ruc>\n" + 
	        //"		         <xsd:codigoAccion>O1</xsd:codigoAccion>\n" + 
	        //"		         <xsd:portal>B2M</xsd:portal>\n" + 
	        //"		         <xsd:id>6695013939</xsd:id>\n" + 
	        //"		         <xsd:userErp>AVINKA008</xsd:userErp>\n" + 
	        //"		         <xsd:userOauth>rmarinos</xsd:userOauth>\n" + 
	        //"		         <xsd:pwdOauth>cGFzczEyMw==</xsd:pwdOauth>\n" + 
	        //"		         <xsd:docXml><![CDATA[<POUPLOADMQ></POUPLOADMQ>]]></xsd:docXml>\n" + 
	        "		      </xsd:documentoRequest>");

	    Source responsePayload = new StringSource(RESPONSE_DOCUMENTO_INVALIDO);
	    
	 
	    mockClient.sendRequest(withPayload(requestPayload)).andExpect(payload(responsePayload));
	  }
	  
	  @Test
	  public void whenPortallIsNullThenErrorESB100() {

	    Source requestPayload = new StringSource(
	        "		      <xsd:documentoRequest xmlns:xsd=\"http://www.ebizlatin.com/service\">\n" + 
	        "		         <xsd:orgId>1b9a0ddb-1c6a-2687-a501-2885d082eba2</xsd:orgId>\n" + 
	        "		         <xsd:ruc>PE20299982</xsd:ruc>\n" + 
	        "		         <xsd:codigoAccion>O1</xsd:codigoAccion>\n" + 
	        //"		         <xsd:portal>B2M</xsd:portal>\n" + 
	        "		         <xsd:id>6695013939</xsd:id>\n" + 
	        "		         <xsd:userErp>AVINKA008</xsd:userErp>\n" + 
	        "		         <xsd:userOauth>rmarinos</xsd:userOauth>\n" + 
	        "		         <xsd:pwdOauth>cGFzczEyMw==</xsd:pwdOauth>\n" + 
	        "		         <xsd:docXml><![CDATA[<POUPLOADMQ></POUPLOADMQ>]]></xsd:docXml>\n" + 
	        "		      </xsd:documentoRequest>");

	    Source responsePayload = new StringSource("<ns2:documentoResponse xmlns:ns2=\"http://www.ebizlatin.com/service\"><ns2:orgId>1b9a0ddb-1c6a-2687-a501-2885d082eba2</ns2:orgId><ns2:codigoAccion>O1</ns2:codigoAccion><ns2:DocumentID>6695013939</ns2:DocumentID><ns2:CodigoError>ESB101</ns2:CodigoError><ns2:DataError>cvc-complex-type.2.4.b: The content of element 'POUPLOADMQ' is not complete. One of '{Usuario, Clave, CompradorOrgID}' is expected.</ns2:DataError></ns2:documentoResponse>");
	    
	 
	    mockClient.sendRequest(withPayload(requestPayload)).andExpect(payload(responsePayload));
	  }
	  
	  @Test
	  public void whenOrgIdIsNullThenErrorESB100() {

	    Source requestPayload = new StringSource(
	        "		      <xsd:documentoRequest xmlns:xsd=\"http://www.ebizlatin.com/service\">\n" + 
	        //"		         <xsd:orgId></xsd:orgId>\n" + 
	        "		         <xsd:ruc>PE20299982</xsd:ruc>\n" + 
	        "		         <xsd:codigoAccion>O1</xsd:codigoAccion>\n" + 
	        "		         <xsd:portal>B2M</xsd:portal>\n" + 
	        "		         <xsd:id>6695013939</xsd:id>\n" + 
	        "		         <xsd:userErp>AVINKA008</xsd:userErp>\n" + 
	        "		         <xsd:userOauth>rmarinos</xsd:userOauth>\n" + 
	        "		         <xsd:pwdOauth>cGFzczEyMw==</xsd:pwdOauth>\n" + 
	        "		         <xsd:docXml><![CDATA[<POUPLOADMQ></POUPLOADMQ>]]></xsd:docXml>\n" + 
	        "		      </xsd:documentoRequest>");

	    Source responsePayload = new StringSource(RESPONSE_DOCUMENTO_INVALIDO);
	    
	 
	    mockClient.sendRequest(withPayload(requestPayload)).andExpect(payload(responsePayload));
	  }
	  
	  
	  @Test
	  public void whenPwdOauthIsNullThenErrorESB100() {

	    Source requestPayload = new StringSource(
	        "		      <xsd:documentoRequest xmlns:xsd=\"http://www.ebizlatin.com/service\">\n" + 
	        "		         <xsd:orgId>1b9a0ddb-1c6a-2687-a501-2885d082eba2</xsd:orgId>\n" +  
	        "		         <xsd:codigoAccion>O1</xsd:codigoAccion>\n" + 
	        "		         <xsd:portal>B2M</xsd:portal>\n" + 
	        "		         <xsd:id>6695013939</xsd:id>\n" + 
	        "		         <xsd:userErp>AVINKA008</xsd:userErp>\n" + 
	        "		         <xsd:userOauth>rmarinos</xsd:userOauth>\n" + 
	        //"		         <xsd:pwdOauth>cGFzczEyMw==</xsd:pwdOauth>\n" + 
	        "		         <xsd:docXml><![CDATA[<POUPLOADMQ></POUPLOADMQ>]]></xsd:docXml>\n" + 
	        "		      </xsd:documentoRequest>");

	    Source responsePayload = new StringSource(RESPONSE_DOCUMENTO_INVALIDO);
	    
	 
	    mockClient.sendRequest(withPayload(requestPayload)).andExpect(payload(responsePayload));
	  }
	  
	  
	  @Test
	  public void whenUserOauthIsNullThenErrorESB100() {

	    Source requestPayload = new StringSource(
	        "		      <xsd:documentoRequest xmlns:xsd=\"http://www.ebizlatin.com/service\">\n" + 
	        "		         <xsd:orgId>1b9a0ddb-1c6a-2687-a501-2885d082eba2</xsd:orgId>\n" +  
	        "		         <xsd:codigoAccion>O1</xsd:codigoAccion>\n" + 
	        "		         <xsd:portal>B2M</xsd:portal>\n" + 
	        "		         <xsd:id>6695013939</xsd:id>\n" + 
	        "		         <xsd:userErp>AVINKA008</xsd:userErp>\n" + 
	        //"		         <xsd:userOauth>rmarinos</xsd:userOauth>\n" + 
	        "		         <xsd:pwdOauth>cGFzczEyMw==</xsd:pwdOauth>\n" + 
	        "		         <xsd:docXml><![CDATA[<POUPLOADMQ></POUPLOADMQ>]]></xsd:docXml>\n" + 
	        "		      </xsd:documentoRequest>");

	    Source responsePayload = new StringSource(RESPONSE_DOCUMENTO_INVALIDO);
	    
	 
	    mockClient.sendRequest(withPayload(requestPayload)).andExpect(payload(responsePayload));
	  }
	  
	  
	  @Test
	  public void whenUserErpIsNullThenErrorESB100() {

	    Source requestPayload = new StringSource(
	        "		      <xsd:documentoRequest xmlns:xsd=\"http://www.ebizlatin.com/service\">\n" + 
	        "		         <xsd:orgId>1b9a0ddb-1c6a-2687-a501-2885d082eba2</xsd:orgId>\n" +  
	        "		         <xsd:codigoAccion>O1</xsd:codigoAccion>\n" + 
	        "		         <xsd:portal>B2M</xsd:portal>\n" + 
	        "		         <xsd:id>6695013939</xsd:id>\n" + 
	        //"		         <xsd:userErp>AVINKA008</xsd:userErp>\n" + 
	        "		         <xsd:userOauth>rmarinos</xsd:userOauth>\n" + 
	        "		         <xsd:pwdOauth>cGFzczEyMw==</xsd:pwdOauth>\n" + 
	        "		         <xsd:docXml><![CDATA[<POUPLOADMQ></POUPLOADMQ>]]></xsd:docXml>\n" + 
	        "		      </xsd:documentoRequest>");

	    Source responsePayload = new StringSource(RESPONSE_DOCUMENTO_INVALIDO);
	    
	 
	    mockClient.sendRequest(withPayload(requestPayload)).andExpect(payload(responsePayload));
	  }
	  
	  @Test
	  public void whenRucIsNullThenErrorESB100() {

	    Source requestPayload = new StringSource(
	        "		      <xsd:documentoRequest xmlns:xsd=\"http://www.ebizlatin.com/service\">\n" + 
	        "		         <xsd:orgId>1b9a0ddb-1c6a-2687-a501-2885d082eba2</xsd:orgId>\n" + 
	        //"		         <xsd:ruc>PE20299982</xsd:ruc>\n" + 
	        "		         <xsd:codigoAccion>O1</xsd:codigoAccion>\n" + 
	        "		         <xsd:portal>B2M</xsd:portal>\n" + 
	        "		         <xsd:id>6695013939</xsd:id>\n" + 
	        "		         <xsd:userErp>AVINKA008</xsd:userErp>\n" + 
	        "		         <xsd:userOauth>rmarinos</xsd:userOauth>\n" + 
	        "		         <xsd:pwdOauth>cGFzczEyMw==</xsd:pwdOauth>\n" + 
	        "		         <xsd:docXml><![CDATA[<POUPLOADMQ></POUPLOADMQ>]]></xsd:docXml>\n" + 
	        "		      </xsd:documentoRequest>");

	    Source responsePayload = new StringSource("<ns2:documentoResponse xmlns:ns2=\"http://www.ebizlatin.com/service\"><ns2:orgId>1b9a0ddb-1c6a-2687-a501-2885d082eba2</ns2:orgId><ns2:codigoAccion>O1</ns2:codigoAccion><ns2:DocumentID>6695013939</ns2:DocumentID><ns2:CodigoError>ESB101</ns2:CodigoError><ns2:DataError>cvc-complex-type.2.4.b: The content of element 'POUPLOADMQ' is not complete. One of '{Usuario, Clave, CompradorOrgID}' is expected.</ns2:DataError></ns2:documentoResponse>");
	    
	 
	    mockClient.sendRequest(withPayload(requestPayload)).andExpect(payload(responsePayload));
	  }
	  
	  @Test
	  public void whenIdIsNullThenErrorESB100() {

	    Source requestPayload = new StringSource(
	        "		      <xsd:documentoRequest xmlns:xsd=\"http://www.ebizlatin.com/service\">\n" + 
	        "		         <xsd:orgId>1b9a0ddb-1c6a-2687-a501-2885d082eba2</xsd:orgId>\n" + 
	        "		         <xsd:ruc>PE20299982</xsd:ruc>\n" + 
	        "		         <xsd:codigoAccion>O1</xsd:codigoAccion>\n" + 
	        "		         <xsd:portal>B2M</xsd:portal>\n" + 
	        //"		         <xsd:id>6695013939</xsd:id>\n" + 
	        "		         <xsd:userErp>AVINKA008</xsd:userErp>\n" + 
	        "		         <xsd:userOauth>rmarinos</xsd:userOauth>\n" + 
	        "		         <xsd:pwdOauth>cGFzczEyMw==</xsd:pwdOauth>\n" + 
	        "		         <xsd:docXml><![CDATA[<POUPLOADMQ></POUPLOADMQ>]]></xsd:docXml>\n" + 
	        "		      </xsd:documentoRequest>");

	    Source responsePayload = new StringSource(RESPONSE_DOCUMENTO_INVALIDO);
	    
	 
	    mockClient.sendRequest(withPayload(requestPayload)).andExpect(payload(responsePayload));
	  }
	  
	  
}
