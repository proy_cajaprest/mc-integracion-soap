package com.ebiz.soap.integration;


import static org.springframework.ws.test.server.RequestCreators.withPayload;
import static org.springframework.ws.test.server.ResponseMatchers.payload;

import javax.xml.transform.Source;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.ws.test.server.MockWebServiceClient;
import org.springframework.xml.transform.StringSource;

import com.ebiz.soap.integration.web.SpringWsApplication;


@RunWith(SpringRunner.class)
@SpringBootTest(classes = SpringWsApplication.class)
@ActiveProfiles("unittest")
public class QueryEndpointTest {

	@Autowired
	  private ApplicationContext applicationContext;

	  private MockWebServiceClient mockClient;

	  @Before
	  public void createClient() {
	    mockClient = MockWebServiceClient.createClient(applicationContext);
	  }
	  
	

	  
	  @Test
	  public void whenQueryIdNoTagIsEmptyThenErrorESB100() {
		  
		  Source requestPayload = new StringSource(
			        "<ser:documentosRequest xmlns:ser=\"http://www.ebizlatin.com/service\">" + 
			        "         <ser:userOauth>rmarinos</ser:userOauth>\n" + 
			        "         <ser:pwdOauth>cGFzczEyMw==</ser:pwdOauth>\n" + 
			        "         <!--1 to 200 repetitions:-->\n" + 
			        "         <ser:documento>\n" + 
			        "            <ser:orgId>1b9a0ddb-1c6a-2687-a501-2885d082eba2</ser:orgId>\n" + 
			        "            <ser:rucComprador>PE20101045995</ser:rucComprador>\n" + 
			        "            <!--Optional:-->\n" + 
			        "            <ser:rucProveedor>PE20101045986</ser:rucProveedor>\n" + 
			        "            <ser:codigoAccion>O1</ser:codigoAccion>\n" + 
			        //"            <ser:id></ser:id>\n" + 
			        "            <ser:userErp>GVERA</ser:userErp>\n" + 
			        "            <ser:docXml/>\n" + 
			        "         </ser:documento>\n" + 
			        "      </ser:documentosRequest>");

			    Source responsePayload = new StringSource("<ns2:documentosResponse xmlns:ns2=\"http://www.ebizlatin.com/service\">"
			    		+ "<ns2:documento><ns2:CodigoError>ESB101</ns2:CodigoError>"
			    		+ "<ns2:DataError>Documento inválido</ns2:DataError>"
			    		+ "</ns2:documento></ns2:documentosResponse>");
			    
			    mockClient.sendRequest(withPayload(requestPayload)).andExpect(payload(responsePayload));
		  
	  }
	  
	  @Test
	  public void whenQueryUserErpNoTagIsEmptyThenErrorESB100() {
		  
		  Source requestPayload = new StringSource(
			        "<ser:documentosRequest xmlns:ser=\"http://www.ebizlatin.com/service\">" + 
			        "         <ser:userOauth>rmarinos</ser:userOauth>\n" + 
			        "         <ser:pwdOauth>cGFzczEyMw==</ser:pwdOauth>\n" + 
			        "         <!--1 to 200 repetitions:-->\n" + 
			        "         <ser:documento>\n" + 
			        "            <ser:orgId>1b9a0ddb-1c6a-2687-a501-2885d082eba2</ser:orgId>\n" + 
			        "            <ser:rucComprador>PE20101045995</ser:rucComprador>\n" + 
			        "            <!--Optional:-->\n" + 
			        "            <ser:rucProveedor>PE20101045986</ser:rucProveedor>\n" + 
			        "            <ser:codigoAccion>O1</ser:codigoAccion>\n" + 
			        "            <ser:id>1</ser:id>\n" + 
			        //"            <ser:userErp>GVERA</ser:userErp>\n" + 
			        "            <ser:docXml/>\n" + 
			        "         </ser:documento>\n" + 
			        "      </ser:documentosRequest>");

			    Source responsePayload = new StringSource("<ns2:documentosResponse xmlns:ns2=\"http://www.ebizlatin.com/service\">"
			    		+ "<ns2:documento><ns2:CodigoError>ESB101</ns2:CodigoError>"
			    		+ "<ns2:DataError>Documento inválido</ns2:DataError>"
			    		+ "</ns2:documento></ns2:documentosResponse>");
			    
			    mockClient.sendRequest(withPayload(requestPayload)).andExpect(payload(responsePayload));
		  
	  }
	  
	  @Test
	  public void whenQueryRucProveedorTagIsEmptyThenErrorESB100() {
		  
		  Source requestPayload = new StringSource(
			        "<ser:documentosRequest xmlns:ser=\"http://www.ebizlatin.com/service\">" + 
			        "         <ser:userOauth>rmarinos</ser:userOauth>\n" + 
			        "         <ser:pwdOauth>cGFzczEyMw==</ser:pwdOauth>\n" + 
			        "         <!--1 to 200 repetitions:-->\n" + 
			        "         <ser:documento>\n" + 
			        "            <ser:orgId>1b9a0ddb-1c6a-2687-a501-2885d082eba2</ser:orgId>\n" + 
			        "            <ser:rucComprador>PE20101045995</ser:rucComprador>\n" + 
			        "            <!--Optional:-->\n" + 
			        //"            <ser:rucProveedor>PE20101045986</ser:rucProveedor>\n" + 
			        "            <ser:codigoAccion>O1</ser:codigoAccion>\n" + 
			        "            <ser:id>1</ser:id>\n" + 
			        "            <ser:userErp>GVERA</ser:userErp>\n" + 
			        "            <ser:docXml/>\n" + 
			        "         </ser:documento>\n" + 
			        "      </ser:documentosRequest>");

			    Source responsePayload = new StringSource("<ns2:documentosResponse xmlns:ns2=\"http://www.ebizlatin.com/service\">"
			    		+ "<ns2:documento><ns2:CodigoError>ESB101</ns2:CodigoError>"
			    		+ "<ns2:DataError>Documento inválido</ns2:DataError>"
			    		+ "</ns2:documento></ns2:documentosResponse>");
			    
			    mockClient.sendRequest(withPayload(requestPayload)).andExpect(payload(responsePayload));
		  
	  }
	  
	  @Test
	  public void whenQueryCodigoAcccionTagIsEmptyThenErrorESB100() {
		  
		  Source requestPayload = new StringSource(
			        "<ser:documentosRequest xmlns:ser=\"http://www.ebizlatin.com/service\">" + 
			        "         <ser:userOauth>rmarinos</ser:userOauth>\n" + 
			        "         <ser:pwdOauth>cGFzczEyMw==</ser:pwdOauth>\n" + 
			        "         <!--1 to 200 repetitions:-->\n" + 
			        "         <ser:documento>\n" + 
			        "            <ser:orgId>1b9a0ddb-1c6a-2687-a501-2885d082eba2</ser:orgId>\n" + 
			        "            <ser:rucComprador>PE20101045995</ser:rucComprador>\n" + 
			        "            <!--Optional:-->\n" + 
			        "            <ser:rucProveedor>PE20101045986</ser:rucProveedor>\n" + 
			        //"            <ser:codigoAccion>O1</ser:codigoAccion>\n" + 
			        "            <ser:id>1</ser:id>\n" + 
			        "            <ser:userErp>GVERA</ser:userErp>\n" + 
			        "            <ser:docXml/>\n" + 
			        "         </ser:documento>\n" + 
			        "      </ser:documentosRequest>");

			    Source responsePayload = new StringSource("<ns2:documentosResponse xmlns:ns2=\"http://www.ebizlatin.com/service\">"
			    		+ "<ns2:documento><ns2:CodigoError>ESB101</ns2:CodigoError>"
			    		+ "<ns2:DataError>Documento inválido</ns2:DataError>"
			    		+ "</ns2:documento></ns2:documentosResponse>");
			    
			    mockClient.sendRequest(withPayload(requestPayload)).andExpect(payload(responsePayload));
		  
	  }
	  
	  
	  @Test
	  public void whenQueryUserOauthNoTagIsEmptyThenErrorESB100() {
		  
		  Source requestPayload = new StringSource(
			        "<ser:documentosRequest xmlns:ser=\"http://www.ebizlatin.com/service\">" + 
			        //"         <ser:userOauth>rmarinos</ser:userOauth>\n" + 
			        "         <ser:pwdOauth>cGFzczEyMw==</ser:pwdOauth>\n" + 
			        "         <!--1 to 200 repetitions:-->\n" + 
			        "         <ser:documento>\n" + 
			        "            <ser:orgId>1b9a0ddb-1c6a-2687-a501-2885d082eba2</ser:orgId>\n" + 
			        "            <ser:rucComprador>PE20101045995</ser:rucComprador>\n" + 
			        "            <!--Optional:-->\n" + 
			        "            <ser:rucProveedor>PE20101045986</ser:rucProveedor>\n" + 
			        "            <ser:codigoAccion>O1</ser:codigoAccion>\n" + 
			        "            <ser:id>1</ser:id>\n" + 
			        "            <ser:userErp>GVERA</ser:userErp>\n" + 
			        "            <ser:docXml/>\n" + 
			        "         </ser:documento>\n" + 
			        "      </ser:documentosRequest>");

			    Source responsePayload = new StringSource("<ns2:documentosResponse xmlns:ns2=\"http://www.ebizlatin.com/service\">"
			    		+ "<ns2:documento><ns2:CodigoError>ESB101</ns2:CodigoError>"
			    		+ "<ns2:DataError>Documento inválido</ns2:DataError>"
			    		+ "</ns2:documento></ns2:documentosResponse>");
			    
			    mockClient.sendRequest(withPayload(requestPayload)).andExpect(payload(responsePayload));
		  
	  }
	  
	  @Test
	  public void whenQueryPwdOauthNoTagIsEmptyThenErrorESB100() {
		  
		  Source requestPayload = new StringSource(
			        "<ser:documentosRequest xmlns:ser=\"http://www.ebizlatin.com/service\">" + 
			        "         <ser:userOauth>rmarinos</ser:userOauth>\n" + 
			        //"         <ser:pwdOauth>cGFzczEyMw==</ser:pwdOauth>\n" + 
			        "         <!--1 to 200 repetitions:-->\n" + 
			        "         <ser:documento>\n" + 
			        "            <ser:orgId>1b9a0ddb-1c6a-2687-a501-2885d082eba2</ser:orgId>\n" + 
			        "            <ser:rucComprador>PE20101045995</ser:rucComprador>\n" + 
			        "            <!--Optional:-->\n" + 
			        "            <ser:rucProveedor>PE20101045986</ser:rucProveedor>\n" + 
			        "            <ser:codigoAccion>O1</ser:codigoAccion>\n" + 
			        "            <ser:id>1</ser:id>\n" + 
			        "            <ser:userErp>GVERA</ser:userErp>\n" + 
			        "            <ser:docXml/>\n" + 
			        "         </ser:documento>\n" + 
			        "      </ser:documentosRequest>");

			    Source responsePayload = new StringSource("<ns2:documentosResponse xmlns:ns2=\"http://www.ebizlatin.com/service\">"
			    		+ "<ns2:documento><ns2:CodigoError>ESB101</ns2:CodigoError>"
			    		+ "<ns2:DataError>Documento inválido</ns2:DataError>"
			    		+ "</ns2:documento></ns2:documentosResponse>");
			    
			    mockClient.sendRequest(withPayload(requestPayload)).andExpect(payload(responsePayload));
		  
	  }
	  
	  
	  
	  
	  
}
