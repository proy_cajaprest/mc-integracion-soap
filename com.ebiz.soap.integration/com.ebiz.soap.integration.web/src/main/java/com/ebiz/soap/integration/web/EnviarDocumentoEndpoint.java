package com.ebiz.soap.integration.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import com.ebiz.soap.integration.service.DocumentService;
import com.ebizlatin.xsd.DocumentoRequest;
import com.ebizlatin.xsd.DocumentoResponse;


@Endpoint
public class EnviarDocumentoEndpoint {
	
	
	private static final Logger LOGGER = LoggerFactory.getLogger(EnviarDocumentoEndpoint.class);

	private static final String NAMESPACE_URI = "http://www.ebizlatin.com/service";
	
	@Autowired
	private DocumentService ordenService;
	
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "documentoRequest")
	@ResponsePayload
	public DocumentoResponse process(@RequestPayload DocumentoRequest request) {
		
		
	
		LOGGER.info("documentoRequest.CodigoAcción:{}",request.getCodigoAccion());
		
		return ordenService.send(request);
		
	}

}
