package com.ebiz.soap.integration.web;

import java.util.List;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.core.io.ClassPathResource;
import org.springframework.ws.config.annotation.EnableWs;
import org.springframework.ws.config.annotation.WsConfigurerAdapter;
import org.springframework.ws.server.EndpointInterceptor;
import org.springframework.ws.soap.security.xwss.XwsSecurityInterceptor;

import org.springframework.ws.soap.security.xwss.callback.SpringDigestPasswordValidationCallbackHandler;

@EnableWs
@Configuration
@Profile("!unittest")
public class WebServiceSecurityConfig extends WsConfigurerAdapter {
		
	@Bean
	public XwsSecurityInterceptor securityInterceptor() {
		XwsSecurityInterceptor securityInterceptor = new XwsSecurityInterceptor();
		securityInterceptor.setCallbackHandler(callbackHandler());
		securityInterceptor.setPolicyConfiguration(new ClassPathResource("securityPolicy.xml"));
		return securityInterceptor;
	}
	
	@Bean
	UserDetailsServiceImpl getUserDetailsService() {
		UserDetailsServiceImpl detailsServiceImpl = new UserDetailsServiceImpl();
		return detailsServiceImpl;
	}

	@Bean
	SpringDigestPasswordValidationCallbackHandler callbackHandler() {
		SpringDigestPasswordValidationCallbackHandler callbackHandler = new SpringDigestPasswordValidationCallbackHandler();
		callbackHandler.setUserDetailsService(getUserDetailsService());
		return callbackHandler;
	}

	@Override
	public void addInterceptors(List<EndpointInterceptor> interceptors) {
		
		interceptors.add(securityInterceptor());
	}


}
