package com.ebiz.soap.integration.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = {"com.ebiz.soap.integration.config",
			"com.ebiz.soap.integration.web",
		  "com.ebiz.soap.integration.service",
		  "com.ebiz.soap.integration.api.ordenes",
		  "com.ebiz.soap.integration.api.pagopublicacion",
		  "com.ebiz.soap.integration.api.facturaanulacion",
		  "com.ebiz.soap.integration.api.oauth",
		  "com.ebiz.soap.integration.dao"})
//@SpringBootApplication()
public class SpringWsApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringWsApplication.class, args);
	}
}
