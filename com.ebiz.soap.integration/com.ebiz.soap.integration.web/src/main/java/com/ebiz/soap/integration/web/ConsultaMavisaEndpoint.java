package com.ebiz.soap.integration.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import com.ebiz.soap.integration.service.DocumentService;
import com.ebizlatin.xsd.DocumentosRequest;
import com.ebizlatin.xsd.DocumentosResponse;

@Endpoint
public class ConsultaMavisaEndpoint {
	
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ConsultaMavisaEndpoint.class);

	private static final String NAMESPACE_URI = "http://www.ebizlatin.com/service";
	
	@Autowired
	private DocumentService ordenService;
	
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "documentosRequest")
	@ResponsePayload
	public DocumentosResponse process(@RequestPayload DocumentosRequest request) {
		
		
		if (!request.getDocumento().isEmpty()) {
			LOGGER.info("Endpoint documentos consultados: {}", request.getDocumento().size());
			return ordenService.query(request);
		}else {
			LOGGER.info("documentos consultados: 0");
		}
		
			
		return null;
		
	}
	


}
