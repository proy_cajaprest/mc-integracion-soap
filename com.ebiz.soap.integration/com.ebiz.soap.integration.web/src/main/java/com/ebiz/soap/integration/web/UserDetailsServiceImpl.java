package com.ebiz.soap.integration.web;


import java.util.ArrayList;
import java.util.List;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;


public class UserDetailsServiceImpl implements UserDetailsService {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	
	@Value("${environment.user}")
	private String environmentUser;
	
	@Value("${environment.password}")
	private String environmentPwd;
	
	

	public User loadUserByUsername(String username) throws UsernameNotFoundException {
	

		logger.debug("username:{}", username);
		User user = new User(environmentUser, environmentPwd, getGrantedAuthorities());
		return user;

	}

	private List<GrantedAuthority> getGrantedAuthorities() {

		List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();

		authorities.add(new SimpleGrantedAuthority("ROLE_USER"));
		authorities.add(new SimpleGrantedAuthority("ROLE_ADMIN"));
		authorities.add(new SimpleGrantedAuthority("ADMIN"));
		authorities.add(new SimpleGrantedAuthority("USER"));

		return authorities;
	}

}