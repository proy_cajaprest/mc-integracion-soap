package com.ebiz.soap.integration.web;


import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.web.client.RestTemplate;
import org.springframework.ws.config.annotation.EnableWs;

import org.springframework.ws.transport.http.MessageDispatcherServlet;
import org.springframework.ws.wsdl.wsdl11.SimpleWsdl11Definition;
import org.springframework.ws.wsdl.wsdl11.Wsdl11Definition;

import com.ebiz.soap.integration.schemas.DocumentXsdService;
import com.ebiz.soap.integration.schemas.impl.DocumentXsdServiceImpl;
import com.ebiz.soap.integration.wsdl.DocumentWsdlService;
import com.ebiz.soap.integration.wsdl.impl.DocumentWsdlServiceImpl;


@EnableWs
@Configuration
public class WebServiceConfig  {
	
	@Bean
	public ServletRegistrationBean messageDispatcherServlet(ApplicationContext applicationContext) {
		MessageDispatcherServlet servlet = new MessageDispatcherServlet();
		servlet.setApplicationContext(applicationContext);
		servlet.setTransformWsdlLocations(true);
		return new ServletRegistrationBean(servlet, "/ws/*");
	}

	@Bean(name = "ebizWebService")
	public Wsdl11Definition defaultWsdl11Definition() {
		SimpleWsdl11Definition wsdl11Definition = new SimpleWsdl11Definition();
		wsdl11Definition.setWsdl(new ClassPathResource("/wsdl/EbizWebService.wsdl"));
		return wsdl11Definition;

	}
	
	

	@Bean
	public RestTemplate restTemplate(RestTemplateBuilder builder) {
		RestTemplate restTemplate = builder.build();
		
		return restTemplate;
	}
	
	@Bean
	public DocumentWsdlService DocumentWsdlService() {
		return new DocumentWsdlServiceImpl();
	}
	
	@Bean
	public DocumentXsdService DocumentXsdService() {
		return new DocumentXsdServiceImpl();
	}

}
