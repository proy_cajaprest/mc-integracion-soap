package com.ebiz.soap.integration;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.List;

import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.ebiz.soap.integration.api.guia.exception.ParseJsonException;
import com.ebiz.soap.integration.api.guia.model.GDUPLOADMQ;
import com.ebiz.soap.integration.api.guia.model.GuiaDespacho;
import com.ebiz.soap.integration.api.guia.proxy.GuiaDespachoApiProxy;
import com.ebiz.soap.integration.api.guia.proxy.impl.GuiaDespachoApiProxyImpl;
import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(SpringRunner.class)
@ActiveProfiles("Default")
public class DocumentServiceTest {

	@Autowired
	private GuiaDespachoApiProxy guiaDespachoApiProxy;

	@Test
	public void validate1() throws Exception {

		String requestJSON = "{\"GDUPLOADMQ\": {  \"RucProveedor\": \"PE20254053822\",  \"RazonSocialProveedor\": \"PRODUCTOS DE ACERO CASSADO S.A.\",  \"GuiaDespacho\": [    {\n"
				+ "		\"IdTablaPeso\":\"10000\",\n" + "		\"IdRegistroPeso\":\"0000027\",\n"
				+ "		\"IdTablaVolumen\":\"10000\",\n" + "		\"IdRegistroVolumen\":\"0000015\",\n"
				+ "		\"IsoPeso\": \"lb\",\n" + "		\"IsoVolumen\":\"ft3\",\n"
				+ "		\"NumeroGuia\":\"7531-00000030\",\n" + "		\"RucCliente\":\"PE20101164901\",\n"
				+ "		\"RazonSocialCliente\":\"WONG\",\n" + "		\"FechaEmision\":\"2017-09-22\",\n"
				+ "		\"FechaInicioTraslado\":\"2017-09-24\",\n" + "		\"FechaProbArribo\":\"2017-09-27\",\n"
				+ "		\"IdTablaMotivoGuia\":\"10010\",\n" + "		\"IdRegistroMotivoGuia\":\"0000001\",\n"
				+ "		\"MotivoGuia\":\"Transporte\",\n" + "		\"Observaciones\":\"Obs Guia\",\n"
				+ "		\"IdTablaTipDocTransportista\":\"10015\",\n"
				+ "		\"IdRegistroTipDocTransportista\":\"06\",\n" + "		\"TipDocTransportista\":\"ruc\",\n"
				+ "		\"NumDocTransportista\":\"PE20700673699\",\n"
				+ "		\"RazonSocialTransportista\":\"Sr. transportista\",\n" + "		\"PlacaoNave\":\"E7A712\",\n"
				+ "		\"DirTransportista\":\"Direc transportista\",\n" + "		\"RegistroMTC\":\"regmtc\",\n"
				+ "		\"IdTablaTipoTransporte\":\"10011\",\n" + "		\"IdRegistroTipoTransporte\":\"0000001\",\n"
				+ "		\"TipoTransporte\":\"Terrestre\",\n" + "		\"PuntoPartida\":\"PuntoPartida\",\n"
				+ "		\"PuntoLlegada\":\"PuntoLlegada\",\n" + "		\"AlmacenDestino\":\"AlmacenDestino\",\n"
				+ "		\"TotalBultos\":20,\n" + "		\"TotalVolumen\":50.45,\n" + "		\"TotalPesoBruto\":20.45,\n"
				+ "		\"Tara\":10.4,\n" + "		\"TotalPesoNeto\":13.5,\n"
				+ "		\"IdOrganizacionCreacion\":\"10000000-0000-0000-0000-000000000042\",\n"
				+ "		\"IdUsuarioCreacion\":\"00000000-0000-0000-0000-000000000023\",\n"
				+ "		\"IdOrganizacionCompradora\":\"58e68c11-a73c-4027-b1dd-4e511ed22222\",\n"
				+ "		\"IdOrganizacionProveedora\":\"b62d6238-4300-49e8-b252-b1e20a0b73ff\",\n"
				+ "		\"TipoGuia\":\"M\",      \"ItemGuia\": [        {          \"IdOc\":\"89d92c35-b17f-496e-a9b5-651355251958\",\"NumeroOrden\":\"1111014555\",\"NumeroParte\":\"CODPROD1\",\"DescripcionItem\":\"Es una descripcion item\",\"PesoNetoItem\":50.6,\"IdTablaunidadMedida\":\"10000\",\"IdRegistroUnidadMedida\":\"0000043\",\"UnidadMedidaItem\":\"Unidades\",\"IdProductoxOc\":\"02dcc28a-22e0-4e1a-944d-778e11209c47\",\"NumeroItem\":\"1\",\"NumeroItemOC\":\"001\",\"IdRegistroUnidadPeso\":\"0000022\",\"UnidadPeso\":\"Kilogram\",\"CantidadDespachada\":10,\"CantidadPedido\":15,\"Cantidadaf\":2,\"Destino\":\"1100\"\n"
				+ "		},{\"IdOc\":\"89d92c35-b17f-496e-a9b5-651355251958\",\"NumeroOrden\":\"1111014555\",\"NumeroParte\":\"CODPROD2\",\"DescripcionItem\":\"Es una descripcion item\",\"PesoNetoItem\":50.6,\"IdTablaunidadMedida\":\"10000\",\"IdRegistroUnidadMedida\":\"0000043\",\"UnidadMedidaItem\":\"Unidades\",\"IdProductoxOc\":\"60e9cb4b-ed5d-49a4-8062-0dbe04512bb0\",\"NumeroItem\":\"2\",\"NumeroItemOC\":\"002\",\"IdRegistroUnidadPeso\":\"0000022\",\"UnidadPeso\":\"Kilogram\",\"CantidadDespachada\":15,\"CantidadPedido\":15,\"Cantidadaf\":0,\"Destino\":\"1100\"\n"
				+ "		}]    }  ]}\n" + "}";

			GDUPLOADMQ  gdUPLOADMQ = guiaDespachoApiProxy.unmarshalGuiaCompraRequest(requestJSON);
			assertEquals(gdUPLOADMQ.getRucProveedor(), "PE20254053822"); 
			assertEquals(gdUPLOADMQ.getRazonSocialProveedor(), "PRODUCTOS DE ACERO CASSADO S.A."); 
			assertEquals(gdUPLOADMQ.getGuiaDespacho().size(), 1); 
			
			GuiaDespacho guiaDespacho = gdUPLOADMQ.getGuiaDespacho().get(0);
			
			assertEquals(guiaDespacho.getNumeroGuia(), "7531-00000030");
			
	
			
		
	}

	@Profile("Default")
	@Configuration
	static public class ConfigTest001 {

		

		@Bean
		@Primary
		public MappingJackson2HttpMessageConverter mappingJackson2HttpMessageConverter() {
			MappingJackson2HttpMessageConverter jacksonMessageConverter = new MappingJackson2HttpMessageConverter();

			return jacksonMessageConverter;
		}
		
		@Bean
		@Primary
		public ObjectMapper objectMapper() {
			MappingJackson2HttpMessageConverter jacksonMessageConverter = mappingJackson2HttpMessageConverter() ;
			return jacksonMessageConverter.getObjectMapper();
		}
		
		
		@Bean
		@Primary
		public GuiaDespachoApiProxy guiaDespachoApiProxy() {
			GuiaDespachoApiProxy guiaDespachoApiProxy = new GuiaDespachoApiProxyImpl();
			return guiaDespachoApiProxy;
		}

	}

}
