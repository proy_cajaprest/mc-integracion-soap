package com.ebiz.soap.integration.api.guia.proxy.impl;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ebiz.soap.integration.api.guia.exception.ParseJsonException;
import com.ebiz.soap.integration.api.guia.model.GDUPLOADMQ;
import com.ebiz.soap.integration.api.guia.model.RequestCreateGuia;
import com.ebiz.soap.integration.api.guia.proxy.GuiaDespachoApiProxy;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Repository
public class GuiaDespachoApiProxyImpl implements GuiaDespachoApiProxy {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(GuiaDespachoApiProxyImpl.class);
	
	
	@Autowired
	private ObjectMapper objectMapper;

	@Override
	public GDUPLOADMQ unmarshalGuiaCompraRequest(String json) throws ParseJsonException {
		
		RequestCreateGuia gdUploadMq = null;
		
		try {
			
			gdUploadMq = objectMapper.readValue(json, RequestCreateGuia.class);
			
			LOGGER.debug("Guia Compra");
			
		} catch (JsonParseException e) {
			LOGGER.error("JsonParseException",e);
		} catch (JsonMappingException e) {
			LOGGER.error("JsonMappingException",e);
		} catch (IOException e) {
			LOGGER.error("IOException",e);
		}
		
		return gdUploadMq.getGDUPLOADMQ();
		
	}
	
	
	
	

}
