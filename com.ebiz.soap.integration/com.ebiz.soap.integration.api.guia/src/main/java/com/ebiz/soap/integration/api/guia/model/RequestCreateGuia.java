package com.ebiz.soap.integration.api.guia.model;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "GDUPLOADMQ"
})
public class RequestCreateGuia {
	
	@JsonProperty("GDUPLOADMQ")
    private GDUPLOADMQ gDUPLOADMQ;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("GDUPLOADMQ")
    public GDUPLOADMQ getGDUPLOADMQ() {
        return gDUPLOADMQ;
    }

    @JsonProperty("GDUPLOADMQ")
    public void setGDUPLOADMQ(GDUPLOADMQ gDUPLOADMQ) {
        this.gDUPLOADMQ = gDUPLOADMQ;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
