
package com.ebiz.soap.integration.api.guia.model;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "NumeroItem",
    "NumeroItemOC",
    "NumeroOrden",
    "NumeroParte",
    "DescripcionItem",
    "CantidadItem",
    "UnidadMedidaItem",
    "PesoNetoItem",
    "CodUnidadPesoItem",
    "ClaseMovimiento",
    "SubTotalItemGuiaLocal",
    "SubTotalItemGuia",
    "MonedaItemGuia",
    "IndicadorEntregaFinal",
    "NumeroMaterial",
    "CentroEntrega",
    "NumeroLote",
    "IndicadorBorradoOC",
    "AlmacenItemGuia",
    "GrupoArticulos",
    "CantidadPendiente",
    "CantidadPedido",
    "CantidadAtendida",
    "CantidadUnidadMedidaPedido",
    "UnidadMedidaPedido",
    "UnidadMedidaPrecioPedido",
    "PrecioItemOC",
    "CantidadBaseItemOC",
    "SubTotalItemOC",
    "SubTotalBrutoItemOC",
    "IndicadorEntradaMercancias",
    "IndicadorRecepcionFactura",
    "ClaveCondicionPago",
    "CondicionPago",
    "FechaEntregaItemOC",
    "CantidadDespachadaUnidadPedido",
    "CantidadAtendidaUnidadGuia",
    "CantidadAtendidaUnidadPrecioPedido",
    "CantidadUnidadMedidaPrecioPedido",
    "EstadoOpcionItem",
    "DescripcionEstadoOpcionItem",
    "ObservacionItem",
    "NumeroActivoFijo"
})
public class ItemGuium {

    @JsonProperty("NumeroItem")
    private String numeroItem;
    @JsonProperty("NumeroItemOC")
    private String numeroItemOC;
    @JsonProperty("NumeroOrden")
    private String numeroOrden;
    @JsonProperty("NumeroParte")
    private String numeroParte;
    @JsonProperty("DescripcionItem")
    private String descripcionItem;
    @JsonProperty("CantidadItem")
    private String cantidadItem;
    @JsonProperty("UnidadMedidaItem")
    private String unidadMedidaItem;
    @JsonProperty("PesoNetoItem")
    private String pesoNetoItem;
    @JsonProperty("CodUnidadPesoItem")
    private String codUnidadPesoItem;
    @JsonProperty("ClaseMovimiento")
    private String claseMovimiento;
    @JsonProperty("SubTotalItemGuiaLocal")
    private String subTotalItemGuiaLocal;
    @JsonProperty("SubTotalItemGuia")
    private String subTotalItemGuia;
    @JsonProperty("MonedaItemGuia")
    private String monedaItemGuia;
    @JsonProperty("IndicadorEntregaFinal")
    private String indicadorEntregaFinal;
    @JsonProperty("NumeroMaterial")
    private String numeroMaterial;
    @JsonProperty("CentroEntrega")
    private String centroEntrega;
    @JsonProperty("NumeroLote")
    private String numeroLote;
    @JsonProperty("IndicadorBorradoOC")
    private String indicadorBorradoOC;
    @JsonProperty("AlmacenItemGuia")
    private String almacenItemGuia;
    @JsonProperty("GrupoArticulos")
    private String grupoArticulos;
    @JsonProperty("CantidadPendiente")
    private String cantidadPendiente;
    @JsonProperty("CantidadPedido")
    private String cantidadPedido;
    @JsonProperty("CantidadAtendida")
    private String cantidadAtendida;
    @JsonProperty("CantidadUnidadMedidaPedido")
    private String cantidadUnidadMedidaPedido;
    @JsonProperty("UnidadMedidaPedido")
    private String unidadMedidaPedido;
    @JsonProperty("UnidadMedidaPrecioPedido")
    private String unidadMedidaPrecioPedido;
    @JsonProperty("PrecioItemOC")
    private String precioItemOC;
    @JsonProperty("CantidadBaseItemOC")
    private String cantidadBaseItemOC;
    @JsonProperty("SubTotalItemOC")
    private String subTotalItemOC;
    @JsonProperty("SubTotalBrutoItemOC")
    private String subTotalBrutoItemOC;
    @JsonProperty("IndicadorEntradaMercancias")
    private String indicadorEntradaMercancias;
    @JsonProperty("IndicadorRecepcionFactura")
    private String indicadorRecepcionFactura;
    @JsonProperty("ClaveCondicionPago")
    private String claveCondicionPago;
    @JsonProperty("CondicionPago")
    private String condicionPago;
    @JsonProperty("FechaEntregaItemOC")
    private String fechaEntregaItemOC;
    @JsonProperty("CantidadDespachadaUnidadPedido")
    private String cantidadDespachadaUnidadPedido;
    @JsonProperty("CantidadAtendidaUnidadGuia")
    private String cantidadAtendidaUnidadGuia;
    @JsonProperty("CantidadAtendidaUnidadPrecioPedido")
    private String cantidadAtendidaUnidadPrecioPedido;
    @JsonProperty("CantidadUnidadMedidaPrecioPedido")
    private String cantidadUnidadMedidaPrecioPedido;
    @JsonProperty("EstadoOpcionItem")
    private String estadoOpcionItem;
    @JsonProperty("DescripcionEstadoOpcionItem")
    private String descripcionEstadoOpcionItem;
    @JsonProperty("ObservacionItem")
    private String observacionItem;
    @JsonProperty("NumeroActivoFijo")
    private String numeroActivoFijo;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("NumeroItem")
    public String getNumeroItem() {
        return numeroItem;
    }

    @JsonProperty("NumeroItem")
    public void setNumeroItem(String numeroItem) {
        this.numeroItem = numeroItem;
    }

    @JsonProperty("NumeroItemOC")
    public String getNumeroItemOC() {
        return numeroItemOC;
    }

    @JsonProperty("NumeroItemOC")
    public void setNumeroItemOC(String numeroItemOC) {
        this.numeroItemOC = numeroItemOC;
    }

    @JsonProperty("NumeroOrden")
    public String getNumeroOrden() {
        return numeroOrden;
    }

    @JsonProperty("NumeroOrden")
    public void setNumeroOrden(String numeroOrden) {
        this.numeroOrden = numeroOrden;
    }

    @JsonProperty("NumeroParte")
    public String getNumeroParte() {
        return numeroParte;
    }

    @JsonProperty("NumeroParte")
    public void setNumeroParte(String numeroParte) {
        this.numeroParte = numeroParte;
    }

    @JsonProperty("DescripcionItem")
    public String getDescripcionItem() {
        return descripcionItem;
    }

    @JsonProperty("DescripcionItem")
    public void setDescripcionItem(String descripcionItem) {
        this.descripcionItem = descripcionItem;
    }

    @JsonProperty("CantidadItem")
    public String getCantidadItem() {
        return cantidadItem;
    }

    @JsonProperty("CantidadItem")
    public void setCantidadItem(String cantidadItem) {
        this.cantidadItem = cantidadItem;
    }

    @JsonProperty("UnidadMedidaItem")
    public String getUnidadMedidaItem() {
        return unidadMedidaItem;
    }

    @JsonProperty("UnidadMedidaItem")
    public void setUnidadMedidaItem(String unidadMedidaItem) {
        this.unidadMedidaItem = unidadMedidaItem;
    }

    @JsonProperty("PesoNetoItem")
    public String getPesoNetoItem() {
        return pesoNetoItem;
    }

    @JsonProperty("PesoNetoItem")
    public void setPesoNetoItem(String pesoNetoItem) {
        this.pesoNetoItem = pesoNetoItem;
    }

    @JsonProperty("CodUnidadPesoItem")
    public String getCodUnidadPesoItem() {
        return codUnidadPesoItem;
    }

    @JsonProperty("CodUnidadPesoItem")
    public void setCodUnidadPesoItem(String codUnidadPesoItem) {
        this.codUnidadPesoItem = codUnidadPesoItem;
    }

    @JsonProperty("ClaseMovimiento")
    public String getClaseMovimiento() {
        return claseMovimiento;
    }

    @JsonProperty("ClaseMovimiento")
    public void setClaseMovimiento(String claseMovimiento) {
        this.claseMovimiento = claseMovimiento;
    }

    @JsonProperty("SubTotalItemGuiaLocal")
    public String getSubTotalItemGuiaLocal() {
        return subTotalItemGuiaLocal;
    }

    @JsonProperty("SubTotalItemGuiaLocal")
    public void setSubTotalItemGuiaLocal(String subTotalItemGuiaLocal) {
        this.subTotalItemGuiaLocal = subTotalItemGuiaLocal;
    }

    @JsonProperty("SubTotalItemGuia")
    public String getSubTotalItemGuia() {
        return subTotalItemGuia;
    }

    @JsonProperty("SubTotalItemGuia")
    public void setSubTotalItemGuia(String subTotalItemGuia) {
        this.subTotalItemGuia = subTotalItemGuia;
    }

    @JsonProperty("MonedaItemGuia")
    public String getMonedaItemGuia() {
        return monedaItemGuia;
    }

    @JsonProperty("MonedaItemGuia")
    public void setMonedaItemGuia(String monedaItemGuia) {
        this.monedaItemGuia = monedaItemGuia;
    }

    @JsonProperty("IndicadorEntregaFinal")
    public String getIndicadorEntregaFinal() {
        return indicadorEntregaFinal;
    }

    @JsonProperty("IndicadorEntregaFinal")
    public void setIndicadorEntregaFinal(String indicadorEntregaFinal) {
        this.indicadorEntregaFinal = indicadorEntregaFinal;
    }

    @JsonProperty("NumeroMaterial")
    public String getNumeroMaterial() {
        return numeroMaterial;
    }

    @JsonProperty("NumeroMaterial")
    public void setNumeroMaterial(String numeroMaterial) {
        this.numeroMaterial = numeroMaterial;
    }

    @JsonProperty("CentroEntrega")
    public String getCentroEntrega() {
        return centroEntrega;
    }

    @JsonProperty("CentroEntrega")
    public void setCentroEntrega(String centroEntrega) {
        this.centroEntrega = centroEntrega;
    }

    @JsonProperty("NumeroLote")
    public String getNumeroLote() {
        return numeroLote;
    }

    @JsonProperty("NumeroLote")
    public void setNumeroLote(String numeroLote) {
        this.numeroLote = numeroLote;
    }

    @JsonProperty("IndicadorBorradoOC")
    public String getIndicadorBorradoOC() {
        return indicadorBorradoOC;
    }

    @JsonProperty("IndicadorBorradoOC")
    public void setIndicadorBorradoOC(String indicadorBorradoOC) {
        this.indicadorBorradoOC = indicadorBorradoOC;
    }

    @JsonProperty("AlmacenItemGuia")
    public String getAlmacenItemGuia() {
        return almacenItemGuia;
    }

    @JsonProperty("AlmacenItemGuia")
    public void setAlmacenItemGuia(String almacenItemGuia) {
        this.almacenItemGuia = almacenItemGuia;
    }

    @JsonProperty("GrupoArticulos")
    public String getGrupoArticulos() {
        return grupoArticulos;
    }

    @JsonProperty("GrupoArticulos")
    public void setGrupoArticulos(String grupoArticulos) {
        this.grupoArticulos = grupoArticulos;
    }

    @JsonProperty("CantidadPendiente")
    public String getCantidadPendiente() {
        return cantidadPendiente;
    }

    @JsonProperty("CantidadPendiente")
    public void setCantidadPendiente(String cantidadPendiente) {
        this.cantidadPendiente = cantidadPendiente;
    }

    @JsonProperty("CantidadPedido")
    public String getCantidadPedido() {
        return cantidadPedido;
    }

    @JsonProperty("CantidadPedido")
    public void setCantidadPedido(String cantidadPedido) {
        this.cantidadPedido = cantidadPedido;
    }

    @JsonProperty("CantidadAtendida")
    public String getCantidadAtendida() {
        return cantidadAtendida;
    }

    @JsonProperty("CantidadAtendida")
    public void setCantidadAtendida(String cantidadAtendida) {
        this.cantidadAtendida = cantidadAtendida;
    }

    @JsonProperty("CantidadUnidadMedidaPedido")
    public String getCantidadUnidadMedidaPedido() {
        return cantidadUnidadMedidaPedido;
    }

    @JsonProperty("CantidadUnidadMedidaPedido")
    public void setCantidadUnidadMedidaPedido(String cantidadUnidadMedidaPedido) {
        this.cantidadUnidadMedidaPedido = cantidadUnidadMedidaPedido;
    }

    @JsonProperty("UnidadMedidaPedido")
    public String getUnidadMedidaPedido() {
        return unidadMedidaPedido;
    }

    @JsonProperty("UnidadMedidaPedido")
    public void setUnidadMedidaPedido(String unidadMedidaPedido) {
        this.unidadMedidaPedido = unidadMedidaPedido;
    }

    @JsonProperty("UnidadMedidaPrecioPedido")
    public String getUnidadMedidaPrecioPedido() {
        return unidadMedidaPrecioPedido;
    }

    @JsonProperty("UnidadMedidaPrecioPedido")
    public void setUnidadMedidaPrecioPedido(String unidadMedidaPrecioPedido) {
        this.unidadMedidaPrecioPedido = unidadMedidaPrecioPedido;
    }

    @JsonProperty("PrecioItemOC")
    public String getPrecioItemOC() {
        return precioItemOC;
    }

    @JsonProperty("PrecioItemOC")
    public void setPrecioItemOC(String precioItemOC) {
        this.precioItemOC = precioItemOC;
    }

    @JsonProperty("CantidadBaseItemOC")
    public String getCantidadBaseItemOC() {
        return cantidadBaseItemOC;
    }

    @JsonProperty("CantidadBaseItemOC")
    public void setCantidadBaseItemOC(String cantidadBaseItemOC) {
        this.cantidadBaseItemOC = cantidadBaseItemOC;
    }

    @JsonProperty("SubTotalItemOC")
    public String getSubTotalItemOC() {
        return subTotalItemOC;
    }

    @JsonProperty("SubTotalItemOC")
    public void setSubTotalItemOC(String subTotalItemOC) {
        this.subTotalItemOC = subTotalItemOC;
    }

    @JsonProperty("SubTotalBrutoItemOC")
    public String getSubTotalBrutoItemOC() {
        return subTotalBrutoItemOC;
    }

    @JsonProperty("SubTotalBrutoItemOC")
    public void setSubTotalBrutoItemOC(String subTotalBrutoItemOC) {
        this.subTotalBrutoItemOC = subTotalBrutoItemOC;
    }

    @JsonProperty("IndicadorEntradaMercancias")
    public String getIndicadorEntradaMercancias() {
        return indicadorEntradaMercancias;
    }

    @JsonProperty("IndicadorEntradaMercancias")
    public void setIndicadorEntradaMercancias(String indicadorEntradaMercancias) {
        this.indicadorEntradaMercancias = indicadorEntradaMercancias;
    }

    @JsonProperty("IndicadorRecepcionFactura")
    public String getIndicadorRecepcionFactura() {
        return indicadorRecepcionFactura;
    }

    @JsonProperty("IndicadorRecepcionFactura")
    public void setIndicadorRecepcionFactura(String indicadorRecepcionFactura) {
        this.indicadorRecepcionFactura = indicadorRecepcionFactura;
    }

    @JsonProperty("ClaveCondicionPago")
    public String getClaveCondicionPago() {
        return claveCondicionPago;
    }

    @JsonProperty("ClaveCondicionPago")
    public void setClaveCondicionPago(String claveCondicionPago) {
        this.claveCondicionPago = claveCondicionPago;
    }

    @JsonProperty("CondicionPago")
    public String getCondicionPago() {
        return condicionPago;
    }

    @JsonProperty("CondicionPago")
    public void setCondicionPago(String condicionPago) {
        this.condicionPago = condicionPago;
    }

    @JsonProperty("FechaEntregaItemOC")
    public String getFechaEntregaItemOC() {
        return fechaEntregaItemOC;
    }

    @JsonProperty("FechaEntregaItemOC")
    public void setFechaEntregaItemOC(String fechaEntregaItemOC) {
        this.fechaEntregaItemOC = fechaEntregaItemOC;
    }

    @JsonProperty("CantidadDespachadaUnidadPedido")
    public String getCantidadDespachadaUnidadPedido() {
        return cantidadDespachadaUnidadPedido;
    }

    @JsonProperty("CantidadDespachadaUnidadPedido")
    public void setCantidadDespachadaUnidadPedido(String cantidadDespachadaUnidadPedido) {
        this.cantidadDespachadaUnidadPedido = cantidadDespachadaUnidadPedido;
    }

    @JsonProperty("CantidadAtendidaUnidadGuia")
    public String getCantidadAtendidaUnidadGuia() {
        return cantidadAtendidaUnidadGuia;
    }

    @JsonProperty("CantidadAtendidaUnidadGuia")
    public void setCantidadAtendidaUnidadGuia(String cantidadAtendidaUnidadGuia) {
        this.cantidadAtendidaUnidadGuia = cantidadAtendidaUnidadGuia;
    }

    @JsonProperty("CantidadAtendidaUnidadPrecioPedido")
    public String getCantidadAtendidaUnidadPrecioPedido() {
        return cantidadAtendidaUnidadPrecioPedido;
    }

    @JsonProperty("CantidadAtendidaUnidadPrecioPedido")
    public void setCantidadAtendidaUnidadPrecioPedido(String cantidadAtendidaUnidadPrecioPedido) {
        this.cantidadAtendidaUnidadPrecioPedido = cantidadAtendidaUnidadPrecioPedido;
    }

    @JsonProperty("CantidadUnidadMedidaPrecioPedido")
    public String getCantidadUnidadMedidaPrecioPedido() {
        return cantidadUnidadMedidaPrecioPedido;
    }

    @JsonProperty("CantidadUnidadMedidaPrecioPedido")
    public void setCantidadUnidadMedidaPrecioPedido(String cantidadUnidadMedidaPrecioPedido) {
        this.cantidadUnidadMedidaPrecioPedido = cantidadUnidadMedidaPrecioPedido;
    }

    @JsonProperty("EstadoOpcionItem")
    public String getEstadoOpcionItem() {
        return estadoOpcionItem;
    }

    @JsonProperty("EstadoOpcionItem")
    public void setEstadoOpcionItem(String estadoOpcionItem) {
        this.estadoOpcionItem = estadoOpcionItem;
    }

    @JsonProperty("DescripcionEstadoOpcionItem")
    public String getDescripcionEstadoOpcionItem() {
        return descripcionEstadoOpcionItem;
    }

    @JsonProperty("DescripcionEstadoOpcionItem")
    public void setDescripcionEstadoOpcionItem(String descripcionEstadoOpcionItem) {
        this.descripcionEstadoOpcionItem = descripcionEstadoOpcionItem;
    }

    @JsonProperty("ObservacionItem")
    public String getObservacionItem() {
        return observacionItem;
    }

    @JsonProperty("ObservacionItem")
    public void setObservacionItem(String observacionItem) {
        this.observacionItem = observacionItem;
    }

    @JsonProperty("NumeroActivoFijo")
    public String getNumeroActivoFijo() {
        return numeroActivoFijo;
    }

    @JsonProperty("NumeroActivoFijo")
    public void setNumeroActivoFijo(String numeroActivoFijo) {
        this.numeroActivoFijo = numeroActivoFijo;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
