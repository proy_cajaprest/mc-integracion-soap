
package com.ebiz.soap.integration.api.guia.model;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "RucProveedor",
    "RazonSocialProveedor",
    "DireccionProveedor",
    "GuiaDespacho"
})
public class GDUPLOADMQ {

    @JsonProperty("RucProveedor")
    private String rucProveedor;
    @JsonProperty("RazonSocialProveedor")
    private String razonSocialProveedor;
    @JsonProperty("DireccionProveedor")
    private String direccionProveedor;
    @JsonProperty("GuiaDespacho")
    private List<GuiaDespacho> guiaDespacho = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("RucProveedor")
    public String getRucProveedor() {
        return rucProveedor;
    }

    @JsonProperty("RucProveedor")
    public void setRucProveedor(String rucProveedor) {
        this.rucProveedor = rucProveedor;
    }

    @JsonProperty("RazonSocialProveedor")
    public String getRazonSocialProveedor() {
        return razonSocialProveedor;
    }

    @JsonProperty("RazonSocialProveedor")
    public void setRazonSocialProveedor(String razonSocialProveedor) {
        this.razonSocialProveedor = razonSocialProveedor;
    }

    @JsonProperty("DireccionProveedor")
    public String getDireccionProveedor() {
        return direccionProveedor;
    }

    @JsonProperty("DireccionProveedor")
    public void setDireccionProveedor(String direccionProveedor) {
        this.direccionProveedor = direccionProveedor;
    }

    @JsonProperty("GuiaDespacho")
    public List<GuiaDespacho> getGuiaDespacho() {
        return guiaDespacho;
    }

    @JsonProperty("GuiaDespacho")
    public void setGuiaDespacho(List<GuiaDespacho> guiaDespacho) {
        this.guiaDespacho = guiaDespacho;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
