package com.ebiz.soap.integration.api.guia.proxy;

import com.ebiz.soap.integration.api.guia.exception.ParseJsonException;
import com.ebiz.soap.integration.api.guia.model.GDUPLOADMQ;

public interface GuiaDespachoApiProxy {
	
	GDUPLOADMQ unmarshalGuiaCompraRequest(String json) throws ParseJsonException;

}
