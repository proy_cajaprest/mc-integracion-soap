
package com.ebiz.soap.integration.api.guia.model;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "NumeroGuia",
    "idGuia",
    "NumeroOC",
    "FechaEmision",
    "RucCliente",
    "FechaCreacion",
    "FechaEnvio",
    "CodigoAlmacen",
    "MotivoGuia",
    "PuntoPartida",
    "PuntoLlegada",
    "TipDocTransportista",
    "NumDocTransportista",
    "RazonSocialTransportista",
    "DirTransportista",
    "TipoTransporte",
    "PlacaoNave",
    "RegistroMTC",
    "FechaInicioTraslado",
    "FechaProbArribo",
    "TotalBultos",
    "TotalPesoNeto",
    "TotalPesoBruto",
    "Tara",
    "TotalVolumen",
    "CodUnidadPeso",
    "CodUnidadVolumen",
    "Observaciones",
    "EjercicioGuia",
    "ClaseDocumentoGuia",
    "ClaseOperacionGuia",
    "FechaContabilizacionGuia",
    "Sociedad",
    "CodigoERPProveedor",
    "TipoGuia",
    "UsuarioCreacionGuia",
    "CodigoGuiaERP",
    "CodigoMercaderiaB2M",
    "MonedaGuia",
    "ItemGuia",
    "CheckPoint",
    "IdOrganizacionCompradora"
})
public class GuiaDespacho {

    @JsonProperty("NumeroGuia")
    private String numeroGuia;
    @JsonProperty("idGuia")
    private String idGuia;
    @JsonProperty("NumeroOC")
    private String numeroOC;
    @JsonProperty("FechaEmision")
    private String fechaEmision;
    @JsonProperty("RucCliente")
    private String rucCliente;
    @JsonProperty("FechaCreacion")
    private String fechaCreacion;
    @JsonProperty("FechaEnvio")
    private String fechaEnvio;
    @JsonProperty("CodigoAlmacen")
    private String codigoAlmacen;
    @JsonProperty("MotivoGuia")
    private String motivoGuia;
    @JsonProperty("PuntoPartida")
    private String puntoPartida;
    @JsonProperty("PuntoLlegada")
    private String puntoLlegada;
    @JsonProperty("TipDocTransportista")
    private String tipDocTransportista;
    @JsonProperty("NumDocTransportista")
    private String numDocTransportista;
    @JsonProperty("RazonSocialTransportista")
    private String razonSocialTransportista;
    @JsonProperty("DirTransportista")
    private String dirTransportista;
    @JsonProperty("TipoTransporte")
    private String tipoTransporte;
    @JsonProperty("PlacaoNave")
    private String placaoNave;
    @JsonProperty("RegistroMTC")
    private String registroMTC;
    @JsonProperty("FechaInicioTraslado")
    private String fechaInicioTraslado;
    @JsonProperty("FechaProbArribo")
    private String fechaProbArribo;
    @JsonProperty("TotalBultos")
    private String totalBultos;
    @JsonProperty("TotalPesoNeto")
    private String totalPesoNeto;
    @JsonProperty("TotalPesoBruto")
    private String totalPesoBruto;
    @JsonProperty("Tara")
    private String tara;
    @JsonProperty("TotalVolumen")
    private String totalVolumen;
    @JsonProperty("CodUnidadPeso")
    private String codUnidadPeso;
    @JsonProperty("CodUnidadVolumen")
    private String codUnidadVolumen;
    @JsonProperty("Observaciones")
    private String observaciones;
    @JsonProperty("EjercicioGuia")
    private String ejercicioGuia;
    @JsonProperty("ClaseDocumentoGuia")
    private String claseDocumentoGuia;
    @JsonProperty("ClaseOperacionGuia")
    private String claseOperacionGuia;
    @JsonProperty("FechaContabilizacionGuia")
    private String fechaContabilizacionGuia;
    @JsonProperty("Sociedad")
    private String sociedad;
    @JsonProperty("CodigoERPProveedor")
    private String codigoERPProveedor;
    @JsonProperty("TipoGuia")
    private String tipoGuia;
    @JsonProperty("UsuarioCreacionGuia")
    private String usuarioCreacionGuia;
    @JsonProperty("CodigoGuiaERP")
    private String codigoGuiaERP;
    @JsonProperty("CodigoMercaderiaB2M")
    private String codigoMercaderiaB2M;
    @JsonProperty("MonedaGuia")
    private String monedaGuia;
    @JsonProperty("ItemGuia")
    private List<ItemGuium> itemGuia = null;
    @JsonProperty("CheckPoint")
    private List<CheckPoint> checkPoint = null;
    @JsonProperty("IdOrganizacionCompradora")
    private String in_idorganizacioncompradora;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("NumeroGuia")
    public String getNumeroGuia() {
        return numeroGuia;
    }

    @JsonProperty("NumeroGuia")
    public void setNumeroGuia(String numeroGuia) {
        this.numeroGuia = numeroGuia;
    }

    @JsonProperty("idGuia")
    public String getIdGuia() {
        return idGuia;
    }
    @JsonProperty("idGuia")
    public void setIdGuia(String idGuia) {
        this.idGuia = idGuia;
    }

    @JsonProperty("NumeroOC")
    public String getNumeroOC() {
        return numeroOC;
    }

    @JsonProperty("NumeroOC")
    public void setNumeroOC(String numeroOC) {
        this.numeroOC = numeroOC;
    }

    @JsonProperty("FechaEmision")
    public String getFechaEmision() {
        return fechaEmision;
    }

    @JsonProperty("FechaEmision")
    public void setFechaEmision(String fechaEmision) {
        this.fechaEmision = fechaEmision;
    }

    @JsonProperty("RucCliente")
    public String getRucCliente() {
        return rucCliente;
    }

    @JsonProperty("RucCliente")
    public void setRucCliente(String rucCliente) {
        this.rucCliente = rucCliente;
    }

    @JsonProperty("FechaCreacion")
    public String getFechaCreacion() {
        return fechaCreacion;
    }

    @JsonProperty("FechaCreacion")
    public void setFechaCreacion(String fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    @JsonProperty("FechaEnvio")
    public String getFechaEnvio() {
        return fechaEnvio;
    }

    @JsonProperty("FechaEnvio")
    public void setFechaEnvio(String fechaEnvio) {
        this.fechaEnvio = fechaEnvio;
    }

    @JsonProperty("CodigoAlmacen")
    public String getCodigoAlmacen() {
        return codigoAlmacen;
    }

    @JsonProperty("CodigoAlmacen")
    public void setCodigoAlmacen(String codigoAlmacen) {
        this.codigoAlmacen = codigoAlmacen;
    }

    @JsonProperty("MotivoGuia")
    public String getMotivoGuia() {
        return motivoGuia;
    }

    @JsonProperty("MotivoGuia")
    public void setMotivoGuia(String motivoGuia) {
        this.motivoGuia = motivoGuia;
    }

    @JsonProperty("PuntoPartida")
    public String getPuntoPartida() {
        return puntoPartida;
    }

    @JsonProperty("PuntoPartida")
    public void setPuntoPartida(String puntoPartida) {
        this.puntoPartida = puntoPartida;
    }

    @JsonProperty("PuntoLlegada")
    public String getPuntoLlegada() {
        return puntoLlegada;
    }

    @JsonProperty("PuntoLlegada")
    public void setPuntoLlegada(String puntoLlegada) {
        this.puntoLlegada = puntoLlegada;
    }

    @JsonProperty("TipDocTransportista")
    public String getTipDocTransportista() {
        return tipDocTransportista;
    }

    @JsonProperty("TipDocTransportista")
    public void setTipDocTransportista(String tipDocTransportista) {
        this.tipDocTransportista = tipDocTransportista;
    }

    @JsonProperty("NumDocTransportista")
    public String getNumDocTransportista() {
        return numDocTransportista;
    }

    @JsonProperty("NumDocTransportista")
    public void setNumDocTransportista(String numDocTransportista) {
        this.numDocTransportista = numDocTransportista;
    }

    @JsonProperty("RazonSocialTransportista")
    public String getRazonSocialTransportista() {
        return razonSocialTransportista;
    }

    @JsonProperty("RazonSocialTransportista")
    public void setRazonSocialTransportista(String razonSocialTransportista) {
        this.razonSocialTransportista = razonSocialTransportista;
    }

    @JsonProperty("DirTransportista")
    public String getDirTransportista() {
        return dirTransportista;
    }

    @JsonProperty("DirTransportista")
    public void setDirTransportista(String dirTransportista) {
        this.dirTransportista = dirTransportista;
    }

    @JsonProperty("TipoTransporte")
    public String getTipoTransporte() {
        return tipoTransporte;
    }

    @JsonProperty("TipoTransporte")
    public void setTipoTransporte(String tipoTransporte) {
        this.tipoTransporte = tipoTransporte;
    }

    @JsonProperty("PlacaoNave")
    public String getPlacaoNave() {
        return placaoNave;
    }

    @JsonProperty("PlacaoNave")
    public void setPlacaoNave(String placaoNave) {
        this.placaoNave = placaoNave;
    }

    @JsonProperty("RegistroMTC")
    public String getRegistroMTC() {
        return registroMTC;
    }

    @JsonProperty("RegistroMTC")
    public void setRegistroMTC(String registroMTC) {
        this.registroMTC = registroMTC;
    }

    @JsonProperty("FechaInicioTraslado")
    public String getFechaInicioTraslado() {
        return fechaInicioTraslado;
    }

    @JsonProperty("FechaInicioTraslado")
    public void setFechaInicioTraslado(String fechaInicioTraslado) {
        this.fechaInicioTraslado = fechaInicioTraslado;
    }

    @JsonProperty("FechaProbArribo")
    public String getFechaProbArribo() {
        return fechaProbArribo;
    }

    @JsonProperty("FechaProbArribo")
    public void setFechaProbArribo(String fechaProbArribo) {
        this.fechaProbArribo = fechaProbArribo;
    }

    @JsonProperty("TotalBultos")
    public String getTotalBultos() {
        return totalBultos;
    }

    @JsonProperty("TotalBultos")
    public void setTotalBultos(String totalBultos) {
        this.totalBultos = totalBultos;
    }

    @JsonProperty("TotalPesoNeto")
    public String getTotalPesoNeto() {
        return totalPesoNeto;
    }

    @JsonProperty("TotalPesoNeto")
    public void setTotalPesoNeto(String totalPesoNeto) {
        this.totalPesoNeto = totalPesoNeto;
    }

    @JsonProperty("TotalPesoBruto")
    public String getTotalPesoBruto() {
        return totalPesoBruto;
    }

    @JsonProperty("TotalPesoBruto")
    public void setTotalPesoBruto(String totalPesoBruto) {
        this.totalPesoBruto = totalPesoBruto;
    }

    @JsonProperty("Tara")
    public String getTara() {
        return tara;
    }

    @JsonProperty("Tara")
    public void setTara(String tara) {
        this.tara = tara;
    }

    @JsonProperty("TotalVolumen")
    public String getTotalVolumen() {
        return totalVolumen;
    }

    @JsonProperty("TotalVolumen")
    public void setTotalVolumen(String totalVolumen) {
        this.totalVolumen = totalVolumen;
    }

    @JsonProperty("CodUnidadPeso")
    public String getCodUnidadPeso() {
        return codUnidadPeso;
    }

    @JsonProperty("CodUnidadPeso")
    public void setCodUnidadPeso(String codUnidadPeso) {
        this.codUnidadPeso = codUnidadPeso;
    }

    @JsonProperty("CodUnidadVolumen")
    public String getCodUnidadVolumen() {
        return codUnidadVolumen;
    }

    @JsonProperty("CodUnidadVolumen")
    public void setCodUnidadVolumen(String codUnidadVolumen) {
        this.codUnidadVolumen = codUnidadVolumen;
    }

    @JsonProperty("Observaciones")
    public String getObservaciones() {
        return observaciones;
    }

    @JsonProperty("Observaciones")
    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    @JsonProperty("EjercicioGuia")
    public String getEjercicioGuia() {
        return ejercicioGuia;
    }

    @JsonProperty("EjercicioGuia")
    public void setEjercicioGuia(String ejercicioGuia) {
        this.ejercicioGuia = ejercicioGuia;
    }

    @JsonProperty("ClaseDocumentoGuia")
    public String getClaseDocumentoGuia() {
        return claseDocumentoGuia;
    }

    @JsonProperty("ClaseDocumentoGuia")
    public void setClaseDocumentoGuia(String claseDocumentoGuia) {
        this.claseDocumentoGuia = claseDocumentoGuia;
    }

    @JsonProperty("ClaseOperacionGuia")
    public String getClaseOperacionGuia() {
        return claseOperacionGuia;
    }

    @JsonProperty("ClaseOperacionGuia")
    public void setClaseOperacionGuia(String claseOperacionGuia) {
        this.claseOperacionGuia = claseOperacionGuia;
    }

    @JsonProperty("FechaContabilizacionGuia")
    public String getFechaContabilizacionGuia() {
        return fechaContabilizacionGuia;
    }

    @JsonProperty("FechaContabilizacionGuia")
    public void setFechaContabilizacionGuia(String fechaContabilizacionGuia) {
        this.fechaContabilizacionGuia = fechaContabilizacionGuia;
    }

    @JsonProperty("Sociedad")
    public String getSociedad() {
        return sociedad;
    }

    @JsonProperty("Sociedad")
    public void setSociedad(String sociedad) {
        this.sociedad = sociedad;
    }

    @JsonProperty("CodigoERPProveedor")
    public String getCodigoERPProveedor() {
        return codigoERPProveedor;
    }

    @JsonProperty("CodigoERPProveedor")
    public void setCodigoERPProveedor(String codigoERPProveedor) {
        this.codigoERPProveedor = codigoERPProveedor;
    }

    @JsonProperty("TipoGuia")
    public String getTipoGuia() {
        return tipoGuia;
    }

    @JsonProperty("TipoGuia")
    public void setTipoGuia(String tipoGuia) {
        this.tipoGuia = tipoGuia;
    }

    @JsonProperty("UsuarioCreacionGuia")
    public String getUsuarioCreacionGuia() {
        return usuarioCreacionGuia;
    }

    @JsonProperty("UsuarioCreacionGuia")
    public void setUsuarioCreacionGuia(String usuarioCreacionGuia) {
        this.usuarioCreacionGuia = usuarioCreacionGuia;
    }

    @JsonProperty("CodigoGuiaERP")
    public String getCodigoGuiaERP() {
        return codigoGuiaERP;
    }

    @JsonProperty("CodigoGuiaERP")
    public void setCodigoGuiaERP(String codigoGuiaERP) {
        this.codigoGuiaERP = codigoGuiaERP;
    }

    @JsonProperty("CodigoMercaderiaB2M")
    public String getCodigoMercaderiaB2M() {
        return codigoMercaderiaB2M;
    }

    @JsonProperty("CodigoMercaderiaB2M")
    public void setCodigoMercaderiaB2M(String codigoMercaderiaB2M) {
        this.codigoMercaderiaB2M = codigoMercaderiaB2M;
    }

    @JsonProperty("MonedaGuia")
    public String getMonedaGuia() {
        return monedaGuia;
    }

    @JsonProperty("MonedaGuia")
    public void setMonedaGuia(String monedaGuia) {
        this.monedaGuia = monedaGuia;
    }

    @JsonProperty("ItemGuia")
    public List<ItemGuium> getItemGuia() {
        return itemGuia;
    }

    @JsonProperty("ItemGuia")
    public void setItemGuia(List<ItemGuium> itemGuia) {
        this.itemGuia = itemGuia;
    }

    @JsonProperty("CheckPoint")
    public List<CheckPoint> getCheckPoint() {
        return checkPoint;
    }

    @JsonProperty("CheckPoint")
    public void setCheckPoint(List<CheckPoint> checkPoint) {
        this.checkPoint = checkPoint;
    }

    @JsonProperty("IdOrganizacionCompradora")
    public String getIn_idorganizacioncompradora() {
        return in_idorganizacioncompradora;
    }
    @JsonProperty("IdOrganizacionCompradora")
    public void setIn_idorganizacioncompradora(String in_idorganizacioncompradora) {
        this.in_idorganizacioncompradora = in_idorganizacioncompradora;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
