
package com.ebiz.soap.integration.api.guia.model;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "NumeroItemOCChk",
    "FechaGuiaDespachoChk",
    "CantidadDespachadaChk",
    "UnidadMedidaChk"
})
public class CheckPoint {

    @JsonProperty("NumeroItemOCChk")
    private String numeroItemOCChk;
    @JsonProperty("FechaGuiaDespachoChk")
    private String fechaGuiaDespachoChk;
    @JsonProperty("CantidadDespachadaChk")
    private String cantidadDespachadaChk;
    @JsonProperty("UnidadMedidaChk")
    private String unidadMedidaChk;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("NumeroItemOCChk")
    public String getNumeroItemOCChk() {
        return numeroItemOCChk;
    }

    @JsonProperty("NumeroItemOCChk")
    public void setNumeroItemOCChk(String numeroItemOCChk) {
        this.numeroItemOCChk = numeroItemOCChk;
    }

    @JsonProperty("FechaGuiaDespachoChk")
    public String getFechaGuiaDespachoChk() {
        return fechaGuiaDespachoChk;
    }

    @JsonProperty("FechaGuiaDespachoChk")
    public void setFechaGuiaDespachoChk(String fechaGuiaDespachoChk) {
        this.fechaGuiaDespachoChk = fechaGuiaDespachoChk;
    }

    @JsonProperty("CantidadDespachadaChk")
    public String getCantidadDespachadaChk() {
        return cantidadDespachadaChk;
    }

    @JsonProperty("CantidadDespachadaChk")
    public void setCantidadDespachadaChk(String cantidadDespachadaChk) {
        this.cantidadDespachadaChk = cantidadDespachadaChk;
    }

    @JsonProperty("UnidadMedidaChk")
    public String getUnidadMedidaChk() {
        return unidadMedidaChk;
    }

    @JsonProperty("UnidadMedidaChk")
    public void setUnidadMedidaChk(String unidadMedidaChk) {
        this.unidadMedidaChk = unidadMedidaChk;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
